package rmiinterfaces;

import entities.Node;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface LeaderPeerDownService extends Remote {
    static final String SERVICE_NAME = "LeaderPeerDownService";

    public void peerDown(Node peer) throws RemoteException;
}
