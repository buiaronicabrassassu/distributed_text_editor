package rmiinterfaces;

import entities.Task;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface TaskNotificationService extends Remote {
    static final String SERVICE_NAME = "taskNotificationService";

    public void notifyTask(Task remoteTask) throws RemoteException;
}
