package rmiinterfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface HeartBeatService extends Remote {
    static final String SERVICE_NAME = "heartBeatService";

    public boolean heartBeat() throws RemoteException;

}