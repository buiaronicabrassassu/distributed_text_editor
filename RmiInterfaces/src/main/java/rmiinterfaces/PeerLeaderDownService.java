package rmiinterfaces;

import entities.Node;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface PeerLeaderDownService extends Remote {
    static final String SERVICE_NAME = "PeerLeaderDownService";

    public void leaderDown(Node leader) throws RemoteException;
}
