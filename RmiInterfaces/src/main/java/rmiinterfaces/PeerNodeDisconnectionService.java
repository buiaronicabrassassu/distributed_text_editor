package rmiinterfaces;

import entities.Node;
import entities.Task;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface PeerNodeDisconnectionService extends Remote {
    static final String SERVICE_NAME = "disconnectionService";

    public void disconnectNode(Node node, List<Task> nodeTasks) throws RemoteException;

    public void disconnectionAck() throws RemoteException;
}
