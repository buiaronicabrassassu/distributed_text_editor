package rmiinterfaces;

import entities.Node;
import entities.Version;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface PeerLoginService extends Remote {
    static final String SERVICE_NAME = "PeerLoginService";

    void loginAck(Version version, List<Node> otherPeers) throws RemoteException;
    void notifyNewNode(Node newNode) throws RemoteException;
}
