package rmiinterfaces;

import entities.Node;


import java.rmi.Remote;
import java.rmi.RemoteException;

public interface LeaderNodeLoginService extends Remote {
    static final String SERVICE_NAME = "LeaderNodeLoginService";

    public void login(Node user) throws RemoteException;
}