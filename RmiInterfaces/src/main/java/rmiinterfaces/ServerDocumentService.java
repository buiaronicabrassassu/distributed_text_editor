package rmiinterfaces;

import entities.Node;
import entities.Version;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ServerDocumentService extends Remote{
    public static final String SERVICE_NAME = "documentService";

    public Node getLeader(Node node) throws RemoteException;
    public void disconnectLeader(Node node) throws RemoteException;
    public void close(Node node) throws RemoteException;
    public Version getLastVersion() throws RemoteException;
    public void updateVersion(Version version) throws RemoteException;
    public Node leaderDown(Node oldLeader, Node node) throws RemoteException;
    public Node electNewLeader(Node oldLeaderNode, Node node) throws RemoteException;



}