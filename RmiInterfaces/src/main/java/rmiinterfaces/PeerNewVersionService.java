package rmiinterfaces;

import entities.Task;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface PeerNewVersionService extends Remote {
    static final String SERVICE_NAME = "peerNewVersionService";

    public void newVersion(int versionID, int userId, List<Task> peerTasks) throws RemoteException;

    public void newVersionAck(int versionID) throws RemoteException;

    public void newVersionFail(int versionID) throws RemoteException;


}
