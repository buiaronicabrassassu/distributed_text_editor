package rmiinterfaces;

import entities.Node;
import entities.Task;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface LeaderNodeDisconnectionService extends Remote {
    static final String SERVICE_NAME = "disconnectionService";

    public void disconnectionRequest(Node user, List<Task> tasks) throws RemoteException;
}
