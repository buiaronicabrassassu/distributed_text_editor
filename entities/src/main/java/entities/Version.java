package entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public final class Version implements Serializable {

    public final int id;
    public final List<Character> crdt;
    private final List<Character> pendingDeletes;

    public Version(int id, List<Character> crdt) {
        this(id,crdt, new ArrayList<>());
    }

    public Version(int id, List<Character> crdt,  List<Character> pendingDeletes){
        this.id = id;
        this.crdt = crdt;
        this.pendingDeletes = pendingDeletes;
    }

    public Version newPartialVersion(List<Task> tasks){
        return newVersion(id, new ArrayList<List<Task>>(){{add(tasks);}});
    }

    public Version newVersion(int versionID, List<List<Task>> updates){
        if(id>versionID){
            throw new IllegalArgumentException("The new versionId must be grater than the current versionId");
        }
        final boolean isPartialNewVersion = id == versionID;
        ArrayList<Character> crdtToUpdate = new ArrayList<>(crdt);

        ArrayList<Character> inserts = new ArrayList<>();
        ArrayList<Character> deletes = new ArrayList<>(this.pendingDeletes);
        ArrayList<Character> pendingDeletes = new ArrayList<>();

        int i,j = 0;
        Task temp;

        //Separate inserts and deletes
        for(i = 0; i < updates.size(); i++) {
            for (j = 0; j < updates.get(i).size(); j++) {
                temp = new Task(updates.get(i).get(j));
                if (temp.type.equals("insert"))
                    inserts.add(temp.character);
                else deletes.add(temp.character);
            }
        }

        inserts.sort((c1,c2)->{return Character.compare(c1, c2);});
        deletes.sort((c1,c2)->{return Character.compare(c1, c2);});

        j = 0;
        for(i = 0; j < inserts.size(); i++){
            if(i < crdtToUpdate.size()){
                if(inserts.get(j).isBefore(crdtToUpdate.get(i))){
                    crdtToUpdate.add(i, inserts.get(j));
                    j++;
                }
            }
            else{
                crdtToUpdate.add(inserts.get(j));
                j++;
            }
        }

        i = 0;
        j = 0;
        while( i<deletes.size() && j<crdtToUpdate.size() ){

            Character charToDelete = deletes.get(i);
            Character currentChar = crdtToUpdate.get(j);

            if(charToDelete.equals(currentChar)){
                crdtToUpdate.remove(j);
                ++i;
            }
            else if(charToDelete.isAfter(currentChar)){
                ++j;
            }
            else{
                if(isPartialNewVersion){
                    pendingDeletes.add(charToDelete);
                }
                ++i;
            }
        }
        return new Version(versionID, crdtToUpdate, pendingDeletes);
    }

    public static Version getTestVersion(){
        User user = new User("Nicolo",90);
        User user2 = new User("Simone Mensa",91);
        ArrayList<Character> document = new ArrayList<Character>(){{
            add(new Character('c',"1.1",user , 0, 0));
            add(new Character('i',"2",user , 0, 1));
            add(new Character('a',"3",user , 0, 2));
            add(new Character('o',"4.8",user , 0, 3));
            add(new Character('\n',"5",user , 0, 4));

            add(new Character('p',"6",user2 , 0, 0));
            add(new Character('r',"7",user2 , 0, 1));
            add(new Character('e',"8",user2 , 0, 2));
            add(new Character('g',"9",user2 , 0, 3));
            add(new Character('o',"10",user2 , 0, 4));
            add(new Character(' ',"11",user2 , 0, 5));
            add(new Character('p',"12",user2 , 0, 6));
            add(new Character('e',"13",user2 , 0, 7));
            add(new Character('r',"14",user2 , 0, 8));
            add(new Character(' ',"15",user2 , 0, 9));
            add(new Character('l',"16",user2 , 0, 10));
            add(new Character('e',"17",user2 , 0, 11));
            add(new Character('i',"18",user2 , 0, 12));
            add(new Character('!',"19",user2 , 0, 13));
        }};
        Version version = new Version(0,new ArrayList<>(document));
        return version;
    }

    public String toString(){
        StringBuilder crdtString = new StringBuilder();
        crdt.forEach((character) ->{
            crdtString.append(character.value);
        });

        return crdtString.toString();
    }
}