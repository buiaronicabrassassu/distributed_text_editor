package entities;

import java.io.Serializable;
import java.util.Objects;

public class User implements Serializable {

    public final String name;
    public final int id;

    /*public String name;
    public int nodeId;*/

    public User(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public User(User user){
        this.name = user.name;
        this.id = user.id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                name.equals(user.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, id);
    }

}