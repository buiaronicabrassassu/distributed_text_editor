package entities;

import com.google.gson.Gson;

import java.io.Serializable;
import java.util.Objects;

public class Node implements Serializable {
    public final User user;
    public final String ipAddress;
    public final int registryPort;

    public Node(User user, String ipAddress, int registryPort){
        this.user = user;
        this.ipAddress = ipAddress;
        this.registryPort = registryPort;
    }

    public String getUserName(){
        return user.name;
    }

    public int getId(){
        return user.id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node node = (Node) o;
        return registryPort == node.registryPort &&
                user.equals(node.user) &&
                ipAddress.equals(node.ipAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, ipAddress, registryPort);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
