package entities;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Task implements Serializable {
    
    public String type;
    public int version;
    @SerializedName(value="char")
    public Character character;

    public Task(String type, int version, Character character){
        this.type = type;
        this.version = version;
        this.character = new Character(character);
    }

    public Task(Task task){
        this.type = task.type;
        this.version = task.version;
        this.character = new Character(task.character);
    }

    public String getKey(){
        StringBuilder key = new StringBuilder();
        key.append(type).append(character.id);
        return key.toString();
    }


    public String toString() {
        return new Gson().toJson(this);
    }
}