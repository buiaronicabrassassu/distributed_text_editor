package entities;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Character implements Serializable {

    public final char value;
    public final String id;
    public final User user;
    public final int version;
    public final int timestamp;


    public Character(char value, String id, entities.User user, int version, int timestamp) {
        this.value = value;
        this.id = id;
        this.user = new User(user);
        this.version = version;
        this.timestamp = timestamp;
    }

    public Character(Character character){
        this.value = character.value;
        this.id = character.id;
        this.user = new User(character.user);
        this.version = character.version;
        this.timestamp = character.timestamp;
    }

    public boolean isBefore(Character c){
        return Character.compare(this, c)<0;
    }

    public boolean equals(Character c){
        return Character.compare(this, c)==0;
    }

    public boolean isAfter(Character c){
        return Character.compare(this, c)>0;
    }

    public static int compare(Character char1, Character char2){
        Integer[] char1Id = char1.getIdAsList();
        Integer[] char2Id = char2.getIdAsList();

        final int maxIdSize = Math.max(char1Id.length, char2Id.length);
        final int fullIdSize = maxIdSize + 3;

        Integer[] id1Padd = Collections.nCopies(maxIdSize - char1Id.length,0).toArray(new Integer[0]);
        Integer[] id2Padd = Collections.nCopies(maxIdSize - char2Id.length,0).toArray(new Integer[0]);

        Integer[] fullId1 = Stream
                .of(char1Id, id1Padd, new Integer[]{char1.user.id, char1.version, char1.timestamp})
                .flatMap(Arrays::stream)
                .toArray(Integer[]::new);
        Integer[] fullId2 = Stream
                .of(char2Id, id2Padd, new Integer[]{char2.user.id, char2.version, char2.timestamp})
                .flatMap(Arrays::stream)
                .toArray(Integer[]::new);

        return IntStream.range(0,fullIdSize).reduce(0,(result,index)->{
            if(result!=0){
                return result;
            }
            return fullId1[index].compareTo(fullId2[index]);
        });

    }

    private Integer[] getIdAsList(){
        return  Arrays.stream(id.split("\\."))
                .map(Integer::valueOf)
                .toArray(Integer[]::new);
    }

}