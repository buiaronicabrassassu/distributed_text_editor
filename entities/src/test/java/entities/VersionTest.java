package entities;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VersionTest {
    User user1 = new User("user1",1);
    User user2 = new User("user1",2);
    List<Character> baseVersion = Arrays.asList(
            new Character('c',"1",user1,0,0),
            new Character('i',"2",user1,0,1),
            new Character('a',"3",user1,0,2)
    );

    @Test
    public void newVersion_insertAtTheEnd(){
        Version v0 = new Version(0,baseVersion);
        List<List<Task>> tasks = new ArrayList<List<Task>>(){{
            add(Arrays.asList(new Task("insert",0,new Character('o',"4",user1,0,1))));
        }};
        Version v1 = v0.newVersion(1, tasks);
        assertEquals("ciao",v1.toString());
    }

    @Test
    public void newVersion_deleteLastChar(){
        Version v0 = new Version(0,baseVersion);
        List<List<Task>> tasks = new ArrayList<List<Task>>(){{
            add(Arrays.asList(new Task("delete",0,new Character('a',"3",user1,0,2))));
        }};
        Version v1 = v0.newVersion(1, tasks);
        assertEquals("ci",v1.toString());
    }

    @Test
    public void newVersion_insertAndDeleteSameChar(){
        Version v0 = new Version(0,baseVersion);
        List<List<Task>> tasks = new ArrayList<List<Task>>(){{
            add(Arrays.asList(new Task("insert",0,new Character('o',"4",user2,0,1))));
            add(Arrays.asList(new Task("delete",0,new Character('o',"4",user2,0,1))));
        }};
        Version v1 = v0.newVersion(1, tasks);
        assertEquals("cia",v1.toString());
    }

    @Test
    public void newVersion_multipleDeletes(){
        Version v0 = new Version(0,baseVersion);
        List<List<Task>> tasks = new ArrayList<List<Task>>(){{
            add(Arrays.asList(new Task("insert",0,new Character('o',"4",user2,0,1))));
            add(Arrays.asList(
                    new Task("delete",0,new Character('o',"4",user2,0,1)),
                    new Task("delete",0,new Character('i',"2",user1,0,1)),
                    new Task("delete",0,new Character('c',"1",user1,0,0))
            ));
        }};
        Version v1 = v0.newVersion(1, tasks);
        assertEquals("a",v1.toString());
    }
}
