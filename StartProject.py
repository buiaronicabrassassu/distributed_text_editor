# this script require:
# - python >= 3.6
# - tmux terminal emulator
# - libtmux

import os
import subprocess as sp
import time
import socket
import signal
import libtmux
import threading

# ================== CONFIG OBJECTS ==================
config = {
    'buildProject': True,
    'dockerBuild': True,
}

server = {
    'address': "172.28.0.2",
    'registry_port': 1099
}

peers = [
    {
        'user_name': 'Tinzu',
        'user_id': 1,
        'peer_registry_port': 8099,
        'peer_server_port': 8081,
        'server_address': server['address'],
        'server_registry_port': server['registry_port']
    },
    {
        'user_name': 'Simingione',
        'user_id': 2,
        'peer_registry_port': 8100,
        'peer_server_port': 8082,
        'server_address': server['address'],
        'server_registry_port': server['registry_port']
    },
    {
        'user_name': 'SaMarra',
        'user_id': 3,
        'peer_registry_port': 8101,
        'peer_server_port': 8083,
        'server_address': server['address'],
        'server_registry_port': server['registry_port']
    }
]

# =============== GLOBAL VARS ======================

PROCESS_DICT = {}
PROJECT_PATH = os.path.dirname(os.path.realpath(__file__))

def store_process(process, key, process_dict=PROCESS_DICT):
    process_dict[key] = process

def execute(command, stdout=sp.DEVNULL, stderr=sp.DEVNULL, cwd=PROJECT_PATH):
    sp_command = [c for c in command.split(" ") if c not in ['']]
    
    result_code = sp.call(
        sp_command,
        cwd=cwd,
        stdout=stdout,
        stderr=stderr
    )

    return result_code

def execute_process(command, stdout=sp.DEVNULL, stderr=sp.DEVNULL, cwd=PROJECT_PATH, shell=False):
    sp_command = [c for c in command.split(" ") if c not in ['']]
    
    process = sp.Popen(
        sp_command,
        cwd=cwd,
        stdout=stdout,
        stderr=stderr,
        shell=shell,
    )
    
    return process

def stop_project(exit_code=0, process_dict = PROCESS_DICT):

    print("\n===========KILL PROJECT=============")    
    signal.signal(signal.SIGINT, signal.SIG_IGN)
    
    tmux_process = process_dict.get('tmux_process',None)
    browsers = { key:value for (key,value) in process_dict.items() if "browser" in key }
    
    print("Killing browsers")
    for (key,browser_process) in browsers.items():
        print(f"killing {key}")
        browser_process.kill()
        browser_process.wait()
    sp.call(['pkill', 'google-chrome'])
    
    print("Killing containers")
    stop_containers()
    
    print("Killing terminals")
    if tmux_process:
        tmux_process.kill()
        tmux_process.wait()

    print("Shutdown Completed")

    exit(exit_code)

def build_gui(gui_prefix=os.path.join(PROJECT_PATH, "gui")):
    print("GUI: build started")
    return_code = execute(f"npm run build --prefix {gui_prefix}")
    if return_code != 0:
        print("GUI: build failed")
        stop_project(exit_code=-1)
    else:
        print("GUI: build completed")
        
def buil_server():
    print("SERVER: build started")
    result_code = execute("gradle server:jar")
    if result_code != 0:
        print("SERVER: build failed")
        stop_project(exit_code=-1)
    else:
        print("SERVER: build completed")
       
def buil_peer():
    print("PEER: build started")
    result=execute("gradle peer:jar")
    if result != 0:
        print("PEER: build failed")
        stop_project(exit_code=-1)
    else:
        print("PEER: build completed")

def build_containers(project_path=PROJECT_PATH):
    print("DOCKER: Building contaners")

    result_code = execute('docker-compose build')
    
    if result_code != 0:
        print("DOCKER: Containers build failed")
        stop_project(exit_code=-1)
    else:
        print("DOCKER: containers build completed")     

def stop_containers():
    print("DOCKER: Killing containers")
    result_code = execute("docker-compose down")

def wait_for_port(port, host='localhost', timeout=40.0):
    start_time = time.perf_counter()
    while True:
        result = execute(f"nc -z -w5 {host} {port}")
        if result == 0:
            return 0
        else:
            time.sleep(0.5)
            if time.perf_counter() - start_time >= timeout:
                print("timeout expired")
                return -1

def wait_for_httpServer(url, timeout=40):
    start_time = time.perf_counter()
    while True:
        result = execute(f"curl {url}")
        if result == 0:
            return 0
        else:
            time.sleep(0.5)
            if time.perf_counter() - start_time >= timeout:
                print("timeout expired")
                return -1

def open_tmux(working_path=PROJECT_PATH):
    session_name = "dte_session"
    execute(f"tmux kill-session -t {session_name}")
    execute(f"tmux new -d -s {session_name} -c {working_path}")
    
    full_command = f"deepin-terminal -w {working_path} -e tmux attach -t {session_name}"
    tmux_process = execute_process(command=full_command)
    
    store_process(process=tmux_process, key='tmux_process')
    
    server = libtmux.Server()
    session = server.find_where({"session_name": session_name})
    tmux_window = session.attached_window

    pane0 = tmux_window.attached_pane
    pane1 = tmux_window.split_window(vertical=True, start_directory=working_path)
    pane2 = pane0.split_window(vertical=False, start_directory=working_path)
    pane3 = pane1.split_window(vertical=False, start_directory=working_path)

    tmux_panes = {
        'server': pane0,
        'peer1': pane2,
        'peer2': pane3,
        'peer3': pane1,
    }

    return (tmux_panes, tmux_window)

def run_server(pane, wait=False):
    print("SERVER: starting server")
    pane.send_keys("docker-compose run --name dte_server server", enter=True)

    if wait:
        # whait untill the server is up
        server_status = wait_for_port(
            port=server['registry_port'], host=server['address'])
        if server_status == 0:
            print("SERVER: server started")
        else:
            print("SERVER: start failed")
            stop_project(exit_code=-1)

def run_peer(pane, user_name, user_id, peer_registry_port, peer_server_port, server_address,        server_registry_port, show_gui=False):
    print(f"PEER: starting {user_name}")
    
    docker_command = f"docker-compose run\
        --name dte_peer_{user_id}\
        -e USER_NAME={user_name}\
        -e USER_ID={user_id}\
        -e PEER_REG_PORT={peer_registry_port}\
        -e SERVER_ADDRESS={server_address}\
        -e SERVER_REG_PORT={server_registry_port}\
        --publish {peer_server_port}:80\
        --rm\
        peer"
    
    browser_command = f"google-chrome\
        --new-window\
        --app=http://127.0.0.1:{peer_server_port}\
        http://127.0.0.1:{peer_server_port}"

    pane.send_keys(docker_command, enter=True)

    if show_gui:
        result = wait_for_httpServer(url=f"127.0.0.1:{peer_server_port}")
        if(result == 0):
            print(f"PEER: {user_name} started")
            process = execute_process(browser_command)
            store_process(process=process, key=f'browser_{user_name}')
        else:
            print(f"PEER: {user_name} failed")
            stop_project(-1)
            
def run_all_nodes():
    run_server(pane=tmux_panes['server'], wait=True)
    peer_pane_ids = [pane_id for pane_id in [*tmux_panes.keys()] if "peer" in pane_id]
    
    for index in range(0,len(peers)):
        if(index < len(tmux_panes)):
            pane_id = peer_pane_ids[index]
            peer_main = lambda:run_peer(pane=tmux_panes[pane_id], **peers[index], show_gui=True)
            threading.Thread(target=peer_main).start()
            if(index == 0 ):
                time.sleep(5)
                
def print_menu():
    
    print("select a command to run:")
    print(" -1: start server")
    print(" -2: start peer1")
    print(" -3: start peer2")
    print(" -4: start peer3")
    print(" -5: kill server")
    print(" -6: kill peer1")
    print(" -7: kill peer2")
    print(" -8: kill peer3")
    print(" -9: start all")
    print(" -0: exit")

def getCommandHandler(command):
    
    commandHandlers = {
        '1': lambda: run_server(pane=tmux_panes['server'], wait=True),
        
        '2': lambda:run_peer(pane=tmux_panes['peer1'], **peers[0], show_gui=True),
                    
        '3': lambda:run_peer(pane=tmux_panes['peer2'], **peers[1], show_gui=True),
        
        '4': lambda:run_peer(pane=tmux_panes['peer3'], **peers[2], show_gui=True),
        
        '5': lambda:execute('docker rm -f dte_server'),
        '6': lambda:execute('docker rm -f dte_peer_1'),
        '7': lambda:execute('docker rm -f dte_peer_2'),
        '8': lambda:execute('docker rm -f dte_peer_3'),
        '9': run_all_nodes,
        '0': lambda: stop_project(0),
    }
    return commandHandlers.get(command, lambda:print("invalid command"))
# ====================================================================
#                           MAIN
# ====================================================================

signal.signal(signal.SIGINT, lambda sigNum, frame: stop_project())

print("Killing existing containers")
stop_containers()

# =======================COMPILE GUI===============================
if config['buildProject']:
    build_gui()
    buil_server()
    buil_peer()

# ====================BUILD CONTAINERS=========================
if config['dockerBuild']:
    build_containers()

# ====================RUN CONTAINERS=========================
(tmux_panes, tmux_window) = open_tmux()

os.system('clear')
while(True):
    print_menu()
    command = input()
    
    os.system('clear')    
    commandHandler = getCommandHandler(command)
    commandHandler()
    print("\ncomand executed\n")
