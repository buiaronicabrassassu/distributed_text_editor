# Project content:
The project is composed by:
- Server App: Server that keep each file and the leader node for each file
- Peer App: A node that partecipate to a document
- Gui: The gui for the Peer App

# Compile the project

## Compile the Server App
The server app can be compiled using the command  
> `gradle server:build`

It is  possible to generate a jar package with  
> `gradle server:jar`

The command must be executed into the main project directory

## Compile the Peer App
The Peer app can be compiled using the command  
> `gradle peer:build`

It is possible to generate a jar package with  
> `gradle peer:jar`

The command must be executed into the main project directory

## Compile the GUI
The GUI can be compiled using the command
> `npm run build`

The command must be executed into the gui directory