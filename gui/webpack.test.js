/* eslint-disable no-undef */
const webpack = require('webpack');
const path = require('path');

const globals = require('./webpack.globals');

module.exports = {
  stats: 'minimal',
  mode: 'development',
  entry: {
    index: ['@babel/polyfill',`${globals.TEST_DIR}/index.test.js`],
  },
  output: {
    path: path.resolve(__dirname, globals.BUILD_DIR),
    publicPath: globals.PUBLIC_PATH,
    filename: '[name].test.js',
  },
  cache: true,
  devtool: 'source-map',

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
            presets: ['@babel/preset-env','@babel/preset-react'],
            plugins: [
              '@babel/plugin-transform-spread',
              "@babel/plugin-proposal-object-rest-spread",
              "@babel/plugin-proposal-class-properties",
              ]

          },
        },
        
      },
    ],
  },
  plugins: [],
};