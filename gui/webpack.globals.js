/* eslint-disable no-undef */
/* eslint-disable max-len */
const path = require('path');

const BUILD_DIR = path.resolve(__dirname, 'build');
const TEST_DIR = path.resolve(__dirname, 'test');
const SRC_DIR = path.resolve(__dirname, 'src');
/* Public dir = directory that contains files that must be copied in the public path for deploy*/
const PUBLIC_DIR = path.resolve(__dirname, 'public');

// Public path = path of the project folder in the deployment server
const PUBLIC_PATH = './';

module.exports = {
  PUBLIC_DIR: PUBLIC_DIR,
  BUILD_DIR: BUILD_DIR,
  TEST_DIR: TEST_DIR,
  SRC_DIR: SRC_DIR,
  PUBLIC_PATH: PUBLIC_PATH,
  
};
