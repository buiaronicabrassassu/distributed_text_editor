/* eslint-disable no-undef */
const webpack = require('webpack');
const path = require('path');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
// const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin').CleanWebpackPlugin;

const merge = require('webpack-merge');
const common = require('./webpack.common');
const globals = require('./webpack.globals');


const extractCSS = new MiniCssExtractPlugin({
  filename: '[name].fonts.css',
  allChunks: true,
});
const extractSASS = new MiniCssExtractPlugin({
  filename: '[name].styles.css',
  allChunks: true,
});


module.exports = merge(common, {
  mode: 'production',
  output: {
    path: path.resolve(__dirname, globals.BUILD_DIR),
    publicPath: globals.PUBLIC_PATH,
    filename: '[name].bundle.js',
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              // you can specify a publicPath here
              // by default it uses publicPath in webpackOptions.output
              publicPath: '../',
              hmr: process.env.NODE_ENV === 'development',
            },
          },
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              // you can specify a publicPath here
              // by default it uses publicPath in webpackOptions.output
              publicPath: '../',
              hmr: process.env.NODE_ENV === 'development',
            },
          },
          'css-loader',
        ],
      },
    ],
  },
  plugins: [

    new CleanWebpackPlugin({
      verbose: true,
    }),
    extractCSS,
    extractSASS,
    // new UglifyJsPlugin({
    //   sourceMap: true,
    //   parallel: true,
    //   exclude: /\.min\.js$/ig, // skip pre-minified libs
    // }),

    new HtmlWebpackPlugin({
      inject: true,
      minify: {
        collapseInlineTagWhitespace: true,
        collapseWhitespace: true,
        removeComments: true,
      },
      template: `${globals.PUBLIC_DIR}/index.html`,
    }),
    /* new CopyWebpackPlugin(
            [
                {from: './public/eds10g.conf', to: globals.NGINX_BUILD},
                {from: './public/Dockerfile', to: globals.BUILD_DIR},
            ],
            {copyUnmodified: false}
        ),*/
    new webpack.DefinePlugin({
      //  disable react developement warnings
      'process.env.NODE_ENV': JSON.stringify('production'),
    }),
  ],
});
