import Crdt from './../src/dataStructures/Crdt'
import Character from './../src/entities/Character'
import User from './../src/entities/User'

const user1 = new User("user1", 1);
const user2 = new User("user2", 2);

const CRDT_insertAndDeleteTest = () => {
    /** @constant @type {Crdt} */
    const CRDT = new Crdt(10,2);
    CRDT.remoteInsert(new Character('c', "2", user1,0,0))
    CRDT.remoteInsert(new Character('i', "2.8", user1,0,1))
    CRDT.remoteInsert(new Character('a', "4", user1,0,3))
    CRDT.remoteInsert(new Character('o', "5.20.40.79", user1,0,4))
    CRDT.remoteInsert(new Character('\n', "6", user2,0,0))

    CRDT.remoteInsert(new Character('m', "8", user1,0,6))
    CRDT.remoteInsert(new Character('o', "9", user1,0,7))
    CRDT.remoteInsert(new Character('d', "10.19", user1,0,8))

    CRDT.localInsert("n",0,4, user1);
    //CRDT.localInsert("o",1,4, user1);




    const versionCRDT = [
        [
            new Character('c', "2", user1,0,0),
            new Character('i', "2.8", user1,0,1),
            new Character('a', "4", user1,0,3),
            new Character('o', "7", user1,0,4),
            //new Character('\n', "7.1", user1,0,5)
        ],
        [
            new Character('w', "8", user2,0,0),
            new Character('o', "9", user2,0,1),
            new Character('r', "10", user2,0,2),
            new Character('l', "11", user2,0,3),
            new Character('d', "12", user2,0,5),
            new Character('!', "12.5", user2,0,5)
        ]
    ]

    
    //CRDT.merge(versionCRDT, 1);
    console.log(CRDT.toString())
}

CRDT_insertAndDeleteTest()
/*const neighbors = CRDT.findNeighbors(char)
assert.deepEqual({prev: 1, next: 2}, neighbors)*/


// const crdtTest = () => {
//     interactor.initializeCrdt({
//         id: 1,
//         crdt: [
//             [
//                 new Character('c', "2", user1, 0, 0),
//                 new Character('i', "2.8", user1, 0, 1),
//                 new Character('a', "4", user1, 0, 3),
//                 new Character('o', "7", user2, 0, 4),
//                 new Character('\n', "7.1", user2, 0, 5)
//             ],
//             [
//                 new Character('m', "8", user1, 0, 6),
//                 new Character('o', "9", user1, 0, 7),
//                 new Character('n', "10", user1, 0, 8),
//                 new Character('d', "11", user1, 0, 9),
//                 new Character('o', "12", user1, 0, 10)
//             ]
//         ]
//     })

//     setTimeout(() => {
//         interactor.merge({
//             id: 1,
//             crdt: [
//                 [
//                     new Character('c', "2", user1, 0, 0),
//                     new Character('i', "2.8", user1, 0, 1),
//                     new Character('a', "4", user1, 0, 3),
//                     new Character('o', "7", user1, 0, 4),
//                     new Character('\n', "7.1", user2, 0, 5)
//                     //new Character('\n', "7.1", user1,0,5)
//                 ],
//                 [
//                     new Character('w', "8", user2, 0, 0),
//                     new Character('o', "9", user2, 0, 1),
//                     new Character('r', "10", user2, 0, 2),
//                     new Character('l', "11", user2, 0, 3),
//                     new Character('d', "12", user2, 0, 5),
//                     new Character('!', "12.5", user2, 0, 5)
//                 ]
//             ]
//         })
//     }, 5000)
// }
