import { assert } from 'chai';
import User from '../src/entities/User'
import Crdt from './../src/dataStructures/Crdt'
import Character from './../src/entities/Character'
import _ from 'lodash'
import CharacterBuffer from '../src/dataStructures/CharacterBuffer';

const user = new User("user0", 0)
const user1 = new User("user1", 1)
const user2 = new User("user2", 2)

describe("CRDT__findChInsertionIndex_tests", () => {

    const getCrdt = () => {
        return new Crdt(100, 20)
    }

    it("depth0_firtsPosition", () => {

        const line = [
            new Character({ value: 'b', id: "2", user: user, version: 0, timestamp: 0 }),
            new Character({ value: 'c', id: "3", user: user, version: 0, timestamp: 0 }),
            new Character({ value: 'e', id: "5", user: user, version: 0, timestamp: 0 }),
        ]

        const CRDT = getCrdt();
        const char = new Character({ value: 'a', id: "1", user: user, version: 0, timestamp: 0 })
        const chInsertionIndex = CRDT._findChInsertionIndex(line, char)

        assert.deepEqual(chInsertionIndex, 0)
    })

    it("depth0_lastPosition", () => {

        const line = [
            new Character({ value: 'b', id: "2", user: user, version: 0, timestamp: 0 }),
            new Character({ value: 'c', id: "3", user: user, version: 0, timestamp: 0 }),
            new Character({ value: 'e', id: "5", user: user, version: 0, timestamp: 0 }),
        ]
        const CRDT = getCrdt();

        const char = new Character({ value: 'a', id: "6", user: user, version: 0, timestamp: 0 })
        const chInsertionIndex = CRDT._findChInsertionIndex(line, char)

        assert.deepEqual(chInsertionIndex, 3)
    })

    it("depth0_middlePosition", () => {
        const line = [
            new Character({ value: 'b', id: "2", user: user, version: 0, timestamp: 0 }),
            new Character({ value: 'c', id: "3", user: user, version: 0, timestamp: 0 }),
            new Character({ value: 'e', id: "5", user: user, version: 0, timestamp: 0 }),
        ]

        const CRDT = getCrdt();
        const char = new Character({ value: 'a', id: "4", user: user, version: 0, timestamp: 0 })
        const neighbors = CRDT._findChInsertionIndex(line, char)

        assert.deepEqual(neighbors, 2)
    })

    it("duplicateCharacter", () => {
        const line = [
            new Character({ value: 'b', id: "2", user: user, version: 0, timestamp: 0 }),
            new Character({ value: 'c', id: "3", user: user, version: 0, timestamp: 0 }),
            new Character({ value: 'e', id: "5", user: user, version: 0, timestamp: 0 }),
        ]

        const CRDT = getCrdt();
        const char = new Character({ value: 'c', id: "3", user: user, version: 0, timestamp: 0 })
        const chInsertionIndex = CRDT._findChInsertionIndex(line, char)

        assert.deepEqual(chInsertionIndex, 1)
    })
});

describe("CRDT_getIdRange_tests", () => {

    const getCrdt = () => {
        return new Crdt(20, 10)
    }

    it("getIdRange_depth0_firstPosition", () => {
        const prevCharId = []
        const nextCharId = Character.idToArray("2")
        const depth = 0

        const CRDT = getCrdt();
        const range = CRDT._getIdRange(prevCharId, nextCharId, depth)

        assert.deepEqual(range, { prevId: "0", nextId: "2" })
    })

    it("getIdRange_depth1_firstPosition", () => {
        const prevCharId = []
        const nextCharId = Character.idToArray("0.2")
        const depth = 1

        const CRDT = getCrdt();
        const range = CRDT._getIdRange(prevCharId, nextCharId, depth)

        assert.deepEqual(range, { prevId: "0.0", nextId: "0.2" })
    })

    it("getIdRange_depth2_firstPosition", () => {
        const prevCharId = []
        const nextCharId = Character.idToArray("0.1")
        const depth = 2

        const CRDT = getCrdt();
        const range = CRDT._getIdRange(prevCharId, nextCharId, depth)

        assert.deepEqual(range, { prevId: "0.0.0", nextId: "0.1.0" })
    })

    it("getIdRange_depth0_lastPosition", () => {
        const prevCharId = Character.idToArray("19")
        const nextCharId = []
        const depth = 0

        const CRDT = getCrdt();
        const range = CRDT._getIdRange(prevCharId, nextCharId, depth)

        assert.deepEqual(range, { prevId: "19", nextId: "21" })
    })

    it("getIdRange_depth1_lastPosition", () => {
        const prevCharId = Character.idToArray("20.5")
        const nextCharId = []
        const depth = 1

        const CRDT = getCrdt();
        const range = CRDT._getIdRange(prevCharId, nextCharId, depth)

        assert.deepEqual(range, { prevId: "20.5", nextId: "20.41" })
    })

    it("getIdRange_depth2_lastPosition", () => {
        const prevCharId = Character.idToArray("20.39")
        const nextCharId = []
        const depth = 1

        const CRDT = getCrdt();
        const range = CRDT._getIdRange(prevCharId, nextCharId, depth)

        assert.deepEqual(range, { prevId: "20.39", nextId: "20.41" })
    })

    it("getIdRange_depth0_emptyDoc", () => {
        const prevCharId = []
        const nextCharId = []
        const depth = 0

        const CRDT = getCrdt();
        const range = CRDT._getIdRange(prevCharId, nextCharId, depth)

        assert.deepEqual(range, { prevId: "0", nextId: "21" })
    })

    it("getIdRange_depth0_middlePosition", () => {
        const prevCharId = Character.idToArray("1")
        const nextCharId = Character.idToArray("5.3")
        const depth = 0

        const CRDT = getCrdt();
        const range = CRDT._getIdRange(prevCharId, nextCharId, depth)

        assert.deepEqual(range, { prevId: "1", nextId: "6" })
    })

    it("getIdRange_depth1_middlePosition_1", () => {
        const prevCharId = Character.idToArray("1")
        const nextCharId = Character.idToArray("2.3")
        const depth = 1

        const CRDT = getCrdt();
        const range = CRDT._getIdRange(prevCharId, nextCharId, depth)

        assert.deepEqual(range, { prevId: "1.0", nextId: "2.3" })
    })

    it("getIdRange_depth1_middlePosition_2", () => {
        const prevCharId = Character.idToArray("1.5")
        const nextCharId = Character.idToArray("2")
        const depth = 1

        const CRDT = getCrdt();
        const range = CRDT._getIdRange(prevCharId, nextCharId, depth)

        assert.deepEqual(range, { prevId: "1.5", nextId: "2.0" })
    })

    it("getIdRange_depth1_middlePosition_3", () => {
        const prevCharId = Character.idToArray("1")
        const nextCharId = Character.idToArray("2")
        const depth = 1

        const CRDT = getCrdt();
        const range = CRDT._getIdRange(prevCharId, nextCharId, depth)

        assert.deepEqual(range, { prevId: "1.0", nextId: "2.0" })
    })

    it("getIdRange_depth2_middlePosition", () => {
        const prevCharId = Character.idToArray("1.20")
        const nextCharId = Character.idToArray("2")
        const depth = 2

        const CRDT = getCrdt();
        const range = CRDT._getIdRange(prevCharId, nextCharId, depth)

        assert.deepEqual(range, { prevId: "1.20.0", nextId: "2.0.0" })
    })

    it("getIdRange_depth4_middlePosition", () => {
        const prevCharId = Character.idToArray("1.20.40.79")
        const nextCharId = Character.idToArray("2")
        const depth = 3

        const CRDT = getCrdt();
        const range = CRDT._getIdRange(prevCharId, nextCharId, depth)

        assert.deepEqual(range, { prevId: "1.20.40.79", nextId: "2.0.0.0" })
    })
})

describe("CRDT_getIdIntervall_tests", () => {

    const getCrdt = () => {
        return new Crdt(10, 5)
    }

    it("getIdIntervall_depth0_firstChar", () => {
        const prevId = "0"
        const nextId = "0"

        const CRDT = getCrdt();

        const range = CRDT._getIdIntervall(prevId, nextId)

        assert.deepEqual(range, 0)
    })


    it("getIdIntervall_depth1_firstChar", () => {
        const prevId = "0.0"
        const nextId = "0.1"

        const CRDT = getCrdt();

        const range = CRDT._getIdIntervall(prevId, nextId)

        assert.deepEqual(range, 0)
    })

    it("getIdIntervall_depth2_firstChar", () => {
        const prevId = "0.0.0"
        const nextId = "0.1.0"

        const CRDT = getCrdt();

        const range = CRDT._getIdIntervall(prevId, nextId)

        assert.deepEqual(range, 40)
    })

    it("getIdIntervall_depth0_lastChar", () => {
        const prevId = "10"
        const nextId = "11"

        const CRDT = getCrdt();

        const range = CRDT._getIdIntervall(prevId, nextId)

        assert.deepEqual(range, 0)
    })

    it("getIdIntervall_depth1_lastChar", () => {
        const prevId = "10.19"
        const nextId = "10.21"

        const CRDT = getCrdt();

        const range = CRDT._getIdIntervall(prevId, nextId)

        assert.deepEqual(range, 1)
    })

    it("getIdIntervall_depth2_lastChar", () => {
        const prevId = "10.20.38"
        const nextId = "10.20.41"

        const CRDT = getCrdt();

        const range = CRDT._getIdIntervall(prevId, nextId)

        assert.deepEqual(range, 2)
    })

    it("getIdIntervall_depth1_fullNode", () => {
        const prevId = "4.20"
        const nextId = "5.0"

        const CRDT = getCrdt();

        const range = CRDT._getIdIntervall(prevId, nextId)

        assert.deepEqual(range, 0)
    })

    it("getIdIntervall_depth2_fullNode", () => {
        const prevId = "4.20.0"
        const nextId = "5.0.0"

        const CRDT = getCrdt();

        const range = CRDT._getIdIntervall(prevId, nextId)

        assert.deepEqual(range, 40)
    })

    it("getIdIntervall_depth0_midChar", () => {
        const prevId = "1"
        const nextId = "9"

        const CRDT = getCrdt();

        const range = CRDT._getIdIntervall(prevId, nextId)

        assert.deepEqual(range, 7)
    })

    it("getIdIntervall_depth1_sameDepth0Id_intervall0", () => {
        const prevId = "4.2"
        const nextId = "4.3"

        const CRDT = getCrdt();

        const range = CRDT._getIdIntervall(prevId, nextId)

        assert.deepEqual(range, 0)
    })

    it("getIdIntervall_depth1_sameDepth0Id_intervall20", () => {
        const prevId = "4.0"
        const nextId = "4.21"

        const CRDT = getCrdt();

        const range = CRDT._getIdIntervall(prevId, nextId)

        assert.deepEqual(range, 20)
    })

    it("getIdIntervall_depth1_DifferentDepth0Id_intervall22", () => {
        const prevId = "4.2"
        const nextId = "5.5"

        const CRDT = getCrdt();

        const range = CRDT._getIdIntervall(prevId, nextId)

        assert.deepEqual(range, 22)
    })

    it("getIdIntervall_depth1_DifferentDepth0Id_intervall0", () => {
        const prevId = "10.20"
        const nextId = "10.21"

        const CRDT = getCrdt();

        const range = CRDT._getIdIntervall(prevId, nextId)

        assert.deepEqual(range, 0)
    })

    it("getIdIntervall_depth1_DifferentDepth0Id_intervall20", () => {
        const prevId = "4.0"
        const nextId = "5.0"

        const CRDT = getCrdt();

        const range = CRDT._getIdIntervall(prevId, nextId)

        assert.deepEqual(range, 20)
    })

    it("getIdIntervall_prev=0.0_next=0.1_intervall0", () => {
        const prevId = "0.0"
        const nextId = "0.1"

        const CRDT = getCrdt();

        const range = CRDT._getIdIntervall(prevId, nextId)

        assert.deepEqual(range, 0)
    })

    it("getIdIntervall_prev=10.20_next=10.21_intervall0", () => {
        const prevId = "10.20"
        const nextId = "10.21"

        const CRDT = getCrdt();

        const range = CRDT._getIdIntervall(prevId, nextId)

        assert.deepEqual(range, 0)
    })

    it("getIdIntervall_depth3_intervall1", () => {
        const prevId = "2.20.40.79"
        const nextId = "3.0.0.0"

        const CRDT = getCrdt();

        const range = CRDT._getIdIntervall(prevId, nextId)

        assert.deepEqual(range, 1)
    })
})

describe("CRDT_getNewId_tests", () => {

    const getCrdt = () => {
        return new Crdt(10, 5)
    }

    it("getNewId_depth0_intervall>1_boundary+", () => {
        const prevId = "1.0"
        const nextId = "5.3"
        const policy = 1
        const offset = 3
        const depth = 0

        const CRDT = getCrdt();

        const range = CRDT._getNewId(prevId, nextId, offset, depth, policy)

        assert.deepEqual(range, "4")
    })

    it("getNewId_depth0_intervall>1_boundary-", () => {
        const prevId = "1.0"
        const nextId = "5.3"
        const policy = -1
        const offset = 3
        const depth = 0

        const CRDT = getCrdt();

        const range = CRDT._getNewId(prevId, nextId, offset, depth, policy)

        assert.deepEqual(range, "2")
    })

    it("getNewId_depth1_intervall=1_boundary+_1", () => {
        const prevId = "4.0"
        const nextId = "5.3"
        const policy = 1
        const offset = 1
        const depth = 1

        const CRDT = getCrdt();

        const range = CRDT._getNewId(prevId, nextId, offset, depth, policy)

        assert.deepEqual(range, "4.1")
    })

    it("getNewId_depth1_intervall=1_boundary+_2", () => {
        const prevId = "4.2"
        const nextId = "5.8"
        const policy = 1
        const offset = 3
        const depth = 1

        const CRDT = new Crdt(10);

        const range = CRDT._getNewId(prevId, nextId, offset, depth, policy)

        assert.deepEqual(range, "4.5")
    })

    it("getNewId_depth1_intervall=1_boundary+_3", () => {
        const prevId = "4.12"
        const nextId = "5.8"
        const policy = 1
        const offset = 9
        const depth = 1

        const CRDT = new Crdt(10, 5);

        const range = CRDT._getNewId(prevId, nextId, offset, depth, policy)

        assert.deepEqual(range, "5.1")
    })

    it("getNewId_depth1_intervall=1_boundary-_1", () => {
        const prevId = "4.6"
        const nextId = "5.0"
        const policy = -1
        const offset = 2
        const depth = 1

        const CRDT = getCrdt();

        const range = CRDT._getNewId(prevId, nextId, offset, depth, policy)

        assert.deepEqual(range, "4.19")
    })

    it("getNewId_depth1_intervall=1_boundary-_2", () => {
        const prevId = "4.2"
        const nextId = "5.3"
        const policy = -1
        const offset = 2
        const depth = 1

        const CRDT = getCrdt();

        const range = CRDT._getNewId(prevId, nextId, offset, depth, policy)

        assert.deepEqual(range, "5.1")
    })

    it("getNewId_depth1_intervall=1_boundary-_3", () => {
        const prevId = "4.2"
        const nextId = "5.3"
        const policy = -1
        const offset = 4
        const depth = 1

        const CRDT = getCrdt();

        const range = CRDT._getNewId(prevId, nextId, offset, depth, policy)

        assert.deepEqual(range, "4.19")
    })

    it("getNewId_depth1_intervall=0_boundary+", () => {
        const prevId = "4.2"
        const nextId = "4.10"
        const policy = +1
        const offset = 3
        const depth = 1

        const CRDT = getCrdt();

        const range = CRDT._getNewId(prevId, nextId, offset, depth, policy)

        assert.deepEqual(range, "4.5")
    })

    it("getNewId_depth1_intervall=0_boundary-", () => {
        const prevId = "4.2"
        const nextId = "4.10"
        const policy = -1
        const offset = 3
        const depth = 1

        const CRDT = getCrdt();

        const range = CRDT._getNewId(prevId, nextId, offset, depth, policy)

        assert.deepEqual(range, "4.7")
    })

    it("getNewId_depth1_nodeMargin_boundary-", () => {
        const prevId = "10.19"
        const nextId = "10.21"
        const policy = -1
        const offset = 1
        const depth = 1

        const CRDT = getCrdt();

        const range = CRDT._getNewId(prevId, nextId, offset, depth, policy)

        assert.deepEqual(range, "10.20")
    })

    it("getNewId_depth3_boundary-_1", () => {
        const prevId = "4.20.40.0"
        const nextId = "5.0.0.0"
        const policy = -1
        const offset = 2
        const depth = 3

        const CRDT = getCrdt();

        const range = CRDT._getNewId(prevId, nextId, offset, depth, policy)

        assert.deepEqual(range, "4.20.40.79")
    })

    it("getNewId_depth3_boundary-_2", () => {
        const prevId = "4.20.40.79"
        const nextId = "5.0.0.0"
        const policy = -1
        const offset = 1
        const depth = 3

        const CRDT = getCrdt();

        const range = CRDT._getNewId(prevId, nextId, offset, depth, policy)

        assert.deepEqual(range, "4.20.40.80")
    })

    it("getNewId_depth3_boundary+_2", () => {
        const prevId = "4.20.40.79"
        const nextId = "5.0.0.0"
        const policy = +1
        const offset = 1
        const depth = 3

        const CRDT = getCrdt();

        const range = CRDT._getNewId(prevId, nextId, offset, depth, policy)

        assert.deepEqual(range, "4.20.40.80")
    })
    it("getNewId_depth3_boundary+_2", () => {
        const prevId = "4.20.40.79"
        const nextId = "5.0.0.0"
        const policy = +1
        const offset = 1
        const depth = 3

        const CRDT = getCrdt();

        const range = CRDT._getNewId(prevId, nextId, offset, depth, policy)

        assert.deepEqual(range, "4.20.40.80")
    })

    it("getNewId_depth2_boundary+_firstChar", () => {
        const prevId = "0.0.0"
        const nextId = "0.1.0"
        const policy = +1
        const offset = 1
        const depth = 2

        const CRDT = getCrdt();

        const range = CRDT._getNewId(prevId, nextId, offset, depth, policy)

        assert.deepEqual(range, "0.0.1")
    })

    it("getNewId_depth3_boundary-_firstChar", () => {
        const prevId = "0.0.0"
        const nextId = "0.1.0"
        const policy = -1
        const offset = 1
        const depth = 2

        const CRDT = getCrdt();

        const range = CRDT._getNewId(prevId, nextId, offset, depth, policy)

        assert.deepEqual(range, "0.0.40")
    })
    it("getNewId_depth3_boundary-_2", () => {
        const prevId = "1.19.0"
        const nextId = "1.20.1"
        const policy = -1
        const offset = 1
        const depth = 2

        const CRDT = getCrdt();

        const range = CRDT._getNewId(prevId, nextId, offset, depth, policy)

        assert.deepEqual(range, "1.19.40")
    })
})

describe("CRDT_findInsertionConditions_tests", () => {

    const getCrdt = () => {
        return new Crdt(10, 2)
    }

    it("findInsertionConditions_depth0_1", () => {
        const prevChar = new Character({ value: 'a', id: "1", user: user, version: 0, timestamp: 0 })
        const nextChar = new Character({ value: 'b', id: "3", user: user, version: 0, timestamp: 0 })

        const CRDT = getCrdt();

        const insertionCondition = CRDT._findInsertionConditions(prevChar, nextChar);

        assert.deepEqual(insertionCondition, {
            prevId: "1",
            nextId: "3",
            depth: 0,
            boundary: 1
        })
    })

    it("findInsertionConditions_depth0_2", () => {
        const prevChar = new Character({ value: 'a', id: "1.1", user: user, version: 0, timestamp: 0 })
        const nextChar = new Character({ value: 'b', id: "2.5", user: user, version: 0, timestamp: 0 })

        const CRDT = getCrdt();

        const insertionCondition = CRDT._findInsertionConditions(prevChar, nextChar);

        assert.deepEqual(insertionCondition, {
            prevId: "1",
            nextId: "3",
            depth: 0,
            boundary: 1
        })
    })

    it("findInsertionConditions_depth0_3", () => {
        const prevChar = new Character({ value: 'a', id: "0.1", user: user, version: 0, timestamp: 0 })
        const nextChar = new Character({ value: 'b', id: "1.5", user: user, version: 0, timestamp: 0 })

        const CRDT = getCrdt();

        const insertionCondition = CRDT._findInsertionConditions(prevChar, nextChar);

        assert.deepEqual(insertionCondition, {
            prevId: "0",
            nextId: "2",
            depth: 0,
            boundary: 1
        })
    })

    it("findInsertionConditions_depth0_4", () => {
        const prevChar = new Character({ value: 'a', id: "9", user: user, version: 0, timestamp: 0 })
        const nextChar = new Character({ value: 'b', id: "10.1", user: user, version: 0, timestamp: 0 })

        const CRDT = getCrdt();

        const insertionCondition = CRDT._findInsertionConditions(prevChar, nextChar);

        assert.deepEqual(insertionCondition, {
            prevId: "9",
            nextId: "11",
            depth: 0,
            boundary: 1
        })
    })

    it("findInsertionConditions_depth1", () => {
        const prevChar = new Character({ value: 'a', id: "1", user: user, version: 0, timestamp: 0 })
        const nextChar = new Character({ value: 'b', id: "2", user: user, version: 0, timestamp: 0 })

        const CRDT = getCrdt();

        const insertionCondition = CRDT._findInsertionConditions(prevChar, nextChar);

        assert.deepEqual(insertionCondition, {
            prevId: "1.0",
            nextId: "2.0",
            depth: 1,
            boundary: 2
        })
    })

    it("findInsertionConditions_depth0_firstChar", () => {
        const nextChar = new Character({ value: 'b', id: "2", user: user, version: 0, timestamp: 0 })

        const CRDT = getCrdt();

        const insertionCondition = CRDT._findInsertionConditions(undefined, nextChar);

        assert.deepEqual(insertionCondition, {
            prevId: "0",
            nextId: "2",
            depth: 0,
            boundary: 1
        })
    })

    it("findInsertionConditions_depth1_firstChar", () => {
        const nextChar = new Character({ value: 'b', id: "1", user: user, version: 0, timestamp: 0 })

        const CRDT = getCrdt();

        const insertionCondition = CRDT._findInsertionConditions(undefined, nextChar);

        assert.deepEqual(insertionCondition, {
            prevId: "0.0",
            nextId: "1.0",
            depth: 1,
            boundary: 2
        })
    })

    it("findInsertionConditions_depth2_firstChar", () => {
        const prevChar = undefined;
        const nextChar = new Character({ value: 'b', id: "0.1", user: user, version: 0, timestamp: 0 })

        const CRDT = getCrdt();

        const insertionCondition = CRDT._findInsertionConditions(prevChar, nextChar);

        assert.deepEqual(insertionCondition, {
            prevId: "0.0.0",
            nextId: "0.1.0",
            depth: 2,
            boundary: 2
        })
    })

    it("findInsertionConditions_depth3_middleChar", () => {
        const prevChar = new Character({ value: 'a', id: "2.20.40.79", user: user, version: 0, timestamp: 0 });
        const nextChar = new Character({ value: 'b', id: "3", user: user, version: 0, timestamp: 1 })

        const CRDT = getCrdt();

        const insertionCondition = CRDT._findInsertionConditions(prevChar, nextChar);

        assert.deepEqual(insertionCondition, {
            prevId: "2.20.40.79",
            nextId: "3.0.0.0",
            depth: 3,
            boundary: 1
        })
    })

    it("findInsertionConditions_depth0_lastChar", () => {
        const prevChar = new Character({ value: 'b', id: "5", user: user, version: 0, timestamp: 0 })

        const CRDT = getCrdt();

        const insertionCondition = CRDT._findInsertionConditions(prevChar, undefined);

        assert.deepEqual(insertionCondition, {
            prevId: "5",
            nextId: "11",
            depth: 0,
            boundary: 2
        })
    })

    it("findInsertionConditions_depth1_lastChar", () => {
        const prevChar = new Character({ value: 'b', id: "10.19", user: user, version: 0, timestamp: 0 })

        const CRDT = getCrdt();

        const insertionCondition = CRDT._findInsertionConditions(prevChar, undefined);

        assert.deepEqual(insertionCondition, {
            prevId: "10.19",
            nextId: "10.21",
            depth: 1,
            boundary: 1
        })
    })

    it("findInsertionConditions_depth2", () => {
        const prevChar = new Character({ value: 'b', id: "4.20", user: user, version: 0, timestamp: 0 })
        const nextChar = new Character({ value: 'c', id: "5", user: user, version: 0, timestamp: 0 })

        const CRDT = getCrdt();

        const insertionCondition = CRDT._findInsertionConditions(prevChar, nextChar);

        assert.deepEqual(insertionCondition, {
            prevId: "4.20.0",
            nextId: "5.0.0",
            depth: 2,
            boundary: 2
        })
    })

    it("findInsertionConditions_depth2_lastChar", () => {
        const prevChar = new Character({ value: 'b', id: "10.20", user: user, version: 0, timestamp: 0 })

        const CRDT = getCrdt();

        const insertionCondition = CRDT._findInsertionConditions(prevChar, undefined);

        assert.deepEqual(insertionCondition, {
            prevId: "10.20.0",
            nextId: "10.20.41",
            depth: 2,
            boundary: 2
        })
    })

    it("findInsertionConditions_depth0_noChars", () => {
        const CRDT = getCrdt();

        const insertionCondition = CRDT._findInsertionConditions(undefined, undefined);

        assert.deepEqual(insertionCondition, {
            prevId: "0",
            nextId: "11",
            depth: 0,
            boundary: 2
        })
    })
})

describe("CRDT_getPrevChar_tests", () => {



    const getCrdt = () => {
        const crdt = new Crdt(10, 2)
        const line = [
            new Character({ value: 'c', id: "0.1", user: user, version: 0, timestamp: 0 }),
            new Character({ value: 'i', id: "1", user: user, version: 0, timestamp: 1 }),
            new Character({ value: 'a', id: "2", user: user, version: 0, timestamp: 2 }),
            new Character({ value: 'o', id: "2.5", user: user, version: 0, timestamp: 3 }),
            new Character({ value: '\n', id: "3", user: user, version: 0, timestamp: 4 }),
            new Character({ value: 'p', id: "4", user: user1, version: 0, timestamp: 0 }),
            new Character({ value: 'l', id: "5", user: user1, version: 0, timestamp: 1 }),
            new Character({ value: 'u', id: "5.2", user: user1, version: 0, timestamp: 2 }),
            new Character({ value: 't', id: "5.4", user: user1, version: 0, timestamp: 3 }),
            new Character({ value: 'o', id: "6", user: user1, version: 0, timestamp: 4 }),
        ]
        line.forEach((char) => {
            crdt.remoteInsert(char)
        })

        return crdt;
    }

    it("getPrevChar_middleCharacter", () => {
        const CRDT = getCrdt();

        const line = 0
        const ch = 2

        const expected = JSON.stringify(new Character({ value: 'i', id: "1", user: user, version: 0, timestamp: 1 }))

        const prevChar = CRDT._getPrevChar(line, ch);

        assert.deepEqual(JSON.stringify(prevChar), expected)
    })

    it("getPrevChar_firstCharacterOfLine", () => {
        const CRDT = getCrdt();

        const line = 1
        const ch = 0

        const expected = JSON.stringify(new Character({value:'\n', id:"3", user:user, version:0, timestamp:4}))

        const prevChar = CRDT._getPrevChar(line, ch);

        assert.deepEqual(JSON.stringify(prevChar), expected)
    })


    it("getPrevChar_firstDocCharacter", () => {
        const CRDT = getCrdt();

        const line = 0
        const ch = 0

        const expected = undefined

        const prevChar = CRDT._getPrevChar(line, ch);

        assert.deepEqual(prevChar, expected)
    })

    it("getPrevChar_lastDocCharacter", () => {
        const CRDT = getCrdt();

        const line = 1
        const ch = 5

        const expected = JSON.stringify(new Character({value:'o', id:"6", user:user1, version:0, timestamp:4}))

        const prevChar = CRDT._getPrevChar(line, ch);

        assert.deepEqual(JSON.stringify(prevChar), expected)
    })
})

describe("CRDT_localInsert_tests", () => {

    const getCrdt = () => {
        const crdt = new Crdt(10, 2)
        const line = [
            new Character({ value: 'c', id: "0.1", user: user, version: 0, timestamp: 0 }),
            new Character({ value: 'i', id: "1", user: user, version: 0, timestamp: 1 }),
            new Character({value:'a', id:"2", user: user, version:0, timestamp: 2}),
            new Character({value:'o', id:"2.20.40.79", user: user, version:0, timestamp: 3}),
            new Character({value:'\n', id:"3", user: user, version:0, timestamp: 4}),
            new Character({value:'p', id:"4", user: user1, version:0, timestamp: 0}),
            new Character({value:'l', id:"5", user: user1, version:0, timestamp: 1}),
            new Character({value:'u', id:"5.2", user: user1, version:0, timestamp: 2}),
            new Character({value:'t', id:"5.4", user: user1, version:0, timestamp: 3}),
            new Character({value:'o', id:"6", user: user1, version:0, timestamp: 4}),
        ]
        line.forEach((char) => {
            crdt.remoteInsert(char)
        })

        return crdt;
    }

    it("localInsert_midLineInsertion", () => {
        const CRDT = getCrdt();

        const line = 0
        const ch = 2

        const prevChar = CRDT._getPrevChar(line, ch);
        const nextChar = CRDT._getNextChar(line, ch);

        const task = CRDT.localInsert('z', line, ch, user2);
        const { char } = task
        const { char: value, id } = char

        const result = prevChar.hasIdLowerThen(char) && nextChar.hasIdGraterThen(char)

        assert.deepEqual(result, true, JSON.stringify(char))
    })

    it("localInsert_midLineInsertion", () => {
        const CRDT = getCrdt();

        const line = 0
        const ch = 4

        const prevChar = CRDT._getPrevChar(line, ch);
        const nextChar = CRDT._getNextChar(line, ch);

        const task = CRDT.localInsert('z', line, ch, user2);
        const { char } = task
        const { char: value, id } = char

        const result = prevChar.hasIdLowerThen(char) && nextChar.hasIdGraterThen(char)

        assert.deepEqual(result, true, JSON.stringify(char))
    })

    it("localInsert_firstLineCharInsertion", () => {
        const CRDT = getCrdt();

        const line = 1
        const ch = 0

        const prevChar = CRDT._getPrevChar(line, ch);
        const nextChar = CRDT._getNextChar(line, ch);

        const task = CRDT.localInsert('z', line, ch, user2);
        const { char } = task
        const { char: value, id } = char

        const result = prevChar.hasIdLowerThen(char) && nextChar.hasIdGraterThen(char)

        assert.deepEqual(result, true, JSON.stringify(char))
    })

    it("localInsert_firstDocCharInsertion", () => {
        const CRDT = getCrdt();

        const line = 0
        const ch = 0

        const prevChar = CRDT._getPrevChar(line, ch);
        const nextChar = CRDT._getNextChar(line, ch);

        const task = CRDT.localInsert('z', line, ch, user2);
        const { char } = task
        const { char: value, id } = char
        const result = prevChar == undefined && nextChar.hasIdGraterThen(char)

        assert.deepEqual(result, true, JSON.stringify(char))
    })

    it("localInsert_lastDocCharInsertion", () => {
        const CRDT = getCrdt();

        const line = 1
        const ch = 5

        const prevChar = CRDT._getPrevChar(line, ch);
        const nextChar = CRDT._getNextChar(line, ch);

        const task = CRDT.localInsert('z', line, ch, user2);
        const { char } = task
        const { char: value, id } = char
        const result = prevChar.hasIdLowerThen(char) && nextChar == undefined

        assert.deepEqual(result, true, JSON.stringify(char))
    })
})

describe("CRDT_remoteInsert_tests", () => {

    const getCrdt = () => {
        const crdt = new Crdt(10, 2)
        const line = [
            new Character({ value: 'c', id: "0.1", user: user, version: 0, timestamp: 0 }),
            new Character({ value: 'i', id: "1", user: user, version: 0, timestamp: 1 }),
            new Character({ value: 'a', id: "2", user: user, version: 0, timestamp: 2 }),
            new Character({ value: 'o', id: "2.5", user: user, version: 0, timestamp: 3 }),
            new Character({ value: '\n', id: "3", user: user, version: 0, timestamp: 4 }),
            new Character({ value: 'p', id: "4", user: user1, version: 0, timestamp: 0 }),
            new Character({ value: 'l', id: "5", user: user1, version: 0, timestamp: 1 }),
            new Character({ value: 'u', id: "5.2", user: user1, version: 0, timestamp: 2 }),
            new Character({ value: 't', id: "5.4", user: user1, version: 0, timestamp: 3 }),
            new Character({ value: 'o', id: "6", user: user1, version: 0, timestamp: 4 }),
        ]
        line.forEach((char) => {
            crdt.remoteInsert(char)
        })

        return crdt;
    }

    it("remoteInsert_newLineInsertion_1", () => {
        const CRDT = getCrdt();

        const task = CRDT.remoteInsert(new Character({value:'\n', id:"1.5", user:user2, version:0, timestamp:0}));
        const { char, line, ch } = task
        const { char: value, id } = char

        const result = line == 0 && ch == 2;

        assert.deepEqual(result, true)
    })

    it("remoteInsert_newLineInsertion_2", () => {
        const CRDT = getCrdt();

        const task = CRDT.remoteInsert(new Character({value:'\n', id:"3.5", user:user2, version:0, timestamp:0}));
        const { char, line, ch } = task
        const { char: value, id } = char

        const result = line == 1 && ch == 0;

        assert.deepEqual(result, true)
    })

    it("remoteInsert_newLineInsertion_3", () => {
        const CRDT = getCrdt();

        const task = CRDT.remoteInsert(new Character({value:'\n', id:"0.0.1", user:user2, version:0, timestamp:0}));
        const { char, line, ch } = task
        const { char: value, id } = char

        const result = line == 0 && ch == 0;

        assert.deepEqual(result, true)
    })

    it("remoteInsert_duplicateCharacter_ignoreInsertion", () => {
        const CRDT = getCrdt();

        const task = CRDT.remoteInsert(new Character({value:'o', id:"2.5", user:user, version: 0, timestamp:3}));

        assert.deepEqual(task, undefined)
    })
})

describe("CRDT_merge_tests", () => {

    const getCrdt = () => {
        const crdt = new Crdt(10, 2)
        const line = [
            new Character({ value: 'c', id: "0.1", user: user, version: 0, timestamp: 0 }),
            new Character({ value: 'i', id: "1", user: user, version: 0, timestamp: 1 }),
            new Character({ value: 'a', id: "2", user: user, version: 0, timestamp: 2 }),
            new Character({ value: 'o', id: "2.5", user: user, version: 0, timestamp: 3 }),
            new Character({ value: '\n', id: "3", user: user, version: 0, timestamp: 4 }),
            new Character({ value: 'p', id: "4", user: user1, version: 0, timestamp: 0 }),
            new Character({ value: 'l', id: "5", user: user1, version: 0, timestamp: 1 }),
            new Character({ value: 'u', id: "5.2", user: user1, version: 0, timestamp: 2 }),
            new Character({ value: 't', id: "5.4", user: user1, version: 0, timestamp: 3 }),
            new Character({ value: 'o', id: "6", user: user1, version: 0, timestamp: 4 }),
        ]
        line.forEach((char) => {
            crdt.remoteInsert(char)
        })

        return crdt;
    }

    it("merge_delete2Chars_lastPosition", () => {
        const crdt = getCrdt();

        const newCrdt = [
            new Character({ value: 'c', id: "0.1", user: user, version: 0, timestamp: 0 }),
            new Character({ value: 'i', id: "1", user: user, version: 0, timestamp: 1 }),
            new Character({value: 'a', id:"2", user:user, version:0, timestamp:2}),
            new Character({value: 'o', id:"2.5", user:user, version:0, timestamp:3}),
            new Character({value: '\n', id:"3", user:user, version:0, timestamp:4}),
            new Character({value: 'p', id:"4", user:user1, version:0, timestamp:0}),
            new Character({value: 'l', id:"5", user:user1, version:0, timestamp:1}),
            new Character({value: 'u', id:"5.2", user:user1, version:0, timestamp:2}),
        ]

        const task = crdt.merge(newCrdt, 1, new CharacterBuffer());

        const flattenCrdt = JSON.stringify(_.flattenDeep(crdt.document))
        const expected = JSON.stringify(newCrdt)

        assert.deepEqual(flattenCrdt, expected)
    })

    it("merge_add2Chars_lastPosition", () => {
        const crdt = getCrdt();

        const newCrdt = [
            new Character({ value: 'c', id: "0.1", user: user, version: 0, timestamp: 0 }),
            new Character({ value: 'i', id: "1", user: user, version: 0, timestamp: 1 }),
            new Character({ value: 'a', id: "2", user: user, version: 0, timestamp: 2 }),
            new Character({ value: 'o', id: "2.5", user: user, version: 0, timestamp: 3 }),
            new Character({ value: '\n', id: "3", user: user, version: 0, timestamp: 4 }),
            new Character({ value: 'p', id: "4", user: user1, version: 0, timestamp: 0 }),
            new Character({ value: 'l', id: "5", user: user1, version: 0, timestamp: 1 }),
            new Character({ value: 'u', id: "5.2", user: user1, version: 0, timestamp: 2 }),
            new Character({ value: 't', id: "5.4", user: user1, version: 0, timestamp: 3 }),
            new Character({ value: 'o', id: "6", user: user1, version: 0, timestamp: 4 }),
            new Character({value: ' ', id:"5.2", user: user1, version:0, timestamp:2}),
            new Character({value: '!', id:"5.4", user: user1, version:0, timestamp:3}),
            new Character({value: '?', id:"6", user: user1, version:0, timestamp:4}),
        ]

        const task = crdt.merge(newCrdt, 2, new CharacterBuffer());

        const flattenCrdt = JSON.stringify(_.flattenDeep(crdt.document))
        const expected = JSON.stringify(newCrdt)

        assert.deepEqual(flattenCrdt, expected)
    })

    it("merge_missingLastChars_deletedAfterNewVersion", () => {
        const crdt = getCrdt();

        const deleted = [
            new Character({value:'t', id:"5.4", user:user1, version:0, timestamp:3}),
            new Character({value:'o', id:"6", user:user1, version:0, timestamp:4})
        ]
        const deletedBuffer = new CharacterBuffer()
        deleted.forEach((char) => deletedBuffer.insertIfMissing(char))

        const newCrdt = [
            new Character({ value: 'c', id: "0.1", user: user, version: 0, timestamp: 0 }),
            new Character({ value: 'i', id: "1", user: user, version: 0, timestamp: 1 }),
            new Character({value: 'a', id:"2", user: user, version:0, timestamp:2}),
            new Character({value: 'o', id:"2.5", user: user, version:0, timestamp:3}),
            new Character({value: '\n', id:"3", user: user, version:0, timestamp:4}),
            new Character({value: 'p', id:"4", user: user1, version:0, timestamp:0}),
            new Character({value: 'l', id:"5", user: user1, version:0, timestamp:1}),
            new Character({value: 'u', id:"5.2", user: user1, version:0, timestamp:2}),
            new Character({ value: 't', id: "5.4", user: user1, version: 0, timestamp: 3 }),
            new Character({ value: 'o', id: "6", user: user1, version: 0, timestamp: 4 }),
        ]

        deleted.forEach((char) => {
            crdt.remoteDelete(char)
        })

        const task = crdt.merge(newCrdt, 0, deletedBuffer);

        const flattenCrdt = JSON.stringify(_.flattenDeep(crdt.document))
        const expected = JSON.stringify([
            new Character({ value: 'c', id: "0.1", user: user, version: 0, timestamp: 0 }),
            new Character({ value: 'i', id: "1", user: user, version: 0, timestamp: 1 }),
            new Character({value: 'a', id:"2", user: user, version:0, timestamp:2}),
            new Character({value: 'o', id:"2.5", user: user, version:0, timestamp:3}),
            new Character({value: '\n', id:"3", user: user, version:0, timestamp:4}),
            new Character({value: 'p', id:"4", user: user1, version:0, timestamp:0}),
            new Character({value: 'l', id:"5", user: user1, version:0, timestamp:1}),
            new Character({value: 'u', id:"5.2", user: user1, version:0, timestamp:2}),
        ])

        assert.deepEqual(flattenCrdt, expected)
    })

    it("merge_delete2Chars_midPosition", () => {
        const crdt = getCrdt();

        const newCrdt = [
            new Character({ value: 'c', id: "0.1", user: user, version: 0, timestamp: 0 }),
            new Character({ value: 'i', id: "1", user: user, version: 0, timestamp: 1 }),
            new Character({value: 'a', id:"2", user: user, version:0, timestamp:2}),
            new Character({value: '\n', id:"3", user: user, version:0, timestamp:4}),
            new Character({value: 'p', id:"4", user: user1, version:0, timestamp:0}),
            new Character({value: 'u', id:"5.2", user: user1, version:0, timestamp:2}),
            new Character({ value: 't', id: "5.4", user: user1, version: 0, timestamp: 3 }),
            new Character({ value: 'o', id: "6", user: user1, version: 0, timestamp: 4 }),
        ]

        const task = crdt.merge(newCrdt, 2, new CharacterBuffer());

        const flattenCrdt = JSON.stringify(_.flattenDeep(crdt.document))
        const expected = JSON.stringify(newCrdt)

        assert.deepEqual(flattenCrdt, expected)
    })

    it("merge_add2Chars_midPosition", () => {
        const crdt = getCrdt();

        const newCrdt = [
            new Character({ value: 'c', id: "0.1", user: user, version: 0, timestamp: 0 }),
            new Character({ value: 'i', id: "1", user: user, version: 0, timestamp: 1 }),
            new Character({value: 'a', id:"2", user: user, version:0, timestamp:2}),
            new Character({value: 'o', id:"2.5", user: user, version:0, timestamp:3}),
            new Character({value: '!', id:"2.8", user:user1, version:0, timestamp:9}),
            new Character({value: '?', id:"2.9", user:user1, version:0, timestamp:10}),
            new Character({value: '\n', id:"3", user: user, version:0, timestamp:4}),
            new Character({value: 'p', id:"4", user: user1, version:0, timestamp:0}),
            new Character({value: 'l', id:"5", user: user1, version:0, timestamp:1}),
            new Character({value: 'u', id:"5.2", user: user1, version:0, timestamp:2}),
            new Character({ value: 't', id: "5.4", user: user1, version: 0, timestamp: 3 }),
            new Character({ value: 'o', id: "6", user: user1, version: 0, timestamp: 4 }),
        ]

        const task = crdt.merge(newCrdt, 1, new CharacterBuffer());

        const flattenCrdt = JSON.stringify(_.flattenDeep(crdt.document))
        const expected = JSON.stringify(newCrdt)

        assert.deepEqual(flattenCrdt, expected)
    })

    it("merge_missingLastChars_deletedAfterNewVersion", () => {
        const crdt = getCrdt();

        const deleted = [
            new Character({ value: 'i', id: "1", user: user, version: 0, timestamp: 1 }),
            new Character({value: 'p', id:"4", user:user1, version: 0, timestamp:0})
        ]
        const deletedBuffer = new CharacterBuffer()
        deleted.forEach((char) => deletedBuffer.insertIfMissing(char))

        const newCrdt = [
            new Character({ value: 'c', id: "0.1", user: user, version: 0, timestamp: 0 }),
            new Character({ value: 'i', id: "1", user: user, version: 0, timestamp: 1 }),
            new Character({value: 'a', id:"2", user: user, version:0, timestamp:2}),
            new Character({value: 'o', id:"2.5", user: user, version:0, timestamp:3}),
            new Character({value: '\n', id:"3", user: user, version:0, timestamp:4}),
            new Character({value: 'p', id:"4", user: user1, version:0, timestamp:0}),
            new Character({value: 'l', id:"5", user: user1, version:0, timestamp:1}),
            new Character({value: 'u', id:"5.2", user: user1, version:0, timestamp:2}),
            new Character({ value: 't', id: "5.4", user: user1, version: 0, timestamp: 3 }),
            new Character({ value: 'o', id: "6", user: user1, version: 0, timestamp: 4 }),
        ]

        deleted.forEach((char) => {
            crdt.remoteDelete(char)
        })

        const task = crdt.merge(newCrdt, 2, deletedBuffer);

        const flattenCrdt = JSON.stringify(_.flattenDeep(crdt.document))
        const expected = JSON.stringify([
            new Character({ value: 'c', id: "0.1", user: user, version: 0, timestamp: 0 }),
            new Character({value: 'a', id:"2", user: user, version:0, timestamp:2}),
            new Character({value: 'o', id:"2.5", user: user, version:0, timestamp:3}),
            new Character({value: '\n', id:"3", user: user, version:0, timestamp:4}),
            new Character({value: 'l', id:"5", user: user1, version:0, timestamp:1}),
            new Character({value: 'u', id:"5.2", user: user1, version:0, timestamp:2}),
            new Character({ value: 't', id: "5.4", user: user1, version: 0, timestamp: 3 }),
            new Character({ value: 'o', id: "6", user: user1, version: 0, timestamp: 4 }),
        ])

        assert.deepEqual(flattenCrdt, expected)
    })

    it("merge_sameVersion", () => {
        const crdt = getCrdt();

        const deleted = []

        const newCrdt = [
            new Character({ value: 'c', id: "0.1", user: user, version: 0, timestamp: 0 }),
            new Character({ value: 'i', id: "1", user: user, version: 0, timestamp: 1 }),
            new Character({value: 'a', id:"2", user: user, version:0, timestamp:2}),
            new Character({value: 'o', id:"2.5", user: user, version:0, timestamp:3}),
            new Character({value: '\n', id:"3", user: user, version:0, timestamp:4}),
            new Character({value: 'p', id:"4", user: user1, version:0, timestamp:0}),
            new Character({value: 'l', id:"5", user: user1, version:0, timestamp:1}),
            new Character({value: 'u', id:"5.2", user: user1, version:0, timestamp:2}),
            new Character({ value: 't', id: "5.4", user: user1, version: 0, timestamp: 3 }),
            new Character({ value: 'o', id: "6", user: user1, version: 0, timestamp: 4 }),
        ]

        const task = crdt.merge(newCrdt, 0, new CharacterBuffer());

        const flattenCrdt = JSON.stringify(_.flattenDeep(crdt.document))
        const expected = JSON.stringify([
            new Character({ value: 'c', id: "0.1", user: user, version: 0, timestamp: 0 }),
            new Character({ value: 'i', id: "1", user: user, version: 0, timestamp: 1 }),
            new Character({value: 'a', id:"2", user: user, version:0, timestamp:2}),
            new Character({value: 'o', id:"2.5", user: user, version:0, timestamp:3}),
            new Character({value: '\n', id:"3", user: user, version:0, timestamp:4}),
            new Character({value: 'p', id:"4", user: user1, version:0, timestamp:0}),
            new Character({value: 'l', id:"5", user: user1, version:0, timestamp:1}),
            new Character({value: 'u', id:"5.2", user: user1, version:0, timestamp:2}),
            new Character({ value: 't', id: "5.4", user: user1, version: 0, timestamp: 3 }),
            new Character({ value: 'o', id: "6", user: user1, version: 0, timestamp: 4 }),
        ])

        assert.deepEqual(flattenCrdt, expected)
    })

    it("merge_MixedVersionChar", () => {
        const crdt = getCrdt();

        const newCrdt = [
            new Character({ value: 'c', id: "0.1", user: user, version: 0, timestamp: 0 }),
            new Character({ value: 'i', id: "1", user: user, version: 0, timestamp: 1 }),
            new Character({value: 'a', id:"2", user: user, version:0, timestamp:2}),
            new Character({value: 'o', id:"2.5", user: user, version:0, timestamp:3}),
            new Character({value: '\n', id:"3", user: user, version:0, timestamp:4}),
            new Character({value: 'p', id:"4", user: user1, version:0, timestamp:0}),
            new Character({value: 'l', id:"5", user: user1, version:0, timestamp:1}),
            new Character({value: 'u', id:"5.2", user: user1, version:0, timestamp:2}),
            new Character({ value: 't', id: "5.4", user: user1, version: 0, timestamp: 3 }),
            new Character({ value: 'o', id: "6", user: user1, version: 0, timestamp: 4 }),
        ]

        crdt.remoteInsert(new Character({value: '!', id:"6.2", user:user, version:1, timestamp:0}))
        crdt.remoteInsert(new Character({value: '?', id:"7", user:user, version:1, timestamp:1}))

        const task = crdt.merge(newCrdt, 1, new CharacterBuffer());

        const flattenCrdt = JSON.stringify(_.flattenDeep(crdt.document))
        const expected = JSON.stringify([
            new Character({ value: 'c', id: "0.1", user: user, version: 0, timestamp: 0 }),
            new Character({ value: 'i', id: "1", user: user, version: 0, timestamp: 1 }),
            new Character({ value: 'a', id: "2", user: user, version: 0, timestamp: 2 }),
            new Character({ value: 'o', id: "2.5", user: user, version: 0, timestamp: 3 }),
            new Character({ value: '\n', id: "3", user: user, version: 0, timestamp: 4 }),
            new Character({ value: 'p', id: "4", user: user1, version: 0, timestamp: 0 }),
            new Character({ value: 'l', id: "5", user: user1, version: 0, timestamp: 1 }),
            new Character({ value: 'u', id: "5.2", user: user1, version: 0, timestamp: 2 }),
            new Character({ value: 't', id: "5.4", user: user1, version: 0, timestamp: 3 }),
            new Character({ value: 'o', id: "6", user: user1, version: 0, timestamp: 4 }),
            new Character({value: '!', id:"6.2", user:user, version:1, timestamp:0}),
            new Character({value: '?', id:"7", user:user, version:1, timestamp:1})
        ])

        assert.deepEqual(flattenCrdt, expected)
    })
})