import { assert } from 'chai'
import Character from './../../src/entities/Character'
import User from './../../src/entities/User'

const user1 = new User("user1", 1)
const user2 = new User("user2", 2)

describe("CaracterId_comparison", () => {

    it("compare_equalLengthChars_-1", () => {
        
        const char1 = new Character({value:'a', id: "2.0.1", user: user1, version:0, timestamp: 0})
        const char2 = new Character({value:'a', id: "20.0.3", user: user2, version:0, timestamp: 0})

        const comp = Character.compare(char1, char2)

        assert.equal(comp, -1)
    })

    it("compare_differentLengthChars_-1", () => {
        
        const char1 = new Character({value:'a', id: "2.0", user: user1, version:0, timestamp: 0})
        const char2 = new Character({value:'a', id: "2.0.1", user: user2, version:0, timestamp: 0})

        const comp = Character.compare(char1, char2)

        assert.equal(comp, -1)
    })


    it("compare_equalLengthChars_1", () => {

        const char1 = new Character({value:'a', id: "2.0.1", user: user1, version:0, timestamp: 0})
        const char2 = new Character({value:'a', id: "20.0.3", user: user2, version:0, timestamp: 0})

        const comp = Character.compare(char2, char1)

        assert.equal(comp, 1)
    })

    it("compare_differentLengthChars_1", () => {
        const char1 = new Character({value:'a', id: "2.0.1", user: user1, version:0, timestamp: 0})
        const char2 = new Character({value:'a', id: "20.0.3", user: user2, version:0, timestamp: 0})

        const comp = Character.compare(char2, char1)

        assert.equal(comp, 1)
    })


    it("compare_sameIdDifferentUsers_-1", () => {
        
        const char1 = new Character({value:'a', id: "2.5", user: user1, version:0, timestamp: 0})
        const char2 = new Character({value:'a', id: "2.5", user: user2, version:0, timestamp: 0})

        const comp = Character.compare(char1, char2)

        assert.equal(comp, -1)
    })

    it("compare_sameIdDifferentUsers_1", () => {

        const char1 = new Character({value:'a', id: "2.5", user: user1, version:0, timestamp: 0})
        const char2 = new Character({value:'a', id: "2.5", user: user2, version:0, timestamp: 0})

        const comp = Character.compare(char2, char1)

        assert.equal(comp, 1)
    })

    it("compare_sameIdSameUsersDifferentVersion_-1", () => {

        const char1 = new Character({value:'a', id: "2.5", user: user1, version:0, timestamp: 0})
        const char2 = new Character({value:'a', id: "2.5", user: user1, version:1, timestamp: 0})

        const comp = Character.compare(char1, char2)

        assert.equal(comp, -1)
    })

    it("compare_sameIdSameUsersDifferentVersion_+1", () => {

        const char1 = new Character({value:'a', id: "2.5", user: user1, version:0, timestamp: 0})
        const char2 = new Character({value:'a', id: "2.5", user: user1, version:1, timestamp: 0})

        const comp = Character.compare(char2, char1)

        assert.equal(comp, 1)
    })

    it("compare_sameIdSameUsersSameVersionDiffTimestamp_-1", () => {
        
        const char1 = new Character({value:'a', id: "2.5", user: user1, version:1, timestamp: 0})
        const char2 = new Character({value:'a', id: "2.5", user: user1, version:1, timestamp: 1})

        const comp = Character.compare(char1, char2)

        assert.equal(comp, -1)
    })

    it("compare_sameIdSameUsersSameVersionDiffTimestamp_1", () => {

        const char1 = new Character({value:'a', id: "2.5", user: user1, version:1, timestamp: 0})
        const char2 = new Character({value:'a', id: "2.5", user: user1, version:1, timestamp: 1})

        const comp = Character.compare(char2, char1)

        assert.equal(comp, 1)
    })

    it("compare_sameChars_0", () => {

        const char1 = new Character({value:'a', id: "2.5", user: user1, version:1, timestamp: 0})

        const comp = Character.compare(char1, char1)

        assert.equal(comp, 0)
    })

    it("compare_sameCharSameVersinDifferentUsers_-1", () => {

        const char1 = new Character({value:'a', id: "2.5", user: user1, version:1, timestamp: 10})
        const char2 = new Character({value:'a', id: "2.5", user: user2, version:1, timestamp: 0})

        const comp = Character.compare(char1, char2)

        assert.equal(comp, -1)
    })

})