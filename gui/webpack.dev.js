/* eslint-disable no-undef */
const webpack = require('webpack');
const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');

const merge = require('webpack-merge');
const common = require('./webpack.common');
const globals = require('./webpack.globals');


const DOCKER_CONFIG = false

const pathRewriteConf = (DOCKER_CONFIG)?{}:{'/api' : ''}
const serverIp = (DOCKER_CONFIG)?'172.28.0.1':'127.0.0.1'
const serverPort = (DOCKER_CONFIG)?8081:8080

const SERVER_URL = `${serverIp}:${serverPort}`

module.exports = merge(common, {
  // it's uncompatible with UglifyJsPlugin
  //devtool: 'cheap-module-source-map',
  devtool: 'source-map',
  cache: true,
  devServer: {
    contentBase: path.join(globals.BUILD_DIR),
    clientLogLevel: 'error',
    //stats: 'normal',
    stats: 'errors-warnings',
    allowedHosts: ['0.0.0.0'],
    // compress: true,
    hot: true,
    open: false,
    host: '127.0.0.1',
    port: 8000,
    proxy: {
      '/api/*':{
        target: `http://${SERVER_URL}`,
        pathRewrite: pathRewriteConf
      }
    },
  },
  watch: true,
  watchOptions: {
    poll: true,
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  plugins: [
    /*
    new PurifyCSSPlugin({
        // Give paths to parse for rules. These should be absolute!
        paths: glob.sync(path.join(__dirname, './public/*.html'))
    }),
    */

    //  Hot module replacement does not work with ExtractTextPlugin
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),

    /* new CopyWebpackPlugin(
        [
            {from: './jsons/list.json', to: './list.json'},
        ],
        {copyUnmodified: false}
    ), */
    new HtmlWebpackPlugin({
      inject: true,
      minify: {
        collapseWhitespace: false,
        removeAttributeQuotes: false,
      },
      template: './public/index.html',
    }),

    new webpack.DefinePlugin({
      // DEVELOPMENT: JSON.stringify(true),
      // SERVER_IP: JSON.stringify(SERVER_IP),
      // SERVER_PORT: SERVER_PORT,
    }),
  ],
});
