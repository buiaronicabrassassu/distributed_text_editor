/* eslint-disable no-undef */
const globals = require('./webpack.globals');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {

  stats: 'minimal',
  entry: {
    
    index: ['@babel/polyfill',`${globals.SRC_DIR}/index.jsx`],
  },
  output: {
    path: globals.BUILD_DIR,
    filename: '[name].bundle.js',
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
            presets: ['@babel/preset-env','@babel/preset-react'],
            plugins: [
              '@babel/plugin-transform-spread',
              "@babel/plugin-proposal-object-rest-spread",
              "@babel/plugin-proposal-class-properties",
              ]

          },
        },
        
      },
      /* {
        test: /\.(png|jpg|jpeg|gif|ico)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              name: './img/[name].[hash].[ext]',
            },
          },
        ],
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file-loader',
        options: {
          name: './fonts/[name].[hash].[ext]',
        },
      },*/
    ],
  },
  plugins: [

    new CopyWebpackPlugin(
        [
          //  {from: '', to: ''},
        ],
        {copyUnmodified: false},
    ),
  ],
};
