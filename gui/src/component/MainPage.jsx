import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CodeMirrorWrapper from './CodeMirrorWrapper.jsx';
import ConnectedUsers from './ConnectedUsers.jsx';
import _ from 'lodash'

import CrdtInteractor from '../CrdtInteractor.js'
import { Task } from '../dataStructures/Crdt'
import User from '../entities/User.js'
import GuiStatusController from '../GuiStatusController.js';


const colors = [
    "rgba(0, 127, 255, 0.4)",
    "rgba(229, 43, 80, 0.4)",
    "rgba(225, 191, 0, 0.4)",
    "rgba(255, 165, 0 , 0.4)",
    "rgba(123, 160, 91, 0.4)",
    "rgba(127, 255, 212, 0.4)",
    "rgba(0, 127, 255, 0.4)",
]

const pos = (line, char) => {
    return { line: line, ch: char }
}

export default class MainPage extends Component {

    constructor(props) {
        super(props);

        /** @member @type {CrdtInteractor} */
        this.interactor = props.interactor

        this.users = {}
        this.nextColorIndex = 0

        this.state = {
            users: this.users,
            status: props.statusController.status
        }

        this.wrapperChangeHandler = () => { }

        this.updateCrdt = this.updateCrdt.bind(this);
        this.onTextChangeHandler = this.onTextChangeHandler.bind(this);
        this.setCodeMirrorWrapperChangeHandler = this.setCodeMirrorWrapperChangeHandler.bind(this)
        this._getUserColor = this._getUserColor.bind(this)
        this.onStatusChange = this.onStatusChange.bind(this) 
        props.statusController.registerStatusChangeObserver(this.onStatusChange)
    }

    componentDidMount() {
        console.log("Main page mount")
        const observerId = this.interactor.addObserver(this.onTextChangeHandler, ['updateGuiEvents'])
        this.setState({
            observerId: observerId
        })
    }

    onStatusChange(newStatus){
        this.setState({status: newStatus});
    }

    setCodeMirrorWrapperChangeHandler(changeHandler) {
        if (_.isFunction(changeHandler)) {
            this.wrapperChangeHandler = changeHandler;
        }
        this.props.initializationCompleted()
    }

    /**
     * @param {Task} task 
     */
    onTextChangeHandler(task) {
        //console.log("crdt->editor:", JSON.stringify(task));
        const editorTask = this.getAdaptedTask(task)
        if (editorTask) {
            this.wrapperChangeHandler(editorTask)
        }
    }

    /** @param {Task} task */
    getAdaptedTask(task) {
        const { type, char, line, ch } = task
        const { value, user } = char;

        const position = pos(line, ch)

        if (task.type.localeCompare(Task.DELETE) == 0) {
            return new EditorTask(type, value, position);
        }
        else if (task.type.localeCompare(Task.INSERT) == 0) {
            const color = this._getUserColor(user)
            return new EditorTask(type, value, position, color)
        }
    }

    /**
     * @param {EditorTask} task 
     */
    updateCrdt(task) {

        //console.log("editor->crdt:", JSON.stringify(task));

        const { value, position } = task
        const { line, ch } = position

        if (task.isInsertTask()) {
            this.interactor.insert(value, line, ch)
            this.setState({
                document: this.interactor.crdt.document
            })
        }
        else if (task.isDeleteTask()) {
            this.interactor.delete(line, ch)
        }
    }

    /**@param {User} user*/
    _getUserColor(user) {
        const userId = user.id
        const {users,nextColorIndex} = this

        if (_.has(this.users, userId)) {
            return users[userId].color
        }
        else {
            const defaultColor = "rgba(255, 255, 255, 0.4)"
            const nextColor = (nextColorIndex < colors.length) ? colors[nextColorIndex] : defaultColor

            let newUser = {}
            newUser[userId] = {
                name: user.name,
                color: nextColor
            }

            this.users = {
                ...this.users,
                ...newUser
            }
            this.nextColorIndex += 1
            
            this.setState({users: {...this.users}})

            return nextColor
        }
    }

    getStatusMessage(status){
        console.log(status)
        switch(status){
            case GuiStatusController.INITIALIZING: return "Initializing GUI"
            case GuiStatusController.DISCONNECTING: return "Disconnecting"
            case GuiStatusController.LEADER_DOWN: return "Wait for new leader"
            
        }
    }

    render() {
        const { fileName , logoutHandler } = this.props
        const { users, status } = this.state
        
        const statusString = status.toString();
        const statusClass = (statusString!="STABLE")?"block":""

        return (
            <div className="mainDiv">
                <div className={"blockingMessages "+statusClass}>
                    <h1>{this.getStatusMessage(statusString)}</h1>
                </div>
                <header>
                    <h1>{fileName}</h1>
                    <button onClick={()=>{logoutHandler()}}>Logout</button>
                </header>
                <div className="documentDiv">
                <CodeMirrorWrapper

                    atachChangeHandler={this.setCodeMirrorWrapperChangeHandler}
                    onDocumentChanges={this.updateCrdt} />
                <ConnectedUsers users={_.values(users)} />
                </div>
            </div>
        );
    }
}

MainPage.defaultProps = {
    fileName: 'Nome del file',
    logoutHandler: ()=>{
        console.log("Null logout handler")
    }
};

MainPage.propTypes = {
    fileName: PropTypes.string,
    status: PropTypes.string.isRequired,
    logoutHandler: PropTypes.func,
    interactor: PropTypes.instanceOf(CrdtInteractor).isRequired
};

export class EditorTask {
    static INSERT = "insert"
    static DELETE = "delete"

    constructor(type, value, position, color) {
        this.type = type,
            this.value = value,
            this.position = position;
        this.color = color;

        this.isInsertTask = this.isInsertTask.bind(this)
        this.isDeleteTask = this.isDeleteTask.bind(this)
        this._checkTaskType = this._checkTaskType.bind(this)
    }

    isInsertTask() {
        return this._checkTaskType(EditorTask.INSERT)
    }

    isDeleteTask() {
        return this._checkTaskType(EditorTask.DELETE)
    }

    _checkTaskType(type) {
        return this.type.toString().localeCompare(type) == 0;
    }
}

