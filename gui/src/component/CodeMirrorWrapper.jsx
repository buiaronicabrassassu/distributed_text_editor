import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import CodeMirror from 'codemirror';
import { EditorTask } from './MainPage.jsx'

export default class CodeMirrorWrapper extends Component {

    constructor(props) {
        super(props);

        this.onValueChange = this.onValueChange.bind(this);
        this.printChar = this.printChar.bind(this);
        this.handleNewOperation = this.handleNewOperation.bind(this)
        this.state = {
            isInitialized: false,
        }
    }

    componentDidMount() {
        const { editorId, editorConfiguration } = this.props;
        const editor = CodeMirror.fromTextArea(
            document.getElementById(editorId),
            editorConfiguration);

        // The remap of the enter key avoid auto-indentation
        editor.setOption("extraKeys", {
            Enter: (cm)=>{
                cm.replaceSelection("\n")
            }
        })

        editor.on("beforeChange", this.onValueChange)

        this.setState({
            editor: editor,
        },this.initialize);
        
    }

    initialize(){
        const {atachChangeHandler} = this.props;
        atachChangeHandler(this.handleNewOperation)
    }

    /**
     * @param {CodeMirror} [editor]
     * @param {CodeMirror.EditorChangeLinkedList} [change]
    */
    onValueChange(editor, change) {
        const { from, text, origin } = change;

        // Disable the default insertion behaviour
        change.cancel();

        const isNewLine = (t) => {
            return (text.length > 1 && text[0].localeCompare("") == 0 && text[1].localeCompare("") == 0)
        }

        if (!origin.localeCompare("+input")) {
            const value = (isNewLine(text)) ? '\n' : text[0];
            this.props.onDocumentChanges(new EditorTask(EditorTask.INSERT, value, from));
        }
        else if (!origin.localeCompare("+delete")) {
            if(!from.hitSide){
                this.props.onDocumentChanges(new EditorTask(EditorTask.DELETE, '', from));
            }
            
        }
    }

    /**
     * @param {EditorTask} task 
     */
    handleNewOperation(task) {
        const editor = this.state.editor;
        editor.off("beforeChange", this.onValueChange);

        const { value, position, color } = task;
        if (task.isDeleteTask()) {
            this.deleteChar(value, position);
        }
        else if (task.isInsertTask()) {
            this.printChar(value, position, color);
        }

        editor.on("beforeChange", this.onValueChange);
    }

    deleteChar(value, from) {
        /**@type {CodeMirror} */
        const editor = this.state.editor;

        if (value.localeCompare('\n') == 0) {
            const toLine = from.line + 1;
            const toChar = 0;
            const to = { line: toLine, ch: toChar };
            editor.replaceRange("", from, to);
        }
        else {
            const toIndex = from.ch + 1;
            const to = { ...from, ch: toIndex };
            editor.replaceRange("", from, to);
        }


    }

    printChar(value, from, color) {

        const toIndex = from.ch + 1;
        const to = { ...from, ch: toIndex };
        const editor = this.state.editor;
        const val = (value.localeCompare('\n')==0)?["",""]:value;
        editor.replaceRange(value, from);
        editor.markText(from, to, { css: `background: ${color}` });
    }

    render() {
        const { editorId } = this.props;
    

        return (
            <div className="codeMirrorDiv">
                <textarea id={editorId}></textarea>
            </div>
        );
    }
}

CodeMirrorWrapper.defaultProps = {
    editorId: "editor",
    editorConfiguration: {
        mode: "null",
        lineWrapping: true,
        lineNumbers: true,
        smartIndent: false,
        electricChars: false,
        indentUnit: 0,
    }
}

CodeMirrorWrapper.propTypes = {
    editorId: PropTypes.string.isRequired,
    editorConfiguration: PropTypes.object,
    documentChanges: PropTypes.array,
    onDocumentChanges: PropTypes.func,
};