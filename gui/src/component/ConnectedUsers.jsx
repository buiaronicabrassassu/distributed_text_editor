import React, {Component} from 'react';

export default class ConnectedUsers extends Component {

    render(){
        const users = this.props.users;

        return(
            <div className="usersDiv">
                {users.map((user, index) => {
                    return <User key={user.name} username={user.name} userStyle={{backgroundColor: user.color}} />
                })}      
            </div>
        );
    }
}

class User extends Component {
    
    render(){
        return(
            <div className="userDiv">
                <span>{this.props.username} </span>
                <div className="colorDiv" style={this.props.userStyle}> </div>
            </div>
        );
    }
}
