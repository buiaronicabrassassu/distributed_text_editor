import Crdt, { Task } from './dataStructures/Crdt'
import CharacterBuffer from './dataStructures/CharacterBuffer'
import Character from './entities/Character';

export default class CrdtInteractor {

    /** @param {Crdt} crdt */
    constructor() {
        /** @member @type {Crdt}*/
        this.crdt = new Crdt()
        /** @member @type {CharacterBuffer} */
        this.localDeleteBuffer = new CharacterBuffer();
        this.remoteDeleteBuffer = new CharacterBuffer();
        this.pendingDeleteBuffer = new CharacterBuffer();

        this.user = undefined;
        this.observers = {
            updateGuiEvents: [],
            sendTaskEvents: [],
            versionIdUpdateEvents: [],
        }

        this.insert = this.insert.bind(this)
        this.addObserver = this.addObserver.bind(this)
        this.removeObserver = this.removeObserver.bind(this)
    }

    initializeCrdt({ user, version }) {
        //const {id, crdt} = version
        this.user = user;
        
        this.merge(version)
    }

    insert(value, lineIndex, chIndex) {

        const task = this.crdt.localInsert(value, lineIndex, chIndex, this.user)
        if (task) {
            this._notify('updateGuiEvents', task)
            this._notify('sendTaskEvents', task)
            this.printCrdt()
        }
    }

    delete(lineIndex, chIndex) {
        const task = this.crdt.localDelete(lineIndex, chIndex)
        if (task) {
            const { char } = task;
            this.localDeleteBuffer.insertIfMissing(char);
            this._notify('updateGuiEvents', task)
            this._notify('sendTaskEvents', task)
            this.printCrdt()
        }
    }

    /**
     * @param {Character} char 
     */
    remoteInsert(remoteChar) {
        if (this.pendingDeleteBuffer.removeIfPresent(remoteChar)) {
            return;
        }

        const task = this.crdt.remoteInsert(remoteChar);
        if (task) {
            this._notify('updateGuiEvents', task)
        }
        this.printCrdt()
    }

    /**
     * @param {Character} char 
     */
    remoteDelete(removeVersion, remoteChar) {
        this.remoteDeleteBuffer.insertIfMissing(remoteChar);
        const task = this.crdt.remoteDelete(remoteChar);
        if (task) {
            const { char } = task;
            this._notify('updateGuiEvents', task)
            this.printCrdt()
        }
        else {
            console.log("Remote Delete:", remoteChar, "is not present")
            this.pendingDeleteBuffer.insertIfMissing(remoteChar)
        }

    }

    merge(version) {
        const { id, crdt: newVersionCRDT } = version

        const deletedChars = new CharacterBuffer({
            ...this.remoteDeleteBuffer.buffer, 
            ...this.localDeleteBuffer.buffer
        })
        // const deletedChars = new CharacterBuffer({
        //     ...this.localDeleteBuffer.buffer
        // })

        const tasks = this.crdt.merge(newVersionCRDT, id, deletedChars)
        console.log("merge Tasks:");
        console.log(JSON.stringify(tasks))

        tasks.forEach(task => {
            this._notify('updateGuiEvents', task)
        })
        
        //this.remoteDeleteBuffer.reset()
    }

    setVersionId(versionId) {
        this.localDeleteBuffer.reset();
        this.crdt.crdtVersion = versionId;
        this._notify('versionIdUpdateEvents');
    }

    printCrdt(){
        const crdt = _.flattenDeep(this.crdt.document)
        .map((char)=>{
            return char.value+"("+char.getFullId()+")"
        })
        console.log(crdt)
    }


    addObserver(callback, events = []) {

        const eventList = events.filter((event) => {
            return this.observers.hasOwnProperty(event)
        })

        const idList = eventList.map((event) => {
            this.observers[event].push(callback);
        })

        return eventList.reduce((result, event, index) => {
            let newResult = { ...result };
            newResult[event] = idList[index]
            return newResult
        }, {})
    }

    removeObserver(observers) {
        observers.forEach((observerId, event) => {
            if (this.observers.hasOwnProperty(event)) {
                this.observers[event].splice(observerId, 1);
            }
        })
    }

    /** @param {Object} param */
    _notify(eventType, param = undefined) {
        this.observers[eventType].forEach((observer) => { observer(param) })
    }
}