import Character from "../entities/Character"
import _ from "lodash"
import CharacterBuffer from "./CharacterBuffer"

export default class Crdt {

    constructor(nRoot = 100, maxBoundary = 20, crdtVersion = 0) {
        this.maxBoundary = maxBoundary
        this.nRoot = nRoot
        this.document = [[]]
        this.boundaryPolicy = []

        this.crdtVersion = crdtVersion;
        this.nextTimestamp = 0

        this.remoteDelete = this.remoteDelete.bind(this);
        this.remoteInsert = this.remoteInsert.bind(this);
        this.localDelete = this.localDelete.bind(this);
        this.localInsert = this.localInsert.bind(this)
        this.toString = this.toString.bind(this)
    }

    /**
     * @param {Character} char 
     */
    remoteInsert(char) {
        if (char.version < this.crdtVersion) {
            return;
        }
        const document = this.document;
        const lineIndex = this._findLineIndex(char, document);
        const charIndex = this._findChInsertionIndex(document[lineIndex], char);

        const docChar = document[lineIndex][charIndex]
        if (docChar && docChar.isEquialTo(char)) {
            return undefined;
        }

        return this._insert(char, lineIndex, charIndex)

    }

    /**
     * @param {string} value 
     * @param {number} lineIndex 
     * @param {number} charIndex 
     * @param {User} user
     */
    localInsert(value, lineIndex, charIndex, user) {
        if (!this._isValidInsertionIndex(lineIndex, charIndex)) {
            throw new Error("Invalid Insertion Index")
        }

        const prevChar = this._getPrevChar(lineIndex, charIndex);
        const nextChar = this._getNextChar(lineIndex, charIndex);

        const { prevId, nextId, depth, boundary } = this._findInsertionConditions(prevChar, nextChar)
        const offset = Math.floor((Math.random() * boundary) + 1);

        const newId = this._getNewId(prevId, nextId, offset, depth, this._getInsertionPolicy(depth))

        const newChar = new Character({value:value, id:newId, user:user, version: this.crdtVersion, timestamp: this.nextTimestamp})
        this.nextTimestamp += 1;
        return this._insert(newChar, lineIndex, charIndex)
    }

    _insert(char, lineIndex, charIndex) {
        const document = this.document;
        const line = document[lineIndex]

        const isNewLineChar = (value) => value.localeCompare('\n') == 0
        const isLastOfLine = charIndex == line.length
        const isAfterNewLine = charIndex > 0 && isNewLineChar(line[charIndex - 1].value)

        if (isNewLineChar(char.value)) { // split the current line in two lines
            const newLine = line.slice(charIndex, line.length);
            document.splice(lineIndex + 1, 0, newLine)
            document[lineIndex].splice(charIndex, line.length)
        }

        const realLineIndex = (isLastOfLine && isAfterNewLine) ? lineIndex + 1 : lineIndex;
        const realCharIndex = (isLastOfLine && isAfterNewLine) ? 0 : charIndex;

        document[realLineIndex].splice(realCharIndex, 0, char);

        return new Task({
            type: Task.INSERT,
            version: this.crdtVersion,
            char: char, 
            line: realLineIndex,
            ch: realCharIndex
        })
    }

    remoteDelete(char) {
        const document = this.document;
        const lineIndex = this._findLineIndex(char, document);
        const line = document[lineIndex];
        const charIndex = this._findCharIndex(line, char);

        if (charIndex === undefined) {
            // throw new Error("Character not found")
            return undefined;
        }
        return this._delete(lineIndex, charIndex)
    }

    localDelete(lineIndex, charIndex) {
        if (!this._isValidCharIndex(lineIndex, charIndex)) {
            throw new Error("Invalid Insertion Index")
        }
        return this._delete(lineIndex, charIndex)
    }

    /**
     * @param {number} lineIndex 
     * @param {number} charIndex 
     * @returns {Task}
     */
    _delete(lineIndex, charIndex) {
        const document = this.document;
        const char = document[lineIndex][charIndex]
        const isNewLineChar = char.value.localeCompare('\n') == 0

        document[lineIndex].splice(charIndex, 1);

        if (isNewLineChar) {
            const nextLine = [...document[lineIndex + 1]]
            document[lineIndex] = document[lineIndex].concat(nextLine);
            document.splice(lineIndex + 1, 1)
        }

        return new Task(
            {
                type: Task.DELETE,
                version: this.crdtVersion,
                char: char, 
                line: lineIndex,
                ch: charIndex
            });
    }

    /**
     * 
     * @param {Character} prevChar 
     * @param {Character} nextChar 
     */
    _findInsertionConditions(prevChar, nextChar) {

        const _getBoundary = (intervall) => {
            return Math.min(this.maxBoundary, intervall)
        }

        if (!prevChar && !nextChar) {
            const nRoot = this.nRoot
            return {
                prevId: "0",
                nextId: `${nRoot + 1}`,
                depth: 0,
                boundary: _getBoundary(this._getIdIntervall(0, nRoot + 1))
            }
        }

        let depth = -1;
        let intervall = 0;
        let idRange = undefined;

        const prevId = (!prevChar) ? [] : Character.idToArray(prevChar.id)
        const nextId = (!nextChar) ? [] : Character.idToArray(nextChar.id)

        do {
            depth += 1;
            idRange = this._getIdRange(prevId, nextId, depth)
            intervall = this._getIdIntervall(idRange.prevId, idRange.nextId);
        } while (intervall <= 0);

        return { ...idRange, depth: depth, boundary: _getBoundary(intervall) };
    }

    /**
     * @param {Array} prevId 
     * @param {Array} nextId
     * @param {number} depth
     * @description return the id of the next and the prev characters.
     * The returned ids have as depth the value passed in the depth parameter.  
     */
    _getIdRange(prevId, nextId, depth) {

        const getLastId = (depth) => {
            return _.range(0, depth + 1)
                .map((index) => {
                    if (index == depth) {
                        return this._getMaxDepthId(depth)
                    }
                    return this._getNodeWidth(index);
                })
        }

        const getNextId = (depth) => {
            return _.range(0, depth + 1)
                .map((index) => {
                    if (index < nextId.length) {
                        if (nextId[index] != 0 && index == depth && index + 1 < nextId.length) {
                            return nextId[index] + 1
                        }
                        return nextId[index]
                    }
                    return 0;
                });
        }


        const prevIdDepthArray = new Array(depth + 1).fill(0).map((v, i) => {
            return (i < prevId.length) ? prevId[i] : v;
        });

        const isLastCharacter = nextId.length == 0
        const nextIdDepthArray = (isLastCharacter) ? getLastId(depth) : getNextId(depth)

        return {
            prevId: Character.arrayToId(prevIdDepthArray),
            nextId: Character.arrayToId(nextIdDepthArray)
        }
    }

    /**
     * @param {Character} char
     * @description Bin search that returns the index of the line that contains the char.
     */
    _findLineIndex(char, document) {
        if (document.length == 0) {
            return 0;
        }

        let start = 0;
        let end = document.length - 1;

        while (start < end) {
            const curr = Math.floor((end - start) / 2) + start;
            const currLine = document[curr];

            if (currLine.length != 0) {
                const firstChar = currLine[0];
                const lastChar = currLine[currLine.length - 1]
                if (char.hasIdGraterOrEqualThen(firstChar) && lastChar.hasIdGraterOrEqualThen(char)) {
                    return curr;
                }
                else {
                    if (char.hasIdGraterThen(lastChar)) {
                        start = curr + 1;
                    }
                    else {
                        end = curr - 1;
                    }
                }
            }
        }
        return start;
    }

    /**
     * @param {[Character]} line 
     * @param {Character} char The character to be inserted
     * @description Used in the insertion of a remote character. 
     * Bin search that returns the index of the prev and the next caracters of char in the line.
     * - If prev doesn't exists (char is the first character) return prev = undefined 
     * - If next doesn't exixts (char is the last character) return next = undefined*/
    _findChInsertionIndex(line, char) {

        /**
         * @param {Array} l 
         * @param {Character} c 
         */
        const binSerach = (l, c) => {
            if (l.length == 0) {
                return 0;
            }

            if (c.hasIdGraterThen(_.last(l))) {
                return l.length;
            }

            const mid = Math.floor((l.length) / 2)
            const midChar = l[mid];
            const compResult = Character.compare(c, midChar)

            if (compResult == 0) {
                return mid;
            }

            if (compResult < 0) {
                return binSerach(l.slice(0, mid), c)
            }
            else {
                return (mid + 1) + binSerach(l.slice(mid + 1, l.length), c)
            }
        }

        return binSerach(line, char)
    }

    /**
     * @param {[Character]} line 
     * @param {Character} char 
     * @description Used in the insertion of a local character. 
     * Bin search that returns the index of char in the line.
     * - If it doesn't exists return undefined
     */
    _findCharIndex(line, char) {
        let start = 0;
        let end = line.length - 1;

        while (start <= end) {
            const mid = Math.floor((end - Math.abs(start)) / 2)
            const curr = mid + start;
            const currentChar = line[curr];

            if (char.isEquialTo(currentChar)) {
                return curr;
            }

            if (char.hasIdGraterThen(currentChar)) {
                start = curr + 1;
            }
            else {
                end = curr - 1;
            }
        }
        return undefined;
    }

    /**
     * @param {String} prevId 
     * @param {String} nextId 
     * @param {String} offset 
     * @param {String} depth
     * @description Compute the id for a new character. The id formath is like 2.14.7
     * Each value is the position occupied in a differnt depth.
     * An cannot end with 0. 0 Is a special position that means `before all the char in that depth`
     * - 0.0.1 is ok
     * - 1.2.3.0 in NOT ok
     */
    _getNewId(prevId, nextId, offset, depth, insertionPolicy) {
        const prevIdArray = Character.idToArray(prevId)
        const nextIdArray = Character.idToArray(nextId)
        const isBoundaryPlusPolicy = insertionPolicy > 0

        if (prevIdArray.length != nextIdArray.length) {
            throw new Error("getNewId: prevId and nextId must have the same depth")
        }

        const getBoundaryPlusId = (id, offset, depth) => {
            const residual = id + offset
            if (residual >= this._getNodeWidth(depth)) {
                if (residual % this._getNodeWidth(depth) == 0) {
                    return this._getNodeWidth(depth)
                }
                return residual % this._getNodeWidth(depth)
            }
            return residual
        }

        const getBoundaryMinusId = (maxDepth) => (id, offset, depth) => {
            const realId = (id == 0) ? this._getMaxDepthId(depth) : id;
            const residual = realId - offset
            if (residual == 0) {
                return (depth == maxDepth) ? this._getNodeWidth(depth) : 0;
            }
            if (residual < 0) {
                return this._getNodeWidth(depth) - (Math.abs(residual) % this._getNodeWidth(depth))
            }
            return residual;
        }

        const getBoundaryMinusResidual = (maxDepth) => (id, offset, depth) => {
            const diff = id - offset
            if (maxDepth == depth) {
                const residual = Math.abs(_.min([diff - 1, 0]))
                return _.ceil(residual / this._getNodeWidth(depth))
            }
            else {
                const residual = Math.abs(_.min([diff, 0]))
                return _.ceil(residual / this._getNodeWidth(depth))
            }
        }

        const getBoundaryPlusResidual = (id, offset, depth) => {
            const residual = Math.abs(_.min([this._getNodeWidth(depth) - (id + offset), 0]))
            return _.ceil(residual / this._getNodeWidth(depth))
        }

        const baseIdArray = (isBoundaryPlusPolicy) ? prevIdArray : nextIdArray;
        const idArray = baseIdArray.slice(0, depth + 1)
        const getDepthId = (isBoundaryPlusPolicy) ? getBoundaryPlusId : getBoundaryMinusId(depth);
        const getResidual = (isBoundaryPlusPolicy) ? getBoundaryPlusResidual : getBoundaryMinusResidual(depth);

        let residual = offset;
        const newId = _.range(0, idArray.length)
            .reverse()
            .reduce((newId, index) => {
                if (residual == 0) {
                    return newId
                }
                const currentOffset = residual
                let tmpId = [...newId]

                residual = getResidual(tmpId[index], currentOffset, index)
                tmpId[index] = getDepthId(tmpId[index], currentOffset, index);

                return tmpId;
            }, [...idArray])

        return Character.arrayToId(newId)
    }

    /**
    * @description Example:
    * maxDeptId(1) = 20
    *  - getIntervall(6.2, 7.5) --> 20 - (2) + (5-1)
    *  - getIntervall(6.2, 6.9) --> (9 - 2) - 1 
    */
    _getIdIntervall = (prevId, nextId) => {
        if (Character.getDepth(prevId) != Character.getDepth(nextId)) {
            throw new Error("The two ids must have the same depth");
        }

        const depth = Character.getDepth(prevId);
        const id1 = Character.idToArray(prevId);
        const id2 = Character.idToArray(nextId);

        const indexes = _.range(0, depth + 1);

        return indexes.reduce((result, index) => {
            const prev = id1[index]
            const next = id2[index]

            if (result == 0) {
                if (index == depth) {
                    const last = (index > 0 && next == 0) ? this._getMaxDepthId(index) : next

                    return _.max([last - prev - 1, 0]);
                }
                return _.max([next - prev, 0]);
            }

            const nodeCapacity = this._getNodeWidth(index)
            const prevBusy = prev
            const nextBusy = (next == 0) ? nodeCapacity : nodeCapacity - next + 1

            const newResult = ((result + 1) * nodeCapacity) - prevBusy - nextBusy
            return newResult
        }, 0)
    }

    /**
     * @param {number} depth 
     * @returns {number}
     * @description return upper bound of a node in a given depth.
     * ex. if the node width is 20, the upper bound is 21.
     * 21 cannot be used as id. It means that the last position of the node is free.
     */
    _getMaxDepthId(depth) {
        return this._getNodeWidth(depth) + 1
    }

    /**
     * @param {number} depth 
     * @returns {number}
     * @description return the number of element of a node in the given depth.
     * ex. if the node width is 20 a character can have all the id from 1 to 20
     */
    _getNodeWidth(depth) {
        return parseInt(this.nRoot * Math.pow(2, depth))
    }

    _getPrevChar(lineIndex, charIndex) {
        if (charIndex > 0) {
            return this.document[lineIndex][charIndex - 1]
        }
        if (charIndex == 0 && lineIndex > 0) {
            const prevLine = this.document[lineIndex - 1]
            return this.document[lineIndex - 1][prevLine.length - 1];
        }

        return undefined

    }

    _getNextChar(lineIndex, charIndex) {
        if (charIndex <= this.document[lineIndex].length - 1) {
            return this.document[lineIndex][charIndex]
        }
        return undefined
    }

    /**
     * @description returns the insertion policy in a given depth.
     * If there is no insertion policy for that depth, the method creates a new one
     */
    _getInsertionPolicy(depth) {
        /*  A boundary policy can be:
        * -1 -> boundary- policy. Insert the new value near to the next neignbor
        * +1 -> boundary+ policy. Insert the new value near to the prev neighbor
        */
        const getNewBoundaryPolicy = () => {
            return (Math.random() < 0.5) ? +1 : -1;
        }

        while (this.boundaryPolicy.length - 1 < depth) {
            this.boundaryPolicy.push(getNewBoundaryPolicy());
        }

        return this.boundaryPolicy[depth]
    }

    _isValidCharIndex(lineIndex, charIndex) {
        const isLineInDocument = lineIndex >= 0 && lineIndex < this.document.length
        const isCharInLine = charIndex >= 0 && charIndex < this.document[lineIndex].length
        return isLineInDocument && isCharInLine
    }

    _isValidInsertionIndex(lineIndex, charIndex) {
        const isLineInDocument = lineIndex >= 0 && lineIndex < this.document.length
        const validInsertionChar = charIndex >= 0 && charIndex <= this.document[lineIndex].length

        return isLineInDocument && validInsertionChar
    }

    toString() {
        return this.document.reduce((result, line, i) => {
            const lineString = line.map((char) => {
                const isNewLineChar = char.value.localeCompare('\n') == 0;
                const value = (isNewLineChar) ? `\\n` : char.value;
                return `${value}: ${char.id}`
            }).join(", ")

            return result.concat("[", lineString, "]\n");
        }, "")
    }

    _getChar(lineIndex, charIndex) {
        if (lineIndex >= this.document.length) {
            return undefined;
        }
        const line = this.document[lineIndex]
        if (charIndex >= line.length) {
            return undefined
        }

        return line[charIndex]
    }

    /**
     * @param {Array} versCrdt 
     * @param {number} newVersionId 
     * @param {CharacterBuffer} deletedCharsBuffer 
     */
    merge(versCrdt, newVersionId, deletedCharsBuffer) {
        if (newVersionId < this.crdtVersion) {
            //a merge with an older version is possible. It can happen if the leader goes down in the middle of a new version protocol.
            console.log("merge with older version")
        }

        const isNextVersionChar = (char) => char.version >= newVersionId

        const newVersionCrdt = _.flattenDeep(versCrdt);
        let tasks = []

        let lineIndex = 0
        let charIndex = 0;
        let newVersionIndex = 0;
        while (lineIndex < this.document.length && newVersionIndex < newVersionCrdt.length) {
            while (charIndex < this.document[lineIndex].length && newVersionIndex < newVersionCrdt.length) {
                /** @constant @type {Character} */
                const currentChar = this.document[lineIndex][charIndex];
                const newVersionChar = newVersionCrdt[newVersionIndex];
                if (isNextVersionChar(currentChar)) {
                    charIndex++;
                    if (currentChar.isEquialTo(newVersionChar)) {
                        newVersionIndex++;
                    }
                }
                else {
                    if (currentChar.isEquialTo(newVersionChar)) {
                        charIndex++;
                        newVersionIndex++;
                    }
                    else if (currentChar.hasIdLowerThen(newVersionChar)) {
                        const task = this._delete(lineIndex, charIndex)
                        tasks.push(task)
                    }
                    else {
                        if (!deletedCharsBuffer.contains(newVersionChar)) {
                            const task = this._insert(newVersionChar, lineIndex, charIndex)
                            tasks.push(task)
                            charIndex++;
                        }
                        newVersionIndex++;
                    }
                }
            }
            if(newVersionIndex < newVersionCrdt.length){
                charIndex = 0;
                lineIndex++;
            }
        }

        while (lineIndex < this.document.length) {
            while (charIndex < this.document[lineIndex].length) {
                if(!isNextVersionChar(this.document[lineIndex][charIndex])){
                    const task = this._delete(lineIndex, charIndex)
                    tasks.push(task)
                }
                else{
                    charIndex++;
                }
            }
            charIndex = 0;
            lineIndex++;
        }

        while (newVersionIndex < newVersionCrdt.length) {
            const newVersionChar = newVersionCrdt[newVersionIndex]
            const lastLine = this.document.length - 1
            const lastChar = this.document[lastLine].length
            if (!deletedCharsBuffer.contains(newVersionChar)) {
                const task = this._insert(newVersionChar, lastLine, lastChar)
                tasks.push(task)
            }
            newVersionIndex++;
        }

        // this.crdtVersion = newVersionId
        this.nextTimestamp = 0;
        return tasks
    }
}

export class Task {
    static INSERT = "insert";
    static DELETE = "delete";

    /**
     * 
     * @param {String} type 
     * @param {Character} char 
     * @param {number} line 
     * @param {number} ch 
     */
    constructor({type, version, char, line, ch}) {
        this.type = type;
        this.version = version;
        this.char = char;
        this.line = line;
        this.ch = ch;

        this.isInsertTask = this.isInsertTask.bind(this)
        this.isDeleteTask = this.isDeleteTask.bind(this)
        this._checkTaskType = this._checkTaskType.bind(this)
    }

    isInsertTask() {
        return this._checkTaskType(Task.INSERT)
    }

    isDeleteTask() {
        return this._checkTaskType(Task.DELETE)
    }

    _checkTaskType(type) {
        return this.type.toString().localeCompare(type) == 0;
    }
}
