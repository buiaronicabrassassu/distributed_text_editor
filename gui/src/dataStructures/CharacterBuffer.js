import Character from '../entities/Character'
import _ from 'lodash'

export default class CharacterBuffer{
    constructor(buffer = {}){
        this.buffer = buffer
    }

    /**
     * @param {Character} char
     * @description Inset a character in the dictionary if is not yet present
     */
    insertIfMissing(char){
        const fullId = char.getFullId()
        this.buffer[fullId] = char
    }

    /**
     * @param {Character} char
     * @description Check if a character is into the buffer
     */
    contains(char){
        const fullId = char.getFullId()
        return _.has(this.buffer, fullId) && char.isEquialTo(this.buffer[fullId]);
    }

    /**
     * @param {Character} char
     * @returns {Boolean} True if the caracter has been removed
     * @description Remove a character from the buffer if present
     */
    removeIfPresent(char){
        if(this.contains(char)){
            
            const fullId = char.getFullId()
            _.unset(this.buffer, fullId)
            return true;
        }
        return false;
    }

    reset(){
        this.buffer = {}
    }
}