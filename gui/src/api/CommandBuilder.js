import CrdtInteractor from '../CrdtInteractor.js';
import { Task } from '../dataStructures/Crdt'
import User from '../entities/User'
import Character from '../entities/Character'
import GuiStatusController from '../GuiStatusController.js';

const getCharFromResponse = (char) => {
    const { value, id, user: receivedUser, version, timestamp } = char;
    const { name, id: nodeId, isOnline } = receivedUser;

    const user = new User(name, nodeId, isOnline);
    const tmp = new Character({ ...char, user: user })
    return tmp;
}

export default class CommandBuilder {
    constructor(crdtInteractor, guiStatusController) {
        this.interactor = crdtInteractor;
        this.guiStatusController = guiStatusController

        this.getCommand = this.getCommand.bind(this)
    }

    getCommand(type, data) {
        switch (type) {
            case 'crdtTask': return new CrdtTaskCommand(this.interactor, data); break;
            case 'disconnect': return new DisconnectionCommand(this.guiStatusController); break;
            case 'leaderDownStart': return new LeaderDownStartCommand(this.guiStatusController); break;
            case 'leaderDownCompleted': return new LeaderDownCompletedCommand(this.interactor, this.guiStatusController, data); break;
            case 'initializationCompleted': return new InitializeCrdtCommand(this.interactor, this.guiStatusController, data); break;
            case 'setNewVersionId': return new SetVersionIdCommand(this.interactor, data); break;
            case 'setNewVersionData': return new SetVersionDataCommand(this.interactor, data); break;
            default: return new NullCommand();
        }
    }
}

/**
 * @interface Command
 * @function execute
 */

export class CrdtTaskCommand {

    constructor(crdtInteractor, remoteData) {
        /** @property @type {CrdtInteractor} */
        this.interactor = crdtInteractor
        this.data = remoteData

        this.execute = this.execute.bind(this)
        this._parseRemoteData = this._parseRemoteData.bind(this)
    }

    execute() {
        const tasks = this._parseRemoteData()

        tasks.forEach((task) => {
            const char = task.char;
            if (char instanceof Character) {
                if (task.type == Task.INSERT) {
                    this.interactor.remoteInsert(task.char);
                }
                else if (task.type == Task.DELETE) {
                    this.interactor.remoteDelete(task.version, task.char);
                }
            }
            else {
                console.error("ERROR:", JSON.stringify(char), "is not a Character");
            }

        });
    }

    _parseRemoteData() {
        return this.data.data.map((task) => {
            const { type, version, char } = task;
            return {...task, char:getCharFromResponse(char)};
        })
    }
}

export class InitializeCrdtCommand {
    constructor(crdtInteractor, guiStatusController, remoteData) {
        /** @property @type {CrdtInteractor} */
        this.interactor = crdtInteractor
        /** @property @type {GuiStatusController} */
        this.guiStatusController = guiStatusController
        this.data = remoteData

        this.execute = this.execute.bind(this)
        this._parseRemoteData = this._parseRemoteData.bind(this)
    }

    execute() {
        //const {user, ...version} = this._parseRemoteData()
        const parsedData = this._parseRemoteData()
        this.interactor.initializeCrdt(parsedData)
        this.guiStatusController.completeInitialization();
    }

    _parseRemoteData() {

        const { user, version } = this.data
        const { id, crdt } = version

        const charCrdt = crdt.map((char) => getCharFromResponse(char))
        return {
            user: new User(user.name, user.id),
            version: { id: id, crdt: charCrdt }
        }
    }
}

export class SetVersionIdCommand {
    constructor(crdtInteractor, remoteData) {
        /** @property @type {CrdtInteractor} */
        this.interactor = crdtInteractor
        this.data = remoteData

        this.execute = this.execute.bind(this)
        this._parseRemoteData = this._parseRemoteData.bind(this)
    }

    execute() {
        const versionId = this._parseRemoteData()
        console.log("New version id received: " + JSON.stringify(versionId))
        this.interactor.setVersionId(versionId)
    }

    _parseRemoteData() {
        return this.data.versionId
    }
}

export class SetVersionDataCommand {
    constructor(crdtInteractor, remoteData) {
        /** @property @type {CrdtInteractor} */
        this.interactor = crdtInteractor
        this.data = remoteData

        this.execute = this.execute.bind(this)
        this._parseRemoteData = this._parseRemoteData.bind(this)
    }

    execute() {
        const version = this._parseRemoteData();
        console.log("New version to merge received: ");
        console.log(version.crdt.map((char) => char.value));
        this.interactor.merge(version);
    }

    _parseRemoteData() {
        const { id, crdt } = this.data.version
        const charCrdt = crdt.map((char) => getCharFromResponse(char))
        return { id: id, crdt: charCrdt }
    }
}

export class DisconnectionCommand {
    constructor(guiStatusController) {
        /** @property @type {GuiStatusController} */
        this.guiStatusController = guiStatusController;
    }
    execute() {
        this.guiStatusController.completeDisconnection()
        alert("disconnection completed")
        window.open('', '_self', '');
        window.close();
    }
}

export class LeaderDownStartCommand {
    constructor(guiStatusController) {
        /** @property @type {GuiStatusController} */
        this.guiStatusController = guiStatusController
    }
    execute() {
        this.guiStatusController.startLeaderDown();
    }
}

export class LeaderDownCompletedCommand {
    constructor(interactor, guiStatusController, data) {
        /** @property @type {CrdtInteractor} */
        this.interactor = interactor;
        /** @property @type {GuiStatusController} */
        this.guiStatusController = guiStatusController;
        this.data = data;
    }

    execute() {
        const version = this._parseRemoteData();
        this.interactor.merge(version);
        this.guiStatusController.completeLeaderDown();
    }

    _parseRemoteData() {
        const { id, crdt } = this.data.version
        const charCrdt = crdt.map((char) => getCharFromResponse(char))
        return { id: id, crdt: charCrdt }
    }
}

export class NullCommand {
    constructor() { }
    execute() { }
}

