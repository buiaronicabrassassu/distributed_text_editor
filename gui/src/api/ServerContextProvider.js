
const contexts = {
    getInitData: 'getInitData',
    getCommand: "getCommand",
    sendCrdtTask: "sendLocalTask",
    disconnect: "disconnect"
}

export default class ServerContextProvider{
    constructor(serverAddress=""){
        this._serverAddress= serverAddress
    } 
    
    getUrlFor(context){
        if (_.has(contexts, context)) {
            return `${this._serverAddress}/api/${contexts[context]}`
        }
        throw new Error(`The context \"${context}\" is not valid`)
    }
}




