import axios from 'axios';
import ContextProvider from './ServerContextProvider'

export class SessionApi{
    constructor(contextProvider){
        /** @type {ContextProvider}*/
        this._contextProvider = contextProvider;
    }

    sendLogoutRequest(){
        
        console.log("Sending logout request")
        axios.post(this._contextProvider.getUrlFor("disconnect"));
    }
}