import axios from 'axios';

import CrdtInteractor from '../CrdtInteractor.js';
import { Task } from '../dataStructures/Crdt';
import CommandBuilder from './CommandBuilder'
import GuiStatusController from '../GuiStatusController.js';

const contexts = {
    getInitData: 'getInitData',
    getCommand: "getCommand",
    sendCrdtTask: "sendLocalTask",
    sendNewVersionIdAck: "sendNewVersionIdAck",
}

export default class RemoteApi {

    constructor(interactor, guiStatusController, serverAddress = "") {
        /** @member @type {CrdtInteractor} */
        this.interactor = interactor;
        /** @member @type {GuiStatusController} */
        this.guiStatusController = guiStatusController;
        this.serverAddress = serverAddress
        this.commandBuilder = new CommandBuilder(interactor, guiStatusController)

        this.sendCrdtTask = this.sendCrdtTask.bind(this);
        this.sendVersionIdUpdateAck = this.sendVersionIdUpdateAck.bind(this);
        this.requireCommands = this.requireCommands.bind(this)
        this.handleCommandResponse = this.handleCommandResponse.bind(this);
        this.handleResponseError = this.handleResponseError.bind(this);

        this.sendTaskObserverId = this.interactor.addObserver(this.sendCrdtTask, ['sendTaskEvents']);

        this.versionIdUpdateObserverId = this.interactor.addObserver(this.sendVersionIdUpdateAck, ['versionIdUpdateEvents']);
    }


    /**
     * @description It sends locally created task to the server
     * 
     * @param {Task} task
     **/
    sendCrdtTask(task) {
        const {type, version, char} = task;

        const remoteTask = {type: type, version: version, char:char};
        if (remoteTask) {
            axios.post(this._getUrl('sendCrdtTask'), remoteTask);
        }
    }

    sendVersionIdUpdateAck() {
        console.log("Send new version id ack");
        axios.post(this._getUrl('sendNewVersionIdAck'), "");
    }

    getInitData() {
        console.log("Send get init data");
        axios.get(this._getUrl(contexts.getInitData))
    }

    /**
     * It requires tasks to server.
     */
    requireCommands() {
        axios.get(this._getUrl(contexts.getCommand))
            .then(this.handleCommandResponse)
            .catch(this.handleResponseError);
    }

    /**
     * If the server don't have a character to send, the function receive a data equal to null.
     * Otherwise it receive a remote task object.
     */
    handleCommandResponse(response) {
        const { data: commandResponse } = response
        /** @constant @type {Array} */
        const { type, ...data } = commandResponse

        /** @constant @implements {Command} */
        const command = this.commandBuilder.getCommand(type, data)
        command.execute()
    }

    handleResponseError(error) {
        console.log(error);
    }

    /**
     * Call periodically the requireData function 
     */
    start(waitInterval) {
        setInterval(() => {
            this.requireCommands()
        }, waitInterval);
    }

    _getUrl(context) {
        if (_.has(contexts, context)) {
            return `${this.serverAddress}/api/${contexts[context]}`
        }
        throw new Error(`The context \"${context}\" is not valid`)
    }
}