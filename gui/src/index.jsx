// eslint-disable-next-line no-undef
require('./../scss/style.scss');

require('./../node_modules/codemirror/lib/codemirror.css')


import React from 'react';
import ReactDOM from 'react-dom';
import MainPage from './component/MainPage.jsx';

import User from './entities/User'
import Crdt from './dataStructures/Crdt'
import CrdtInteractor from './CrdtInteractor'
import RemoteApi from './api/RemoteApi'
import ContextProvider from './api/ServerContextProvider'
import { SessionApi } from './api/SessionApi.js';
import GuiStatusController from './GuiStatusController.js';

const user1 = new User("Alex", 1);
const user2 = new User("Simingione", 2);

const interactor = new CrdtInteractor();
const contextProvider = new ContextProvider()
const sessionApi = new SessionApi(contextProvider);
const guiStatusController = new GuiStatusController();
const remoteApi = new RemoteApi(interactor, guiStatusController);


const initializationCompletedHandler = () => {
    remoteApi.start(2000);
    remoteApi.getInitData();
}

const logoutHandler = ()=>{
    guiStatusController.startDisconnection()
    sessionApi.sendLogoutRequest()
}

ReactDOM.render(<MainPage
    statusController={guiStatusController}
    interactor={interactor}
    logoutHandler={logoutHandler}
    initializationCompleted={() => initializationCompletedHandler()}
/>, document.getElementById('root'));

