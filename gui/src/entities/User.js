
export default class User{
    
    /**
     * @param {String} name 
     * @param {number} id 
     * @param {boolean} isOnline 
     */
    constructor(name, id, isOnline=false){

        this.name = name;
        this.id = id;
        this.isOnline = isOnline;
    }
}