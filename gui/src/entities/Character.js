import User from "./User"
import _ from "lodash" 

export default class Character{
    /**
     * @param {String} char 
     * @param {String} id 
     * @param {User} user
     * @param {number} version
     * @param {number} timestamp
     */
    constructor({value, id, user, version, timestamp}){
        if(_.isNil(value) || _.isNil(id)||_.isNil(version) || _.isNil(timestamp)){
            throw Error("Invalid construction parameters for character")
        }
        this.value = value;
        this.id = id;

        /** @property @type {User} */
        this.user = user

        this.version = version
        this.timestamp = timestamp

        this.hasIdGraterThen = this.hasIdGraterThen.bind(this);
        this.hasIdGraterOrEqualThen = this.hasIdGraterOrEqualThen.bind(this);
        this.isEquialTo = this.isEquialTo.bind(this);
    }

    /** 
     * @param {Character} char 
     */
    hasIdGraterThen(char){
        return Character.compare(this, char) == 1
    }

    /** 
     * @param {Character} char 
     */
    hasIdLowerThen(char){
        return Character.compare(this, char) == -1
    }

    /**
     * @param {Character} char 
     */
    isEquialTo(char){
        return Character.compare(this, char) == 0
    }

    /** 
     * @param {Character} char 
     */
    hasIdGraterOrEqualThen(char){
        return this.hasIdGraterThen(char) || this.isEquialTo(char)
    }

    /** 
     * @param {Character} char 
     */
    hasIdLowerOrEqualThen(char){
        return this.hasIdLowerThen(char) || this.isEquialTo(char)
    }

    /**
     * 
     * @param {Character} char1 
     * @param {Character} char2 
     * @returns {Number} 
     *  - 0 : equals
     *  - -1: char1 < char2
     *  - +1: char1 > char2
     */
    static compare(char1, char2){
        const getFullId = (idArray, dim)=>{
            const zeros = Array(dim).fill(0)
            return zeros.map((v,i)=>{
                return (i<idArray.length)?idArray[i]:v   
            });
        }

        const maxIdLength = _.max([Character.getDepth(char1.id), Character.getDepth(char2.id)])+1
        const id1 = getFullId(Character.idToArray(char1.id), maxIdLength)
        const id2 = getFullId(Character.idToArray(char2.id), maxIdLength)

        const fullId1 = [...id1, char1.user.id, char1.version , char1.timestamp]
        const fullId2 = [...id2, char2.user.id, char2.version, char2.timestamp]

        return fullId1.reduce((result, id1Val, index)=>{
            if(result != 0){
                return result;
            }

            const id2Val = fullId2[index]
            if(id1Val != id2Val){
                return (id1Val>id2Val)?1:-1
            }
            return result;
        },0)
    }

    getDepth(){
        return Character.getDepth(this.id)
    }

    /**
     * @param {string} id
     * @returns {number} --> 0 means the root node
     */
    static getDepth(id){
        return parseInt(id.toString().split(".").length) -1;
    }

    static idToArray(id){
        const idArray = id.toString().split(".").map((v)=>parseInt(v));
        return idArray;
    }

    getFullId(){
        const fullIdArray = [
            ...Character.idToArray(this.id),
            this.user.id,
            this.version,
            this.timestamp
        ];

        return Character.arrayToId(fullIdArray)
    }

    /**
     * 
     * @param {Array} arrayId 
     */
    static arrayToId(arrayId){
        return arrayId.join('.');
    }

}