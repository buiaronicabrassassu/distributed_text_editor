

export default class GuiStatusController{
    static INITIALIZING = "INITIALIZING"
    static DISCONNECTING = "DISCONNECTING"
    static LEADER_DOWN = "LEADER_DOWN"
    static STABLE = "STABLE"

    constructor(){
        this.status = GuiStatusController.INITIALIZING;

        this.completeInitialization = this._setStatus.bind(this,GuiStatusController.STABLE)
        this.completeDisconnection =  this._setStatus.bind(this,GuiStatusController.STABLE)
        this.completeLeaderDown =  this._setStatus.bind(this,GuiStatusController.STABLE)
        this.startDisconnection = this._setStatus.bind(this, GuiStatusController.DISCONNECTING)
        this.startLeaderDown = this._setStatus.bind(this, GuiStatusController.LEADER_DOWN)
        this.registerStatusChangeObserver = this.registerStatusChangeObserver.bind(this)
        this.observers = [];
    }

    registerStatusChangeObserver(observer){
        this.observers.push(observer)
    }

    completeInitialization(){}
    startDisconnection(){}
    completeDisconnection(){}
    
    startLeaderDown(){}
    completeLeaderDown(){}

    _setStatus(status){
        console.log("New Status:",status);
        this.status = status;
        this.observers.forEach((callbackfn)=>{
            callbackfn(status)
        })
    }
    
}