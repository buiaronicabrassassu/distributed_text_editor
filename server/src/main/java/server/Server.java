package server;

import java.rmi.Remote;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.CompletableFuture;

import entities.Node;
import rmiinterfaces.ServerDocumentService;
import server.rmiobject.IServerDocumentService;

public class Server {

    /*public static Document getInitializedDocument() {
        final State documentState = State.CLOSED;
        final Version currentVersion = Version.getTestVersion();
        final Node leader = null;

        final Document document = new Document(documentState, currentVersion, leader);

        return document;
    }*/

    public static void main(final String[] args) {
        String ipAddress = args[0];
        System.setProperty("java.rmi.server.hostname", ipAddress);

        try {

            IServerDocumentService documentService = new IServerDocumentService();

            Registry reg = LocateRegistry.createRegistry(1099);

            reg.bind(ServerDocumentService.SERVICE_NAME, documentService);

        } catch (final Exception e) {
            e.printStackTrace();
        }

        System.out.println("\nThe server is ready");
    }
}
