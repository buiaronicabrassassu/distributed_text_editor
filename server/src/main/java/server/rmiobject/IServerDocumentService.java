package server.rmiobject;

import java.rmi.RemoteException;

import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import entities.Node;
import entities.Version;
import rmiinterfaces.ServerDocumentService;

public class IServerDocumentService extends UnicastRemoteObject implements ServerDocumentService {

    private final AtomicReference<Node> leader = new AtomicReference<>();
    private final AtomicReference<Version> currentVersion = new AtomicReference<>();
    private final AtomicBoolean pendingDisconnectionRequest = new AtomicBoolean(false);

    private Future<?> leaderElectionTimer;
    private final ExecutorService executorService = Executors.newFixedThreadPool(5);

    public IServerDocumentService() throws RemoteException {
        super();
        currentVersion.set(Version.getTestVersion());
    }

    @Override
    public Node getLeader(Node node) throws RemoteException {
        return leader.updateAndGet((currentLeader) -> (currentLeader == null)? node: currentLeader);
    }

    //Called by the leader when there are other nodes connected
    @Override
    public synchronized void disconnectLeader(Node node) throws RemoteException {
        System.out.println(node.getUserName()+ " has required a leader disconnection");
        if(!leader.get().equals(node)){
            return;
        }

        pendingDisconnectionRequest.set(true);
        leaderElectionTimer = executorService.submit(() -> {
            awaitLeaderElectionOrCloseDocument(node);
        });

        System.out.println(node.getUserName()+ " is no more the leader");
    }

    @Override
    public synchronized Node electNewLeader(Node oldLeaderNode, Node node) throws RemoteException {
        System.out.println("Elect new leader received from "+ node.getUserName());

        if(!pendingDisconnectionRequest.get()){
            return leader.get();
        }

        Node leader = this.leader.updateAndGet((currLeader)->currLeader.equals(oldLeaderNode)?node:currLeader);
        if(leader.equals(node)){
            leaderElectionTimer.cancel(true);
            pendingDisconnectionRequest.set(false);

            System.out.println(node.getUserName()+" is the new leader");

        }
        return leader;
    }

    private void awaitLeaderElectionOrCloseDocument(Node oldLeader){
        try {
            Thread.sleep(15000);

            synchronized (this){
                if(pendingDisconnectionRequest.get()){
                    if(leader.updateAndGet((leader)->leader.equals(oldLeader)?null:leader) == null){
                        System.out.println("Force document close");
                        pendingDisconnectionRequest.set(false);
                    }
                }
            }
        } catch (InterruptedException e){
            System.out.println("Leader election timer interrupted");
        }
    }

    //Called by the leader when is the unique node of the document
    @Override
    public void close(Node node) throws RemoteException {
        System.out.println(node.getUserName()+ " has required a document close");
        leader.updateAndGet((leader)->leader.equals(node)?null:leader);
    }

    @Override
    public Node leaderDown(Node oldLeader, Node node) throws RemoteException {
        System.out.println("Handle leader down request from node: "+node.getUserName());
        System.out.println("Old leader: "+oldLeader.getUserName());

        return leader.updateAndGet((leader)->leader.equals(oldLeader)?node:leader);
    }

    @Override
    public Version getLastVersion() throws RemoteException {
        return currentVersion.get();
    }

    @Override
    public void updateVersion(Version version) throws RemoteException {
        currentVersion.set(version);
        System.out.println("============== Server version updated ============");
        System.out.println(version.toString());
        System.out.println("============== ====================== ============");
    }

}
