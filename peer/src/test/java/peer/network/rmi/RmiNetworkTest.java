package peer.network.rmi;

import entities.Node;
import entities.User;
import org.junit.Test;
import peer.Log;

import static org.junit.Assert.*;


public class RmiNetworkTest {


    private User user1 = new User("user1", 1);
    private User user2 = new User("user2", 2);
    private User user3 = new User("user3", 3);
    private Node node1 = new Node(user1,"",1);
    private Node node2 = new Node(user2,"",1);
    private Node node3 = new Node(user3,"",1);

    public RmiNetworkTest(){
    }

    @Test
    public void addPeerIfAbsent() {
    }

    @Test
    public void removeNode() {
        RmiNetwork network = new RmiNetwork(node1,"",2);
        network.setLeader(node2);
        network.addPeerIfAbsent(node3);
        network.removeNode(node3);
        network.removeNode(node1);
        network.removeNode(node2);

        Log.print(String.valueOf(network.getLeaderPeer().getNodeId()));
        assertTrue(true);
    }

    @Test
    public void setLeader() {
    }

    @Test
    public void getLeaderPeer() {
    }
}
