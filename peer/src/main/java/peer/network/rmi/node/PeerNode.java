package peer.network.rmi.node;

import entities.Node;
import entities.Task;
import entities.Version;
import rmiinterfaces.PeerLoginService;
import rmiinterfaces.PeerNewVersionService;
import rmiinterfaces.PeerNodeDisconnectionService;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.function.Function;

public class PeerNode extends DocumentNode {

    public PeerNode(Node node) {
        super(node);
    }

    public void sendNewVersionMessage(int versionId, int nodeId, List<Task> updates){
        String activityName = "SendNewVersionMessage";

        try {
            getPeerNewVersionService().newVersion(versionId, nodeId, updates);
        } catch (Exception e) {
            throw new RuntimeException("Rmi error in activity " + activityName);
        }
    }

    public void sendNewVersionAck(int versionId){
        String activityName = "SendNewVersionAck";

        try {
            getPeerNewVersionService().newVersionAck(versionId);
        } catch (Exception e) {
            throw new RuntimeException("Rmi error in activity " + activityName);
        }

    }

    public void sendNewVersionFail(int versionId){
        String activityName = "SendNewVersionFail";

        try {
            getPeerNewVersionService().newVersionFail(versionId);
        } catch (Exception e) {
            throw new RuntimeException("Rmi error in activity " + activityName);
        }
    }

    public void sendLoginAck(Version currentVersion, List<Node> peers) {
        String activityName = "SendLoginAck";

        try {
            getLoginService().loginAck(currentVersion, peers);
        } catch (Exception e) {
            throw new RuntimeException("Rmi error in activity " + activityName);
        }
    }

    public void notifyNewNode(Node newNode){
        String activityName = "NotifyNewNode";

        try {
            getLoginService().notifyNewNode(newNode);
        } catch (Exception e) {
            throw new RuntimeException("Rmi error in activity " + activityName);
        }
    }

    public void disconnectNode(Node node, List<Task> nodeTasks){
        String activityName = "DisconnectNode";

        try {
            getDisconnectionService().disconnectNode(node, nodeTasks);
        } catch (Exception e) {
            throw new RuntimeException("Rmi error in activity " + activityName);
        }
    }

    public void sendDisconnectionConfirm(){
        String activityName = "DisconnectionConfirm";

        try {
            getDisconnectionService().disconnectionAck();
        } catch (Exception e) {
            throw new RuntimeException("Rmi error in activity " + activityName);
        }
    }

    private PeerLoginService getLoginService() throws RemoteException, NotBoundException {
        String serviceName = PeerLoginService.SERVICE_NAME;
        return (PeerLoginService) this.getRmiRegistry().lookup(serviceName);
    }

    private PeerNodeDisconnectionService getDisconnectionService() throws RemoteException, NotBoundException {
        String serviceName = PeerNodeDisconnectionService.SERVICE_NAME;
        return (PeerNodeDisconnectionService) this.getRmiRegistry().lookup(serviceName);
    }

    private PeerNewVersionService getPeerNewVersionService() throws RemoteException, NotBoundException {
        String serviceName = PeerNewVersionService.SERVICE_NAME;
        return (PeerNewVersionService) this.getRmiRegistry().lookup(serviceName);
    }
}
