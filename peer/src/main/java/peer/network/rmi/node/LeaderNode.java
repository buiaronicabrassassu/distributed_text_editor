package peer.network.rmi.node;

import entities.Node;
import entities.Task;
import peer.Log;
import rmiinterfaces.LeaderNewVersionService;
import rmiinterfaces.LeaderNodeDisconnectionService;
import rmiinterfaces.LeaderNodeLoginService;
import rmiinterfaces.LeaderPeerDownService;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.function.Function;

public class LeaderNode extends DocumentNode {

    public LeaderNode(Node node){
        super(node);
    }

    public void sendNewVersionMessage(int versionId, int nodeId, List<Task> updates){
        String activityName = "SendNewVersionMessageToLeader";
        try {
            getLeaderNewVersionService().newVersion(versionId, nodeId, updates);

        } catch (Exception e) {
            Log.print("Send new version message failed");
            throw new RuntimeException("Rmi error in activity " + activityName);
        }
    }

    public void sendNewVersionFail(int versionId){
        String activityName = "SendNewVersionFailToLeader";

        try {
            getLeaderNewVersionService().newVersionFail(versionId);
        } catch (Exception e) {
            throw new RuntimeException("Rmi error in activity " + activityName);
        }
    }

    public void sendLoginRequest(Node user){
        String activityName = "SendLoginRequestToLeader";

        try {
            System.out.println("Send login requesto to "+node.getUserName());
            getNodeLoginService().login(user);
        }catch (RemoteException e) {
            System.out.println("Remote exception");
            e.printStackTrace();
            throw new RuntimeException("Rmi error in activity " + activityName+" due to remote Exeption");
        } catch (NotBoundException e) {
            System.out.println("NotBownd exception exception");
            e.printStackTrace();
            throw new RuntimeException("Rmi error in activity " + activityName+" due NotBoundException");
        }
    }

    public void sendDisconnectionRequest(Node node, List<Task> nodeTasks){
        String activityName = "SendDisconnectionRequestToLeader";

        try {
            getDisconnectionService().disconnectionRequest(node, nodeTasks);
        } catch (Exception e) {
            throw new RuntimeException("Rmi error in activity " + activityName);
        }
    }

    public void sendPeerDown(Node peer){
        String activityName = "SendPeerDownToLeader";

        try {
            getPeerDownService().peerDown(peer);
        } catch (Exception e) {
            throw new RuntimeException("Rmi error in activity " + activityName);
        }
    }

    public void waitForLoginService(){
        try {
            getNodeLoginService();
        } catch (RemoteException | NotBoundException  e) {
            throw new RuntimeException("The login service is not yet ready");
        }
    }

    private LeaderPeerDownService getPeerDownService() throws RemoteException, NotBoundException {
        String serviceName = LeaderPeerDownService.SERVICE_NAME;
        return (LeaderPeerDownService) this.getRmiRegistry().lookup(serviceName);

    }

    private LeaderNodeLoginService getNodeLoginService() throws RemoteException, NotBoundException {
        String serviceName = LeaderNodeLoginService.SERVICE_NAME;
        return (LeaderNodeLoginService) this.getRmiRegistry().lookup(serviceName);
    }

    private LeaderNodeDisconnectionService getDisconnectionService() throws RemoteException, NotBoundException {
        String serviceName = LeaderNodeDisconnectionService.SERVICE_NAME;
        return  (LeaderNodeDisconnectionService) this.getRmiRegistry().lookup(serviceName);
    }

    private LeaderNewVersionService getLeaderNewVersionService() throws RemoteException, NotBoundException {
        String serviceName = LeaderNewVersionService.SERVICE_NAME;
        return (LeaderNewVersionService) this.getRmiRegistry().lookup(serviceName);
    }
}
