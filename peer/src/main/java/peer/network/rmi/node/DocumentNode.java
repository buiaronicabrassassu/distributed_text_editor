package peer.network.rmi.node;

import entities.Node;
import entities.Task;
import peer.Log;

import rmiinterfaces.HeartBeatService;
import rmiinterfaces.TaskNotificationService;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;


public class DocumentNode {

    public final Node node;

    public DocumentNode(Node node){
        this.node = node;
    }

    public int getNodeId(){
        return node.user.id;
    }

    public void sendTask(Task task){
        String activityName = "SendTask";
        try {
            getTaskNotificationService().notifyTask(task);
        } catch (Exception e) {
            Log.print("Task notification to "+node.getUserName()+" failed");
            throw new RuntimeException("Rmi error in activity " + activityName);
        }

    }

    public void sendHeartBeat() {
        String activityName = "SendHeartBeat";
        try {
            getHeartbeatService().heartBeat();
            Log.printError("HEART BEAT: "+node.getUserName()+" is up");
        } catch (Exception e) {
            throw new RuntimeException("Rmi error in activity " + activityName);
        }
    }

    protected final Registry getRmiRegistry() throws RemoteException {
        return LocateRegistry.getRegistry(node.ipAddress, node.registryPort);
    }

    private HeartBeatService getHeartbeatService() throws RemoteException, NotBoundException {
        String serviceName = HeartBeatService.SERVICE_NAME;
        Registry registry = LocateRegistry.getRegistry(node.ipAddress,node.registryPort);
        return (HeartBeatService) registry.lookup(serviceName);
    }

    private TaskNotificationService getTaskNotificationService() throws RemoteException, NotBoundException {
        String serviceName = TaskNotificationService.SERVICE_NAME;
        Registry registry = LocateRegistry.getRegistry(node.ipAddress,node.registryPort);
        return (TaskNotificationService) registry.lookup(serviceName);
    }


}
