package peer.network.rmi.node;

import entities.Node;
import entities.Version;
import rmiinterfaces.ServerDocumentService;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ServerNode {

    private final String serverAddress;
    private final int serverRegistryPort;

    public ServerNode(String serverAddress, int serverRegistryPort) {
        this.serverAddress = serverAddress;
        this.serverRegistryPort = serverRegistryPort;

    }

    public void sendNewVersion(Version newVersion){
        try {
            getServerDocumentService().updateVersion(newVersion);
        } catch (RemoteException e) {
            e.printStackTrace();
            throw new RuntimeException("Error during new version update into the server", e.getCause());
        }
    }

    public Node getDocumentLeader(Node currentNode){
        try {
            Node leader = getServerDocumentService().getLeader(currentNode);
            return leader;
        } catch (RemoteException e) {
            e.printStackTrace();
            throw new RuntimeException("Server communication error", e.getCause());
        }
    }

    public Version getCurrentVersion(){
        try {
            return getServerDocumentService().getLastVersion();
        } catch (RemoteException e) {
            e.printStackTrace();
            throw new RuntimeException("Server communication error", e.getCause());
        }
    }

    public void updateVersion(Version version){
        try {
            getServerDocumentService().updateVersion(version);
        } catch (RemoteException e) {
            e.printStackTrace();
            throw new RuntimeException("Server communication error", e.getCause());
        }
    }

    public void disconnectLeader(Node leader){
        try {
            getServerDocumentService().disconnectLeader(leader);
        } catch (RemoteException e) {
            e.printStackTrace();
            throw new RuntimeException("Server communication error", e.getCause());
        }
    }

    public Node sendLeaderDown(Node oldLeader, Node currentNode){
        try {
            return getServerDocumentService().leaderDown(oldLeader, currentNode);
        } catch (RemoteException e) {
            e.printStackTrace();
            throw new RuntimeException("Error during send leader down", e.getCause());
        }
    }

    public void closeDocument(Node leader){
        try {
            getServerDocumentService().close(leader);
        } catch (RemoteException e) {
            e.printStackTrace();
            throw new RuntimeException("Server communication error", e.getCause());
        }
    }

    public Node electNewLeader(Node oldLeaderNode, Node node){
        try {
            return getServerDocumentService().electNewLeader(oldLeaderNode, node);
        } catch (RemoteException e) {
            e.printStackTrace();
            throw new RuntimeException("Server communication error", e.getCause());
        }
    }

    private ServerDocumentService getServerDocumentService(){
        try {
            Registry serverRegistry = LocateRegistry.getRegistry(this.serverAddress, this.serverRegistryPort);
            ServerDocumentService rmiService = (ServerDocumentService) serverRegistry.lookup(ServerDocumentService.SERVICE_NAME);
            return rmiService;
        } catch (RemoteException e) {
            e.printStackTrace();
            throw new RuntimeException("Server Registry not found", e.getCause());
        } catch (NotBoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Server rmi service not found", e.getCause());
        }
    }
}
