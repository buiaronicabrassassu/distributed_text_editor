package peer.network.rmi;

import peer.Log;
import peer.network.rmi.service.login.IPeerLoginService;
import peer.network.rmi.service.taskExchange.ITaskNotificationService;
import peer.network.rmi.service.disconnection.IPeerNodeDisconnectionService;
import entities.Node;
import rmiinterfaces.*;
import peer.network.rmi.service.heartBeat.IHeartBeatService;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.rmi.NoSuchObjectException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.RMISocketFactory;
import java.rmi.server.UnicastRemoteObject;
import java.util.Arrays;
import java.util.List;

public class RmiRegistryController {
    private final Node currentNode;
    RmiNetwork network;

    private final Registry registry;

    public RmiRegistryController(
            RmiNetwork network
    ){
        this.currentNode = network.currentNode;

        this.network = network;
        try {
            RMISocketFactory.setSocketFactory(new RMISocketFactory()
            {
                public Socket createSocket(String host, int port ) throws IOException, SocketException {
                    int timeoutMillis = 5000;
                    Socket socket = new Socket();
                    socket.setSoTimeout( timeoutMillis );
                    socket.setSoLinger( false, 0 );
                    socket.connect( new InetSocketAddress( host, port ), timeoutMillis );
                    return socket;
                }

                public ServerSocket createServerSocket(int port ) throws IOException
                {
                    return new ServerSocket( port );
                }
            } );
        } catch (IOException ignored) {
            ignored.printStackTrace();
        }

        registry = startRegistry(currentNode.registryPort);
    }

    /**
     * The start registry method must be idempotent.
     * If the RMI registry has already been started this method do nothing
     */
    private Registry startRegistry(int port){
        try {
            return LocateRegistry.createRegistry(port);
        } catch (RemoteException e) {
            e.printStackTrace();
            throw new RuntimeException("error during registry creation");
        }
    }

    public void shutdownRegistry(){
        try {
            UnicastRemoteObject.unexportObject(registry,true);
        } catch (NoSuchObjectException e) {
            e.printStackTrace();
        }
    }

    public void unbindExistingServices(){
        try {
            synchronized (registry){
                List<String> serviceList = Arrays.asList(registry.list());
                serviceList
                        .forEach((serviceName)->{
                            try {
                                registry.unbind(serviceName);
                            } catch (RemoteException | NotBoundException e) {
                                e.printStackTrace();
                            }
                        });
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void bindLeaderServices(
            ITaskNotificationService taskNotificationService,
            IHeartBeatService iHeartBeatService,
            LeaderNodeLoginService nodeLoginService,
            LeaderNodeDisconnectionService disconnectionService,
            LeaderNewVersionService newVersionService,
            LeaderPeerDownService leaderPeerDownService
    ) throws RemoteException{
        unbindExistingServices();
        synchronized (registry){
            bindAndWait(TaskNotificationService.SERVICE_NAME, taskNotificationService);
            bindAndWait(LeaderNodeLoginService.SERVICE_NAME, nodeLoginService);
            bindAndWait(LeaderNewVersionService.SERVICE_NAME, newVersionService);
            bindAndWait(LeaderNodeDisconnectionService.SERVICE_NAME, disconnectionService);
            bindAndWait(LeaderPeerDownService.SERVICE_NAME, leaderPeerDownService);
            bindAndWait(HeartBeatService.SERVICE_NAME, iHeartBeatService);
        }
    }


    public void bindPeerServices(
            ITaskNotificationService taskNotificationService,
            IPeerLoginService loginService,
            IPeerNodeDisconnectionService disconnectionService,
            PeerNewVersionService newVersionService,
            IHeartBeatService iHeartBeatService
    ) throws RemoteException{
        unbindExistingServices();
        bindAndWait(TaskNotificationService.SERVICE_NAME, taskNotificationService);
        bindAndWait(PeerLoginService.SERVICE_NAME, loginService);
        bindAndWait(PeerNodeDisconnectionService.SERVICE_NAME, disconnectionService);
        bindAndWait(PeerNewVersionService.SERVICE_NAME, newVersionService);

        bindAndWait(HeartBeatService.SERVICE_NAME, iHeartBeatService);
    }

    private void bindAndWait(String serviceName, Remote remoteInstance){
        try {
            registry.rebind(serviceName, remoteInstance);
        } catch (RemoteException e) {
            Log.printError("Error during the binding of the service "+serviceName);
            e.printStackTrace();
            assert false;
        }

        while(!isServiceReady(serviceName)){
            try {
                Thread.sleep(500);
            } catch (InterruptedException ignored){}
        }


    }

    private boolean isServiceReady(String serviceName){
        try {
            Arrays.asList(registry.list()).contains(serviceName);
            Log.print(serviceName +" ready");
            return true;
        } catch (RemoteException e) {
            Log.printError("Registry does not respond");
            assert false;
        }
        return false;
    }

    /*public static void printRegistryEntries(Registry registry) {
        try {
            Log.print("\n======== REGISTRY ENTRIES =========");
            List<String> nameList = Arrays.asList(registry.list());
            nameList.forEach((name) -> {
                Log.print(name);
            });
            Log.print("===================================\n");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }*/
}
