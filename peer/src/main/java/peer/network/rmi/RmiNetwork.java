package peer.network.rmi;

import entities.Node;
import peer.Log;
import peer.network.rmi.node.LeaderNode;
import peer.network.rmi.node.DocumentNode;
import peer.network.rmi.node.ServerNode;
import peer.network.rmi.node.PeerNode;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class RmiNetwork {
    private final int NO_LEADER = -1;
    public final Node currentNode;
    private final ServerNode server;
    private final ConcurrentHashMap<Integer, Node> peers = new ConcurrentHashMap<>();
    private final AtomicInteger leaderId = new AtomicInteger(NO_LEADER);

    public RmiNetwork(final Node currentNode, String serverAddress, int serverRegistryPort){
        this.currentNode = currentNode;
        server = new ServerNode(serverAddress, serverRegistryPort);
    }

    public List<PeerNode> getPeerNodes(){
        synchronized (this.leaderId){
            int leaderId = this.leaderId.get();
            return peers.entrySet().stream()
                    .filter(entrySet-> entrySet.getKey() != leaderId)
                    .map(entrySet-> entrySet.getValue())
                    .map(node-> new PeerNode(node))
                    .collect(Collectors.toList());
        }
    }

    public List<DocumentNode> getDocumentNodes(){
        return peers.values().stream()
                .map(node -> new DocumentNode(node))
                .collect(Collectors.toList());
    }

    public synchronized PeerNode addPeerIfAbsent(Node node) {
        PeerNode peerNode = new PeerNode(node);

        if( peerNode.getNodeId() != currentNode.getId()){
            Log.print("Add new peer: "+node.getUserName()+"("+node.getId()+")");
            peers.putIfAbsent(peerNode.getNodeId(), node);
        }
        return peerNode;
    }

    public void removeNode(Node node){
        boolean isLeader;

        synchronized (this){
            if(!peers.containsKey(node.getId())){
                throw new NoSuchElementException("The node does not exists");
            }

            isLeader = this.leaderId.compareAndSet(node.getId(),NO_LEADER);
            peers.remove(node.getId());
        }

        if(isLeader){
            Log.print("Removed Leader: "+node.getUserName()+"("+node.getId()+")");
        }
        else{
            Log.print("Removed Peer: "+node.getUserName()+"("+node.getId()+")");
        }

    }

    public void setLeader(Node leaderNode){
        synchronized (this){
            addPeerIfAbsent(leaderNode);
            leaderId.set(leaderNode.user.id);
        }
        Log.print("Set leader: "+leaderNode.getUserName()+"("+leaderNode.getId()+")");
    }

    /*return the number of nodes logged into the document. The current user is non counted*/
    public int getDocumentNodesNumber(){
        return peers.size();
    }

    public ServerNode getServerNode(){
        return server;
    }

    public synchronized LeaderNode getLeaderPeer(){
        int leaderId = this.leaderId.get();
        if(leaderId == NO_LEADER){
            throw new RuntimeException("There is no leader node");
        }

        if(currentNode.getId() == leaderId){
            return new LeaderNode(currentNode);
        }
        return new LeaderNode(peers.get(leaderId));
    }

    public synchronized boolean isLeader(Node node){
        return leaderId.get() == node.getId();
    }

    public DocumentNode getDocumentNode(Node node) {
        int nodeId = node.getId();
        if(peers.containsKey(nodeId)){
            return new DocumentNode(peers.get(nodeId));
        }
        throw new NoSuchElementException("The node does not exists");
    }

    public void clear(){
        peers.clear();
        leaderId.set(NO_LEADER);
    }

    public boolean containsNode(String nodeAddress){
        long count = peers.values().stream()
                .map((node)->node.ipAddress)
                .filter((address)->address.equals(nodeAddress)).count();
        return count >= 1;
    }
}
