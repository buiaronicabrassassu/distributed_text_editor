package peer.network.rmi.service.taskExchange;

import entities.Node;
import entities.Task;
import peer.Log;

import peer.network.rmi.RmiNetwork;
import peer.network.rmi.RmiUtils;
import peer.network.rmi.service.errorHandler.RmiErrorHandler;
import peer.useCase.taskExchange.TaskNotificationService;
import peer.network.rmi.node.DocumentNode;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RmiTaskNotificationService implements TaskNotificationService {
    private final RmiNetwork rmiNetwork;
    private final RmiErrorHandler errorHandler;
    private final ExecutorService executorService = Executors.newFixedThreadPool(10);

    public RmiTaskNotificationService(RmiNetwork rmiNetwork, RmiErrorHandler errorHandler){
        this.rmiNetwork = rmiNetwork;
        this.errorHandler = errorHandler;
    }

    @Override
    public void notifyTask(Task task){
        List<DocumentNode> nodes = rmiNetwork.getDocumentNodes();

        nodes.forEach((peerNode)->{
            CompletableFuture
                    .runAsync(()->{
                        RmiUtils.tryNTimes(()->{
                            peerNode.sendTask(task);
                            //Log.print("Task notified to "+ peerNode.node.getUserName());
                            return null;
                        },1,1000);
                    },executorService)
                    .exceptionally((exception)->{
                        Log.print("Task notification error for peer "+peerNode.node.getUserName());
                        errorHandler.handleNodeDown(peerNode);
                        return null;
                    });
        });
    }

    @Override
    public void notifyTasksToNode(List<Task> tasks, Node node) {
        DocumentNode peerNode = new DocumentNode(node);

        tasks.forEach((task)->{
            CompletableFuture
                    .runAsync(()->{
                        RmiUtils.tryNTimes(()->{
                            peerNode.sendTask(task);
                            //Log.print("Task notified to "+ peerNode.node.user.name);
                            return null;
                        });
                    },executorService)
                    .exceptionally((exception)->{
                        errorHandler.handleNodeDown(peerNode);
                        return null;
                    });
        });
    }
}
