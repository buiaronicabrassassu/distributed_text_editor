package peer.network.rmi.service.disconnection;

import entities.Node;
import entities.Task;
import peer.Log;
import peer.network.rmi.RmiNetwork;
import peer.network.rmi.node.DocumentNode;
import peer.useCase.disconnection.PeerDisconnectionUseCase;
import rmiinterfaces.PeerNodeDisconnectionService;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class IPeerNodeDisconnectionService extends UnicastRemoteObject implements PeerNodeDisconnectionService {
    RmiNetwork network;
    RmiPeerDisconnectionService rmiPeerDisconnectionService;
    PeerDisconnectionUseCase disconnectionUseCase;

    public IPeerNodeDisconnectionService(RmiNetwork network, RmiPeerDisconnectionService rmiPeerDisconnectionService, PeerDisconnectionUseCase disconnectionUseCase) throws RemoteException {
        super();
        this.network = network;
        this.rmiPeerDisconnectionService = rmiPeerDisconnectionService;
        this.disconnectionUseCase = disconnectionUseCase;
    }

    @Override
    public void disconnectNode(Node node, List<Task> nodeTasks) throws RemoteException{
        CompletableFuture.runAsync(()->{
            Log.print("disconnect node received: "+node.getUserName());
            boolean isLeaderNode = network.isLeader(node);
            network.removeNode(node);
            Log.print("Ipeer: node removed");

            if(isLeaderNode){
                disconnectionUseCase.handleLeaderDisconnection(node, nodeTasks);
            }
            else{
                disconnectionUseCase.handlePeerDisconnection(node, nodeTasks);
            }
            Log.print(node.getUserName()+ " disconnection request completed");
        });
    }

    @Override
    public void disconnectionAck() throws RemoteException {
        rmiPeerDisconnectionService.handleDisconnectionAck();
    }
}
