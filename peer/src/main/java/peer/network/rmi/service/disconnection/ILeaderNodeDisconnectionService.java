package peer.network.rmi.service.disconnection;

import entities.Node;
import entities.Task;
import peer.Log;
import peer.useCase.disconnection.LeaderDisconnectionUseCase;
import rmiinterfaces.LeaderNodeDisconnectionService;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;

public class ILeaderNodeDisconnectionService extends UnicastRemoteObject implements LeaderNodeDisconnectionService {
    LeaderDisconnectionUseCase disconnectionUseCase;

    public ILeaderNodeDisconnectionService(LeaderDisconnectionUseCase disconnectionUseCase) throws RemoteException {
        super();
        this.disconnectionUseCase = disconnectionUseCase;
    }

    @Override
    public void disconnectionRequest(Node user, List<Task> tasks) throws RemoteException {
        Log.print("Disconnection request received");
        CompletableFuture
                .runAsync(()->{
                    disconnectionUseCase.handlePeerDisconnectionRequest(user, tasks);
                }, Executors.newSingleThreadExecutor());

    }
}
