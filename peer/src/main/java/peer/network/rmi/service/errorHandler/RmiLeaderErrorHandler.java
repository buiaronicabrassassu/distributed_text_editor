package peer.network.rmi.service.errorHandler;

import peer.Log;
import peer.network.rmi.RmiNetwork;
import peer.network.rmi.node.DocumentNode;
import peer.useCase.nodeDown.PeerDownUseCase;

public class RmiLeaderErrorHandler implements RmiErrorHandler {

    RmiNetwork network;
    PeerDownUseCase peerDownUseCase;

    public RmiLeaderErrorHandler(RmiNetwork network, PeerDownUseCase peerDownUseCase) {
        this.network = network;
        this.peerDownUseCase = peerDownUseCase;
    }

    @Override
    public void handleNodeDown(DocumentNode documentNode) {
        Log.print("Error in sending new message to peer node " + documentNode.node.getUserName());
        peerDownUseCase.handlePeerDown(documentNode.node);
    }
}