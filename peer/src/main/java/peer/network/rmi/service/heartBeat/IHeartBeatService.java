package peer.network.rmi.service.heartBeat;

import peer.network.rmi.RmiNetwork;
import rmiinterfaces.HeartBeatService;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class IHeartBeatService extends UnicastRemoteObject implements HeartBeatService {

    final RmiNetwork rmiNetwork;

    public IHeartBeatService(RmiNetwork rmiNetwork) throws RemoteException {
        super();
        this.rmiNetwork = rmiNetwork;
    }

    @Override
    public boolean heartBeat() throws RemoteException {
        return true;
    }
}
