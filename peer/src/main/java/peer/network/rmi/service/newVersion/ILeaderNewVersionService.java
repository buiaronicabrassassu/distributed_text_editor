package peer.network.rmi.service.newVersion;

import entities.Task;
import peer.network.rmi.RmiNetwork;
import rmiinterfaces.LeaderNewVersionService;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class ILeaderNewVersionService extends UnicastRemoteObject implements LeaderNewVersionService {
    RmiNetwork network;
    RmiLeaderNetworkNewVersionService leaderNetworkNewVersionService;

    public ILeaderNewVersionService(RmiNetwork network, RmiLeaderNetworkNewVersionService leaderNetworkNewVersionService) throws RemoteException {
        super();
        this.network = network;
        this.leaderNetworkNewVersionService = leaderNetworkNewVersionService;
    }

    @Override
    public void newVersion(int versionID, int userId, List<Task> peerTasks) throws RemoteException {
        leaderNetworkNewVersionService.handleNewVersionMessage(versionID, userId, peerTasks);
    }

    @Override
    public void newVersionFail(int versionID) throws RemoteException {
        leaderNetworkNewVersionService.handleNewVersionFail(versionID);
    }
}
