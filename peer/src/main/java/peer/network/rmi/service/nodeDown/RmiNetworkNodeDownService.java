package peer.network.rmi.service.nodeDown;

import entities.Node;
import peer.Log;
import peer.network.rmi.RmiNetwork;
import peer.network.rmi.node.DocumentNode;
import peer.useCase.nodeDown.NetworkNodeDownService;

public class RmiNetworkNodeDownService implements NetworkNodeDownService {
    private final RmiNetwork rmiNetwork;

    public RmiNetworkNodeDownService(RmiNetwork rmiNetwork){
        this.rmiNetwork = rmiNetwork;
    }

    @Override
    public boolean isNodeDown(Node node) {
        try{
            DocumentNode peerNode = rmiNetwork.getDocumentNode(node);
            peerNode.sendHeartBeat();
            Log.print("Heartbeat message sent to "+ peerNode.node.user.name);
            return false;
        }
        catch(RuntimeException exception){
            return true;
        }

    }
}
