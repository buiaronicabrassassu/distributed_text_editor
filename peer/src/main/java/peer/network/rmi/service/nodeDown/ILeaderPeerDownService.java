package peer.network.rmi.service.nodeDown;

import entities.Node;
import peer.Log;
import peer.useCase.nodeDown.PeerDownUseCase;
import rmiinterfaces.LeaderPeerDownService;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.CompletableFuture;

public class ILeaderPeerDownService extends UnicastRemoteObject implements LeaderPeerDownService {
    PeerDownUseCase peerDownUseCase;

    public ILeaderPeerDownService(PeerDownUseCase peerDownUseCase) throws RemoteException {
        super();
        this.peerDownUseCase = peerDownUseCase;
    }

    @Override
    public void peerDown(Node peer) throws RemoteException {
        Log.print("Peer down notification received");
        CompletableFuture
                .runAsync(()-> {
                    peerDownUseCase.handlePeerDown(peer);
                });
    }

}
