package peer.network.rmi.service.errorHandler;

import peer.Log;
import peer.network.rmi.RmiNetwork;
import peer.network.rmi.node.DocumentNode;
import peer.useCase.ProtocolScheduler.ProtocolManager;
import peer.useCase.nodeDown.LeaderDownUseCase;

public class RmiPeerErrorHandler implements RmiErrorHandler {

    private final RmiNetwork rmiNetwork;
    private final LeaderDownUseCase leaderDownUseCase;
    private final ProtocolManager protocolManager;

    public RmiPeerErrorHandler(RmiNetwork rmiNetwork, LeaderDownUseCase leaderDownUseCase, ProtocolManager protocolManager){
        this.rmiNetwork = rmiNetwork;
        this.leaderDownUseCase = leaderDownUseCase;
        this.protocolManager = protocolManager;
    }

    @Override
    public void handleNodeDown(DocumentNode documentNode) {
        if(rmiNetwork.isLeader(documentNode.node)){
            Log.print("Error in sending message to leader node " + documentNode.node.getUserName());
            protocolManager.fail();
            leaderDownUseCase.handleLeaderDown(documentNode.node);
        }
        else{
            Log.print("Error in sending new message to peer node " + documentNode.node.getUserName());
            rmiNetwork.getLeaderPeer().sendPeerDown(documentNode.node);
        }
    }
}
