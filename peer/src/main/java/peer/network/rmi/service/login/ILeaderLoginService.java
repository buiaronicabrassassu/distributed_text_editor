package peer.network.rmi.service.login;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;

import entities.Node;
import peer.Log;
import peer.network.rmi.RmiNetwork;
import peer.useCase.login.Leader_PeerLoginUseCase;
import rmiinterfaces.LeaderNodeLoginService;

public class ILeaderLoginService extends UnicastRemoteObject implements LeaderNodeLoginService {

    RmiNetwork network;
    Leader_PeerLoginUseCase loginUseCase;

    public ILeaderLoginService(RmiNetwork network, Leader_PeerLoginUseCase loginUseCase) throws RemoteException {
        super();
        this.network = network;
        this.loginUseCase = loginUseCase;
    }

    @Override
    public void login(Node user) throws RemoteException {
        Log.print("Login required by "+user.getUserName());

        CompletableFuture
                .runAsync(()->{
                    loginUseCase.handleLoginRequest(user);
                }, Executors.newSingleThreadExecutor());
    }
}