package peer.network.rmi.service.initialization;

import entities.Node;
import peer.network.rmi.RmiNetwork;
import peer.useCase.initialization.NetworkLeaderElectionService;

public class RmiInitializationService implements NetworkLeaderElectionService {
    private final RmiNetwork network;

    public RmiInitializationService(RmiNetwork network){
        this.network = network;
    }

    @Override
    public Node getLeaderNode() {
        Node leader = network.getServerNode().getDocumentLeader(network.currentNode);
        network.setLeader(leader);
        return leader;
    }

    @Override
    public Node newLeaderElection(Node oldLeaderNode) {
        Node leader = network.getServerNode().electNewLeader(oldLeaderNode ,network.currentNode);
        network.setLeader(leader);
        return leader;
    }

    @Override
    public Node notifyLeaderDownToServer(Node oldLeaderNode) {
        synchronized (network){
            Node newLeader = network.getServerNode().sendLeaderDown(oldLeaderNode, network.currentNode);
            network.setLeader(newLeader);
            return newLeader;
        }

    }


}
