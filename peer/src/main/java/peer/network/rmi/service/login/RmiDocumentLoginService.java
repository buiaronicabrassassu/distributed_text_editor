package peer.network.rmi.service.login;

import entities.Node;
import entities.Version;
import peer.Log;
import peer.network.rmi.RmiNetwork;
import peer.network.rmi.node.PeerNode;
import peer.useCase.login.NodeLoginService;

import java.util.List;
import java.util.stream.Collectors;

public class RmiDocumentLoginService implements NodeLoginService {
    RmiNetwork network;

    public RmiDocumentLoginService(RmiNetwork network){
        this.network = network;
    }

    public void sendLoginAck(Node node, Version version){

        List<Node> otherPeerNodes = network.getPeerNodes().stream()
                .map((documentNode)->documentNode.node)
                .collect(Collectors.toList());
        PeerNode peerNode = network.addPeerIfAbsent(node);

        peerNode.sendLoginAck(version, otherPeerNodes);
    }

    @Override
    public void notifyNewNode(Node newNode) {
        List<PeerNode> peersToNotify = network.getPeerNodes().stream()
                .filter((node)-> {
                    boolean isNotNewNode = node.getNodeId() != newNode.user.id;
                    return isNotNewNode;
                })
                .collect(Collectors.toList());

        peersToNotify.forEach((peer)->{
            Log.print("Notify "+newNode.getUserName()+" to "+peer.node.getUserName());
            peer.notifyNewNode(newNode);
        });
    }
}
