package peer.network.rmi.service.newVersion;

import entities.Task;
import entities.Version;
import peer.Log;

import peer.network.rmi.RmiNetwork;
import peer.network.rmi.node.PeerNode;
import peer.network.rmi.node.ServerNode;
import peer.network.rmi.service.errorHandler.RmiLeaderErrorHandler;
import peer.useCase.newVersion.LeaderNetworkNewVersionService;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.IntFunction;

public class RmiLeaderNetworkNewVersionService implements LeaderNetworkNewVersionService {
    private final int PEER_MESSAGES_WAITING_TIME = 30000;

    private RmiNetwork rmiNetwork;
    private RmiLeaderErrorHandler errorHandler;

    private final Map<Integer, List<Task>> peerUpdates = new ConcurrentHashMap<>();
    private List<PeerNode> connectedPeerNodes;
    private CompletableFuture<List<List<Task>>> pendingNewVersion;
    private int myNodeId;
    private volatile int newVersionId;
    private volatile int numberOfNodesData;
    private final ScheduledExecutorService executorService;
    private final AtomicReference<ScheduledFuture<?>> scheduledTimer = new AtomicReference<>(null);

    public RmiLeaderNetworkNewVersionService(RmiNetwork rmiNetwork, RmiLeaderErrorHandler errorHandler){
        this.rmiNetwork = rmiNetwork;
        this.errorHandler = errorHandler;
        this.myNodeId = rmiNetwork.currentNode.getId();
        this.executorService = Executors.newScheduledThreadPool(1);
    }

    @Override
    public void sendNewVersionMessage(int versionId, List<Task> updates){
        this.peerUpdates.clear();
        this.peerUpdates.put(rmiNetwork.currentNode.getId(), updates);
        this.numberOfNodesData = rmiNetwork.getDocumentNodesNumber() + 1;
        connectedPeerNodes = rmiNetwork.getPeerNodes();
        this.newVersionId = versionId;
        this.pendingNewVersion = new CompletableFuture<>();

        /*if the leader is the only node it don't have to wait for other messages*/
        if(numberOfNodesData == 1){
            Log.print("The leader is the only node in the network");
            pendingNewVersion.complete(new ArrayList<>(peerUpdates.values()));
            return;
        }

        CompletableFuture<Void>[] sendPromises = connectedPeerNodes
                .stream()
                .map((peerNode) -> CompletableFuture.runAsync(() -> {
                    Log.print("Version message sent to a normal peer " + peerNode.node.getUserName());
                    peerNode.sendNewVersionMessage(versionId, myNodeId, updates);
                })).toArray((IntFunction<CompletableFuture<Void>[]>) CompletableFuture[]::new);

        try {
            CompletableFuture.allOf(sendPromises).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.print("new version protocol failed");
            throw new RuntimeException("New version protocol failed");
        }

        scheduleTimerForPeerMessages();
    }

    @Override
    public CompletableFuture<List<List<Task>>> waitForPeerTasks(){
        return pendingNewVersion;
    }

    void handleNewVersionMessage(int versionID, int userId, List<Task> updates){
        if(versionID != newVersionId){
            Log.print("Ignoring new version message of version: "+ versionID+". The current version is "+newVersionId);
            //throw new RuntimeException("New version error: version id received different from new version id");
        }

        Log.print("Received new version message from user with id="+userId);

        int numberOfReceivedData;
        synchronized (peerUpdates) {
            peerUpdates.put(userId, updates);
            numberOfReceivedData = peerUpdates.size();
        }

        if(numberOfReceivedData == numberOfNodesData){
            Log.print("All new version messages received from peers");
            stopTimerForPeerMessages();
            pendingNewVersion.complete(new ArrayList<>(peerUpdates.values()));
        }

        /*if(peerUpdates.values().size() > rmiNetwork.getDocumentNodesNumber()){
            throw new RuntimeException("New version error: number of received 'newVersion message' is greater than the total number of peer");
        }*/
    }

    public void handleNewVersionFail(int versionID) {
        if(versionID == newVersionId){
            stopTimerForPeerMessages();
            pendingNewVersion.completeExceptionally(new RuntimeException("New version protocol failed"));
        }

    }

    @Override
    public void sendNewVersionFailed(){
        connectedPeerNodes.forEach((peerNode)->{
            CompletableFuture
                    .runAsync(()->{
                        peerNode.sendNewVersionFail(newVersionId);
                        Log.print("Version fail sent to a normal peer "+ peerNode.node.user.name);
                    })
                    .exceptionally((none)->{
                        errorHandler.handleNodeDown(peerNode);
                        return null;
                    });
        });
    }

    @Override
    public void sendNewVersionAck() {
        Log.print("===Start sending new version ack===");
        CompletableFuture<Void>[] sendPromises = connectedPeerNodes
                .stream()
                .map((peerNode) -> CompletableFuture
                        .runAsync(()->{
                            peerNode.sendNewVersionAck(newVersionId);
                            Log.print("Version ack sent to a normal peer "+ peerNode.node.user.name);
                        })
                        .exceptionally((none)->{
                            errorHandler.handleNodeDown(peerNode);
                            return null;
                        }).thenRun(()->{})
                ).toArray((IntFunction<CompletableFuture<Void>[]>) CompletableFuture[]::new);

        try {
            CompletableFuture.allOf(sendPromises).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void notifyNewVersionToServer(Version newVersion) {
        ServerNode server = rmiNetwork.getServerNode();
        server.sendNewVersion(newVersion);
        Log.print("New version sent to the server");
    }



    private void scheduleTimerForPeerMessages(){
        Log.print("Start peer HeartBeat Timer");
        scheduledTimer.set(executorService.schedule(
                this::checkMissingPeers,
                PEER_MESSAGES_WAITING_TIME,
                TimeUnit.MILLISECONDS));
    }

    private void stopTimerForPeerMessages(){
        Log.print("Stop Peer HeartBeat Timer");
        scheduledTimer.updateAndGet((future)->{
            future.cancel(true);
            return null;
        });
    }

    private void checkMissingPeers() {
        Log.print("Check missing peers");

        CompletableFuture<Void>[] promise = connectedPeerNodes.stream()
                .filter((peerNode) -> !peerUpdates.containsKey(peerNode.node.getId()))
                .map((peerNode) -> CompletableFuture
                        .runAsync(() -> {
                            Log.print("Send heartbeat to " + peerNode.node.getUserName());
                            peerNode.sendHeartBeat();
                        })
                        .exceptionally((e) -> {
                            Log.print("Send heartbeat to " + peerNode.node.getUserName());

                            errorHandler.handleNodeDown(peerNode);
                            throw new RuntimeException("failed");
                        })).toArray((IntFunction<CompletableFuture<Void>[]>) CompletableFuture[]::new);

        try {
            CompletableFuture.allOf(promise).get();
            Log.print("All peers up");
            scheduleTimerForPeerMessages();
        } catch (InterruptedException ignore) {
            Log.print("New Version Timer interrupted");
            scheduleTimerForPeerMessages();
        }catch (RuntimeException | ExecutionException e) {
            Log.print("New version error in sending heartbeat");
            pendingNewVersion.completeExceptionally(new RuntimeException("New Version Protocol failed. There are some peers Down"));
        }
    }

}
