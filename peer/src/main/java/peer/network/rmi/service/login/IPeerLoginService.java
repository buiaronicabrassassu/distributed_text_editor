package peer.network.rmi.service.login;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import entities.Node;
import entities.Version;
import peer.network.rmi.RmiNetwork;

import peer.network.rmi.node.DocumentNode;

import peer.useCase.login.NewNodeUseCase;
import rmiinterfaces.PeerLoginService;

public class IPeerLoginService extends UnicastRemoteObject implements PeerLoginService {

    RmiNetwork network;
    RmiPeerLoginService loginService;
    NewNodeUseCase newNodeUseCase;

    public IPeerLoginService(RmiNetwork network, RmiPeerLoginService loginService, NewNodeUseCase newNodeUseCase) throws RemoteException {
        super();
        this.network = network;
        this.loginService = loginService;
        this.newNodeUseCase = newNodeUseCase;
    }

    @Override
    public void loginAck(Version version, List<Node> otherPeers) throws RemoteException {
        otherPeers.forEach((peer)->network.addPeerIfAbsent(peer));
        CompletableFuture.runAsync(()->loginService.handleLoginAck(version));

    }

    @Override
    public void notifyNewNode(Node newNode) throws RemoteException{
        DocumentNode document = new DocumentNode(newNode);
        document.sendHeartBeat();
        CompletableFuture
                .runAsync(()->network.addPeerIfAbsent(newNode))
                .thenRun(()-> newNodeUseCase.handleNewNode(newNode));
    }

}