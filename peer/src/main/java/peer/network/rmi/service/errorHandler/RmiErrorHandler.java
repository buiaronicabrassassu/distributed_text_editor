package peer.network.rmi.service.errorHandler;

import peer.network.rmi.node.DocumentNode;

public interface RmiErrorHandler {

    void handleNodeDown(DocumentNode documentNode);
}
