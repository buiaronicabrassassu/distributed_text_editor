package peer.network.rmi.service.nodeDown;

import entities.Node;
import peer.Log;
import peer.useCase.nodeDown.LeaderDownUseCase;
import rmiinterfaces.PeerLeaderDownService;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Used when a peer wants to warn another peer about the leader's fall
 * */
public class IPeerLeaderDownService extends UnicastRemoteObject implements PeerLeaderDownService {

    LeaderDownUseCase leaderDownUseCase;

    protected IPeerLeaderDownService(LeaderDownUseCase leaderDownUseCase) throws RemoteException {
        super();
        this.leaderDownUseCase = leaderDownUseCase;
    }

    @Override
    public void leaderDown(Node leader) throws RemoteException {
        Log.print("Leader down notification received");
        leaderDownUseCase.handleNotificationOfLeaderDown(leader);
    }
}
