package peer.network.rmi.service.newVersion;

import entities.Task;
import peer.useCase.newVersion.PeerNewVersionUseCase;
import rmiinterfaces.PeerNewVersionService;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;

public class IPeerNewVersionService extends UnicastRemoteObject implements PeerNewVersionService {
    private PeerNewVersionUseCase peerNewVersionUseCase;
    private RmiPeerNetworkNewVersionService rmiNewVersionService;
    private AtomicInteger currentVersionId = new AtomicInteger(-1);

    public IPeerNewVersionService(PeerNewVersionUseCase peerNewVersionUseCase, RmiPeerNetworkNewVersionService rmiNewVersionService) throws RemoteException {
        super();
        this.peerNewVersionUseCase = peerNewVersionUseCase;
        this.rmiNewVersionService = rmiNewVersionService;
    }

    @Override
    public void newVersion(int versionID, int userId, List<Task> peerTasks) throws RemoteException {
        synchronized (currentVersionId) {
            int currentVersionID = this.currentVersionId.getAndUpdate((currVersionID) -> Math.max(versionID, currVersionID));

            if (currentVersionID < versionID) {
                rmiNewVersionService.resetProtocol();
                CompletableFuture.runAsync(() -> {
                    peerNewVersionUseCase.startNewVersionProtocol(versionID);
                });
            }
        }

        rmiNewVersionService.handleNewVersionMessage(userId, peerTasks);
    }

    @Override
    public void newVersionAck(int versionID) throws RemoteException{
        if(versionID!=currentVersionId.get()){
            throw new RuntimeException("New version error: version id received in the new version ack message is wrong");
        }

        rmiNewVersionService.handleNewVersionAck(versionID);
    }

    @Override
    public void newVersionFail(int versionID) throws RemoteException {
        if(versionID!=currentVersionId.get()){
            throw new RuntimeException("New version fail error: version id received in the new version fail message is wrong");
        }

        rmiNewVersionService.handleNewVersionFail(versionID);
    }
}
