package peer.network.rmi.service.newVersion;

import entities.Task;
import peer.Log;
import peer.network.rmi.RmiNetwork;
import peer.network.rmi.node.LeaderNode;
import peer.network.rmi.node.PeerNode;
import peer.network.rmi.service.errorHandler.RmiPeerErrorHandler;
import peer.useCase.newVersion.PeerNetworkNewVersionService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.IntFunction;

public class RmiPeerNetworkNewVersionService implements PeerNetworkNewVersionService {
    private RmiNetwork rmiNetwork;
    private RmiPeerErrorHandler errorHandler;
    private CompletableFuture<List<List<Task>>> pendingNewVersion;
    private Map<Integer, List<Task>> peerUpdates = new ConcurrentHashMap<>();
    private volatile int numberOfNodesData;

    public RmiPeerNetworkNewVersionService(RmiNetwork rmiNetwork, RmiPeerErrorHandler errorHandler){
        this.rmiNetwork = rmiNetwork;
        this.errorHandler = errorHandler;
        numberOfNodesData = rmiNetwork.getDocumentNodesNumber() + 1;
    }

    public void resetProtocol(){
        peerUpdates.clear();
        numberOfNodesData = rmiNetwork.getDocumentNodesNumber() + 1;
    }

    public void handleNewVersionMessage(int userId, List<Task> peerTasks){
        peerUpdates.put(userId, peerTasks);
        checkProtocolError();
    }

    /**
     * Send the new version message first to all the normal peers and after to the leader
     */
    @Override
    public void sendNewVersionMessage(int versionId, List<Task> updates) {
        int myNodeId = rmiNetwork.currentNode.getId();
        peerUpdates.put(myNodeId, updates);
        checkProtocolError();

        pendingNewVersion = new CompletableFuture<>();

        CompletableFuture<Void>[] pendingSendOfNewVersionToPeers = sendNewVersionMessageToPeers(versionId, updates, myNodeId);

        try {
            CompletableFuture.allOf(pendingSendOfNewVersionToPeers).get();
            sendNewVersionMessageToLeader(versionId, updates, myNodeId);
        } catch (Exception e) {
            rmiNetwork.getLeaderPeer().sendNewVersionFail(versionId);
        }
    }

    private CompletableFuture<Void>[] sendNewVersionMessageToPeers(int versionId, List<Task> updates, int myNodeId) {
        List<PeerNode> nodes = rmiNetwork.getPeerNodes();

        return nodes
                .stream()
                .map((peerNode) -> CompletableFuture
                        .runAsync(() -> {
                            peerNode.sendNewVersionMessage(versionId, myNodeId, updates);
                            Log.print("Version message sent to a normal peer " + peerNode.node.user.name);
                        })
                        .exceptionally((e) -> {
                            errorHandler.handleNodeDown(peerNode);
                            return null;
                        })).toArray((IntFunction<CompletableFuture<Void>[]>) CompletableFuture[]::new);
    }

    private void sendNewVersionMessageToLeader(int versionId, List<Task> updates, int myNodeId){
        LeaderNode leader = rmiNetwork.getLeaderPeer();
        Log.print("Try to send new version message to leader");

        try{
            leader.sendNewVersionMessage(versionId, myNodeId, updates);
            Log.print("Version message sent to a leader node "+ leader.node.user.name);
        }catch (RuntimeException e){
            errorHandler.handleNodeDown(leader);
            throw new RuntimeException("Leader does not respond");
        }
    }

    @Override
    public CompletableFuture<List<List<Task>>> waitForLeaderAck(){
        return pendingNewVersion;
    }

    public void handleNewVersionAck(int versionId){
        Log.print("New version ack received from the leader");
        int numberOfReceivedNewVersionMsg = peerUpdates.size();

        if(numberOfReceivedNewVersionMsg != numberOfNodesData){
            throw new RuntimeException("New version error: leader ack received but not all the version messages have been received");
        }

        List<List<Task>> updatesList = new ArrayList<>(peerUpdates.values());

        pendingNewVersion.complete(updatesList);
    }

    void handleNewVersionFail(int versionID) {
        Log.print("new version failed received");
        pendingNewVersion.completeExceptionally(new RuntimeException("New version protocol failed"));
    }

    private void checkProtocolError(){
        int numberOfReceivedNewVersionMsg = peerUpdates.size();

        if(numberOfReceivedNewVersionMsg > numberOfNodesData){
            throw new RuntimeException("New version error: number of received version messages is greater than number of nodes in the network");
        }
    }
}
