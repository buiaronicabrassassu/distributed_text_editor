package peer.network.rmi.service.disconnection;

import entities.Node;
import entities.Task;
import entities.Version;
import peer.Log;
import peer.network.rmi.RmiNetwork;
import peer.network.rmi.node.PeerNode;
import peer.network.rmi.node.ServerNode;
import peer.network.rmi.service.errorHandler.RmiLeaderErrorHandler;
import peer.useCase.disconnection.NetworkDisconnectionService;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.IntFunction;

public class RmiLeaderDisconnectionService extends NetworkDisconnectionService {
    private final RmiNetwork network;
    private final RmiLeaderErrorHandler leaderErrorHandler;

    public RmiLeaderDisconnectionService(RmiNetwork network, RmiLeaderErrorHandler leaderErrorHandler){
        this.network = network;
        this.leaderErrorHandler = leaderErrorHandler;
    }

    @Override
    public void notifyLeaderDisconnection(List<Task> nodeTasks) {
        ServerNode serverNode = network.getServerNode();
        if(network.getDocumentNodesNumber()>0){
            serverNode.disconnectLeader(network.currentNode);
        }
        else{
            serverNode.closeDocument(network.currentNode);
        }

        network.getPeerNodes()
                .forEach(peer->{
                    Log.print("Send disconnect to "+ peer.node.getUserName());
                    peer.disconnectNode(network.currentNode, nodeTasks);
                });
    }

    @Override
    public void notifyNodeDisconnection(Node node, List<Task> nodeTasks){

        try{
            network.removeNode(node);

            List<PeerNode> peers = network.getPeerNodes();
            Log.print("Notify disconnection to "+peers.size());

            CompletableFuture<Void>[] promises = peers.stream()
                    .map((peer) -> CompletableFuture
                                .runAsync(() -> {
                                    Log.print("Send disconnect to "+ peer.node.getUserName());
                                    peer.disconnectNode(node, nodeTasks);
                                })
                                .exceptionally((e)->{
                                    leaderErrorHandler.handleNodeDown(peer);
                                    return null;
                                })
                    ).toArray((IntFunction<CompletableFuture<Void>[]>) CompletableFuture[]::new);
            CompletableFuture.allOf(promises).get();
        }
        catch (NoSuchElementException | ExecutionException | InterruptedException ignored){
            Log.print("disconnection exception");
        }

    }

    @Override
    public void confirmDisconnection(Node node){

        PeerNode disconnectingNode = new PeerNode(node);
        disconnectingNode.sendDisconnectionConfirm();
    }

    @Override
    public void updateServerVersion(Version version) {
        network.getServerNode().updateVersion(version);
    }
}
