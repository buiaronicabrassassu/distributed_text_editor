package peer.network.rmi.service.disconnection;

import entities.Task;
import peer.Log;
import peer.network.rmi.RmiNetwork;
import peer.network.rmi.RmiRegistryController;
import peer.useCase.disconnection.NodeDisconnectionService;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class RmiPeerDisconnectionService implements NodeDisconnectionService {
    private RmiNetwork network;
    private RmiRegistryController registryController;
    private CompletableFuture<Void> disconnectionPromise;

    public RmiPeerDisconnectionService(RmiNetwork network, RmiRegistryController registryController){
        this.network = network;
        this.registryController = registryController;
    }

    @Override
    public CompletableFuture<Void> disconnectCurrentNode(List<Task> nodeTasks){
        Log.print("disconnect CurrentNode");
        disconnectionPromise = new CompletableFuture<>();
        Log.print("send request to leader");
        CompletableFuture
                .runAsync(() -> {
                    network.getLeaderPeer().sendDisconnectionRequest(network.currentNode , nodeTasks);
                })
                .exceptionally((e)->{
                    disconnectionPromise.completeExceptionally(e);
                    return null;
                });

        Log.print("disconnection request sent");

        return disconnectionPromise;
    }

    public void handleDisconnectionAck(){
        disconnectionPromise.complete(null);
        disconnectionPromise = null;
        registryController.shutdownRegistry();
    }
}