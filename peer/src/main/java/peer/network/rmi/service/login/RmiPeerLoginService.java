package peer.network.rmi.service.login;

import entities.Version;

import peer.Log;
import peer.network.rmi.RmiNetwork;
import peer.useCase.login.NetworkLoginService;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicReference;

public class RmiPeerLoginService implements NetworkLoginService {

    private final RmiNetwork network;
    private final AtomicReference<CompletableFuture<Version>> pendingRequest = new AtomicReference<>(null);

    public RmiPeerLoginService(RmiNetwork network){
        this.network = network;
    }

    @Override
    public CompletableFuture<Version> login(){
        System.out.println("out of sync block"+this);
        synchronized (pendingRequest){
            System.out.println("into"+this);
            CompletableFuture<Version> promise = pendingRequest.updateAndGet((pendingRequest) -> new CompletableFuture<>());
            CompletableFuture.runAsync(()->{
                network.getLeaderPeer().sendLoginRequest(network.currentNode);
            },Executors.newSingleThreadExecutor())
                    .exceptionally((e)->{
                        e.printStackTrace();
                        promise.completeExceptionally(new RuntimeException("login request failed"));
                        return null;
                    });

            return promise;
        }
    }


    public void handleLoginAck(Version version){
        pendingRequest.updateAndGet((promise)->{
            promise.complete(version);
            return null;
        });

    }
}
