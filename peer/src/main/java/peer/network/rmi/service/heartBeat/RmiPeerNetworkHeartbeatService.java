package peer.network.rmi.service.heartBeat;

import peer.Log;
import peer.network.rmi.RmiNetwork;
import peer.network.rmi.node.LeaderNode;
import peer.network.rmi.service.errorHandler.RmiErrorHandler;
import peer.useCase.initialization.NetworkLeaderAliveService;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;


public class RmiPeerNetworkHeartbeatService implements NetworkLeaderAliveService {
    private final int PEER_LEADER_WAITING_TIME = 5000;
    private final RmiNetwork rmiNetwork;
    private final RmiErrorHandler errorHandler;
    private final ScheduledExecutorService executorService;
    private final AtomicReference<ScheduledFuture<?>> scheduledHeartBeet = new AtomicReference<>(null);

    public RmiPeerNetworkHeartbeatService(RmiNetwork rmiNetwork, RmiErrorHandler errorHandler){
        this.rmiNetwork = rmiNetwork;
        this.errorHandler = errorHandler;
        this.executorService = Executors.newScheduledThreadPool(1);
    }

    @Override
    public void startHeartBeatScheduling() {
        //Log.print("Start HeartBeet Scheduling");
        scheduleLeaderCheck();
    }

    @Override
    public void stopHeartBeatScheduling(){
        //Log.print("Stop HeartBeat Scheduling");
        scheduledHeartBeet.updateAndGet((future)->{
            if(future != null){
                future.cancel(true);
            }

            return null;
        });
    }

    private void checkLeaderActivity(){
        LeaderNode leader = rmiNetwork.getLeaderPeer();

        try {
            //Log.print("HEART_BEAT: Send HeatBeat to leader");
            leader.sendHeartBeat();
            //Log.print("HEART_BEAT: The leader is up");
            scheduleLeaderCheck();
        }
        catch(RuntimeException exception) {
            Log.print("HEART_BEAT: The leader is down. Force protocol fail");
            errorHandler.handleNodeDown(leader);
        }
    }

    private void scheduleLeaderCheck(){
        scheduledHeartBeet.set(
                executorService.schedule(
                        this::checkLeaderActivity,
                        PEER_LEADER_WAITING_TIME,
                        TimeUnit.MILLISECONDS)
        );
    }

    @Override
    protected void finalize() throws Throwable {
        executorService.shutdownNow();
    }
}
