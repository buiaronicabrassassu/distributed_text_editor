package peer.network.rmi.service.taskExchange;

import entities.Task;
import peer.Log;
import peer.network.rmi.RmiNetwork;
import peer.useCase.taskExchange.TaskExchangeUseCase;
import rmiinterfaces.TaskNotificationService;

import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;

public class ITaskNotificationService extends UnicastRemoteObject implements TaskNotificationService {
    private final RmiNetwork rmiNetwork;
    private final TaskExchangeUseCase taskExchangeUseCase;

    public ITaskNotificationService(RmiNetwork rmiNetwork, TaskExchangeUseCase taskExchangeUseCase) throws RemoteException {
        super();
        this.rmiNetwork = rmiNetwork;
        this.taskExchangeUseCase = taskExchangeUseCase;
    }

    @Override
    public void notifyTask(Task remoteTask) throws RemoteException {
        try {
            String nodeIp = getClientHost();
            if(rmiNetwork.containsNode(nodeIp)){
                taskExchangeUseCase.handleRemoteTask(remoteTask);
            }
            else{
                throw new RemoteException("Unknown sender");
            }
        } catch (ServerNotActiveException ignored) {}

        //receivedTaskUseCase.handleReceivedTask(remoteTask);
    }
}
