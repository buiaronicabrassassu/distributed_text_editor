package peer.network.rmi.service.initialization;

import peer.network.rmi.RmiNetwork;
import peer.network.rmi.RmiUtils;
import peer.network.rmi.node.LeaderNode;
import peer.useCase.initialization.DocumentInitializationService;

public class RmiDocumentInitializationService implements DocumentInitializationService {
    private final RmiNetwork network;

    public RmiDocumentInitializationService(RmiNetwork network){
        this.network = network;
    }

    @Override
    public void waitForLeader() {
        LeaderNode leader = network.getLeaderPeer();

        RmiUtils.tryNTimes(()->{
            leader.sendHeartBeat();
            return null;
        },2, 2000);

        RmiUtils.tryNTimes(()->{
            leader.waitForLoginService();
            return null;
        },2, 2000);



    }
}
