package peer.network.rmi.service.login;

import entities.Version;
import peer.network.rmi.RmiNetwork;
import peer.useCase.login.NetworkLoginService;

import java.util.concurrent.CompletableFuture;

public class RmiLeaderLoginService implements NetworkLoginService {

    private RmiNetwork network;

    public RmiLeaderLoginService(RmiNetwork network){
        this.network = network;
    }

    @Override
    public CompletableFuture<Version> login() {
        CompletableFuture<Version> version = new CompletableFuture<>();

        version.complete(network.getServerNode().getCurrentVersion());
        return version;
    }
}
