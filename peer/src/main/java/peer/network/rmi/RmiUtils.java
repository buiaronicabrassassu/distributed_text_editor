package peer.network.rmi;

import peer.Log;

import java.util.concurrent.*;

public class RmiUtils {
    private static int DEFAULT_ATTEMPTS = 5;
    private static int DEFAULT_WAITING_TIME = 2000;

    static public <R> R tryNTimes(final Callable<R> functionToExecute, final int attemptsNumber, final int waitingTime){

        final ScheduledExecutorService scheduledExecutor = Executors.newScheduledThreadPool(1);
        int attempts = 0;
        try {
            while (attempts < attemptsNumber) {
                try {
                    return scheduledExecutor.schedule(functionToExecute, waitingTime * attempts, TimeUnit.MILLISECONDS).get();
                } catch (InterruptedException | ExecutionException e) {
                    Log.print("Exception during attempt "+attempts);
                    ++attempts;
                    if (attempts == attemptsNumber)
                        throw new RuntimeException("All attempts failed");
                }
            }
            return null;
        } finally {
            scheduledExecutor.shutdownNow();
        }
    }

    /**
     * blocking function
     * It waits until the callable passed as parameter complete successfully.
     * If it fails after N tries throw an exception
     */
    static public <R> R tryNTimes(Callable<R> functionToExecute){
        return tryNTimes(functionToExecute, DEFAULT_ATTEMPTS, DEFAULT_WAITING_TIME);
    }
}
