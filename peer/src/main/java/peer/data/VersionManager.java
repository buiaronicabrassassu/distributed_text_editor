package peer.data;

import entities.Version;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;

public class VersionManager{

    public final AtomicReference<Version> currentVersion = new AtomicReference<>();

    public VersionManager(){
        clear();
    }

    public void updateCurrentVersion(Version version){
        this.currentVersion.set(version);
    }

    public Version getCurrentVersion(){
        return this.currentVersion.get();
    }

    public int getCurrentVersionId(){
        return this.currentVersion.get().id;
    }

    public void clear() {
        this.currentVersion.set(new Version(-1, new ArrayList<>()));
    }
}