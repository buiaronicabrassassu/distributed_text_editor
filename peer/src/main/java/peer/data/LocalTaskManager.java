package peer.data;

import entities.Character;
import entities.Task;
import peer.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class LocalTaskManager {
    private final Map<String, Task> notifiedTasks = new ConcurrentHashMap<>();;

    public synchronized void insertTask(Task task) {
        notifiedTasks.putIfAbsent(task.getKey(), task);
    }

    public synchronized void clear(){
        getNotifiedTasks().forEach((task) -> {
            notifiedTasks.remove(task.getKey());
        });
    }

    public void deleteTasks(int versionId){
        getNotifiedTasks().forEach((task) -> {
            if(task.character.version < versionId){
                notifiedTasks.remove(task.getKey());
            }
        });

        Log.print("Tasks with version id lower than " + versionId + " deleted");
    }

    public synchronized List<Task> getNotifiedTasks(){
        return new ArrayList<>(notifiedTasks.values());
    }

    public List<Task> getTasksForNewVersion(int versionId){
        List<Task> tasks = getNotifiedTasks().stream()
                .filter((task) -> {
                    boolean isNotToFilter = task.version < versionId;
                    return isNotToFilter;
                })
                .collect(Collectors.toList());

        printTasks(new ArrayList<>(tasks));

        return tasks;

    }

    public void printTasks(List<Task> tasks){
        tasks.sort((t1,t2)->{return Character.compare(t1.character, t2.character);});

        AtomicReference<String> currentType = new AtomicReference<>(null);
        StringBuilder output = new StringBuilder();
        tasks.forEach((task)->{
            if(currentType.get() == null || !currentType.get().equals(task.type)){
                if(currentType.get()!=null){
                    output.append("\n");
                }
                output.append(task.type).append(":");
                currentType.set(task.type);
            }
            output.append(" ");
            if(task.character.value == '\n'){
                output.append("\\n");
            }
            else{
                output.append(task.character.value);
            }
        });

        System.out.println("================= New Version Tasks =================");
        System.out.println(output.toString());
        System.out.println("=====================================================");
    }
}
