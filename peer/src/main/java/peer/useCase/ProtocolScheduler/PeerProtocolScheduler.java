package peer.useCase.ProtocolScheduler;

import peer.Log;

public class PeerProtocolScheduler extends ProtocolScheduler{

    public PeerProtocolScheduler(ProtocolManager protocolManager){
        super(protocolManager);
    }

    @Override
    public void schedule(){
        Protocol protocol;

        if(!protocolManager.isRunningProtocolEmpty()){
            return;
        }

        protocol = protocolManager.extractInitializationProtocol();
        if(protocol != null){
            executeProtocol(protocol);
            return;
        }

        protocol = protocolManager.extractLoginProtocol();
        if(protocol != null){
            executeProtocol(protocol);
            return;
        }

        protocol = protocolManager.extractLeaderDownProtocol();
        if(protocol != null){
            executeProtocol(protocol);
            return;
        }

        if(protocolManager.newVersionProtocolRequestPending()){
            protocol = protocolManager.extractNodeDisconnectionProtocol();
            if(protocol != null){
                executeProtocol(protocol);
                return;
            }

            protocol = protocolManager.extractNodeLoginProtocol();
            if(protocol != null){
                executeProtocol(protocol);
                return;
            }

            protocol = protocolManager.extractNewVersionProtocol();
            executeProtocol(protocol);

            return;
        }
        else{
            protocol = protocolManager.extractDisconnectionProtocol();
            if(protocol != null){
                executeProtocol(protocol);
                return;
            }

            Protocol nodeDisconnection = protocolManager.extractNodeDisconnectionProtocol();
            if(nodeDisconnection != null){
                executeProtocol(nodeDisconnection);
                return;
            }

            Protocol nodeLogin = protocolManager.extractNodeLoginProtocol();
            if(nodeLogin != null){
                executeProtocol(nodeLogin);
                return;
            }
        }

        protocol = protocolManager.extractGuiInitProtocol();
        if(protocol != null){
            Log.print("Run Gui initialization Protocol");
            executeProtocol(protocol);
            return;
        }


        Log.print("There is no protocol to execute");
    }

    /*public synchronized void scheduleProtocols(){
        
        if(*//*disconnection running && new version protocol pending*//*){
            //run new version protocolo
            //ci saranno due protocollo in esecuzione nello stesso momento
        }

        if(true*//*running protocols is not empty*//*){
            return;
        }

        if(*//*new version protocol pending*//*){

            if(*//*node_disconnection pending*//*){
                //run node disconnection
            }

            if(*//*node_login pending*//*){
                *//*node_login pending*//*
            }

            //run new version protocol
        }

        if(*//*disconnect request pending*//*){
            //run leader disconnection
        }

        //nothing to run
        return;

    }*/

    /*public void runningProtocolCompleted(){
        //remove completerd protocol
        scheduleProtocols();
    }*/
}
