package peer.useCase.ProtocolScheduler;

public interface UniqueInstanceProtocol extends Protocol{
    String INITIALIZATION="Initialization";
    String NEW_VERSION = "New_Version";
    String LOGIN = "Login";
    String DISCONNECTION = "Disconnection";
    String LEADER_DOWN = "Leader_down";
    String GUI_INITIALIZATION = "Gui_initialization";
}
