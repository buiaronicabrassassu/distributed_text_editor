package peer.useCase.ProtocolScheduler;

import peer.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

public class ProtocolManager{
    final AtomicReference<Protocol> runningProtocol = new AtomicReference<>(null);
    final AtomicReference<CompletableFuture<Void>> promiseRunningProtocol = new AtomicReference<>(null);

    final ConcurrentHashMap<String, UniqueInstanceProtocol> singleProtocolsRequests =  new ConcurrentHashMap<>();
    final ConcurrentHashMap<String, List<MultiInstanceProtocol>> multiProtocolsRequests = new ConcurrentHashMap<>();

    public ProtocolManager(){}

    void addProtocolRequest(UniqueInstanceProtocol protocol){
        synchronized (singleProtocolsRequests){
            if(!singleProtocolsRequests.containsKey(protocol.getName())){
                singleProtocolsRequests.put(protocol.getName(), protocol);
            }
            else{
                Log.print(protocol.getName()+ " field present, replace");
                singleProtocolsRequests.replace(protocol.getName(), protocol);
            }
        }

    }

    void addProtocolRequest(MultiInstanceProtocol protocol){
        synchronized(multiProtocolsRequests){
            if(!multiProtocolsRequests.containsKey(protocol.getName())){
                multiProtocolsRequests.put(protocol.getName(), new ArrayList<>());
            }
            Log.print("put "+protocol.getName()+" protocol in the protocols list");
            multiProtocolsRequests.get(protocol.getName()).add(protocol);
        }
    }

    boolean isRunningProtocolEmpty(){
        synchronized (runningProtocol){
            return runningProtocol.get() == null;
        }
    }

    public void removeRunningProtocol() {
        runningProtocol.set(null);
    }

    public synchronized void setRunningProtocol(Protocol protocol, CompletableFuture<Void> protocolPromise){
        runningProtocol.set(protocol);
        promiseRunningProtocol.set(protocolPromise);
    }

    private MultiInstanceProtocol extractFromMultiProtocol(String protocolName){
        synchronized (multiProtocolsRequests){
            try{
                List<MultiInstanceProtocol> protocols = multiProtocolsRequests.get(protocolName);
                return protocols.remove(0);
            }catch(RuntimeException e){
                return null;
            }
        }
    }

    public UniqueInstanceProtocol extractFromSingleProtocol(String protocolName) {
        return singleProtocolsRequests.remove(protocolName);
    }

    public Protocol extractLoginProtocol() {
        synchronized (singleProtocolsRequests){
            return extractFromSingleProtocol(UniqueInstanceProtocol.LOGIN);
        }
    }

    public Protocol extractInitializationProtocol() {
        synchronized (multiProtocolsRequests){
            return extractFromSingleProtocol(UniqueInstanceProtocol.INITIALIZATION);
        }
    }

    public Protocol extractGuiInitProtocol() {
        synchronized (singleProtocolsRequests){
            return extractFromSingleProtocol(UniqueInstanceProtocol.GUI_INITIALIZATION);
        }
    }

    public Protocol extractNodeLoginProtocol() {
        synchronized (multiProtocolsRequests){
            return extractFromMultiProtocol(MultiInstanceProtocol.NODE_LOGIN);
        }
    }

    public Protocol extractNewVersionProtocol(){
        synchronized (singleProtocolsRequests){
            return extractFromSingleProtocol(UniqueInstanceProtocol.NEW_VERSION);
        }
    }

    public boolean newVersionProtocolRequestPending(){
        synchronized (singleProtocolsRequests){
            return singleProtocolsRequests.get(UniqueInstanceProtocol.NEW_VERSION) != null;
        }
    }

    public Protocol extractNodeDownProtocol(){
        synchronized (multiProtocolsRequests){
            return extractFromMultiProtocol(MultiInstanceProtocol.NODE_DOWN);
        }
    }

    public Protocol extractDisconnectionProtocol(){
        synchronized (singleProtocolsRequests){
            return extractFromSingleProtocol(UniqueInstanceProtocol.DISCONNECTION);
        }
    }

    public Protocol extractNodeDisconnectionProtocol() {
        synchronized (multiProtocolsRequests){
            return extractFromMultiProtocol(MultiInstanceProtocol.NODE_DISCONNECTION);
        }
    }

    public Protocol extractLeaderDownProtocol() {
        synchronized (singleProtocolsRequests){
            return extractFromSingleProtocol(UniqueInstanceProtocol.LEADER_DOWN);
        }
    }

    public void clear() {
        UniqueInstanceProtocol guiInitialization = extractFromSingleProtocol(UniqueInstanceProtocol.GUI_INITIALIZATION);
        singleProtocolsRequests.clear();
        multiProtocolsRequests.clear();
        if(guiInitialization != null){
            addProtocolRequest(guiInitialization);
        }

    }

    public void fail() {
        synchronized (runningProtocol){
            Protocol currentProtocol = runningProtocol.get();
            if(currentProtocol!=null) {
                String protocolName = currentProtocol.getName();
                if (protocolName.equals(UniqueInstanceProtocol.NEW_VERSION)||(protocolName.equals(UniqueInstanceProtocol.DISCONNECTION))) {
                    promiseRunningProtocol.get().completeExceptionally(new RuntimeException("Protocol failed"));
                }
            }
        }
    }


}