package peer.useCase.ProtocolScheduler;

import peer.Log;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public abstract class ProtocolScheduler {
    protected final ProtocolManager protocolManager;
    private final Executor executor = Executors.newFixedThreadPool(4);

    protected ProtocolScheduler(ProtocolManager protocolManager) {
        this.protocolManager = protocolManager;
    }

    public void scheduleProtocol(MultiInstanceProtocol protocol){
        Log.print("Add Protocol "+protocol.getName());
        protocolManager.addProtocolRequest(protocol);
        schedule();
    }

    public void scheduleProtocol(UniqueInstanceProtocol protocol){
        Log.print("Add Protocol "+protocol.getName());
        protocolManager.addProtocolRequest(protocol);
        schedule();
    }

    abstract void schedule();

    void executeProtocol(Protocol protocol){
        CompletableFuture<Void> protocolPromise = protocol.execute(executor);
        protocolManager.setRunningProtocol(protocol, protocolPromise);
        protocolPromise
                .thenRunAsync(()->{
                    Log.print("protocol "+protocol.getName()+" completed");
                    protocolManager.removeRunningProtocol();
                    schedule();
                },executor)
                .exceptionally((e)->{
                    e.printStackTrace();
                    Log.print("protocol "+protocol.getName()+" fails");
                    protocolManager.removeRunningProtocol();
                    schedule();
                    return null;
                });
    }
}
