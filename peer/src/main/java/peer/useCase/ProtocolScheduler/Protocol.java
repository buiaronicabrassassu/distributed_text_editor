package peer.useCase.ProtocolScheduler;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

interface Protocol {
    CompletableFuture<Void> execute(Executor executor);
    String getName();
}
