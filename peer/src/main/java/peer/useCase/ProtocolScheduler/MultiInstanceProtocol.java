package peer.useCase.ProtocolScheduler;

public interface MultiInstanceProtocol extends Protocol{
    String NODE_LOGIN = "Node_Login";
    String NODE_DISCONNECTION = "Node_Disconnection";
    String NODE_DOWN = "Node_Down" ;
}