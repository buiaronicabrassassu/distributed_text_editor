package peer.useCase.ProtocolScheduler;

import peer.Log;

public class LeaderProtocolScheduler extends ProtocolScheduler{

    public LeaderProtocolScheduler(ProtocolManager protocolManager){
        super(protocolManager);
    }

    @Override
    public void schedule(){
        synchronized (protocolManager){
            Protocol protocol;
            Log.print("Scheduler called");
            if(!protocolManager.isRunningProtocolEmpty()){
                Log.print("Pending running protocols");
                return;
            }

            protocol = protocolManager.extractInitializationProtocol();
            if(protocol != null){
                Log.print("Run Initialization Protocol");
                executeProtocol(protocol);
                return;
            }

            protocol = protocolManager.extractLoginProtocol();
            if(protocol != null){
                Log.print("Run Login Protocol");
                executeProtocol(protocol);
                return;
            }

            protocol = protocolManager.extractNodeDownProtocol();
            if (protocol != null){
                Log.print("Run Node down Protocol");
                executeProtocol(protocol);
                return;
            }

            protocol = protocolManager.extractNodeDisconnectionProtocol();
            if (protocol != null){
                Log.print("Run Node Disconnection Protocol");
                executeProtocol(protocol);
                return;
            }

            protocol = protocolManager.extractNewVersionProtocol();
            if(protocol != null){
                Log.print("Run New Version Protocol");
                executeProtocol(protocol);
                return;
            }

            protocol = protocolManager.extractGuiInitProtocol();
            if(protocol != null){
                Log.print("Run Gui initialization Protocol");
                executeProtocol(protocol);
                return;
            }

            protocol = protocolManager.extractNodeLoginProtocol();
            if(protocol != null){
                Log.print("Run Node Login Protocol");
                executeProtocol(protocol);
                return;
            }

            protocol = protocolManager.extractDisconnectionProtocol();
            if(protocol != null){
                Log.print("Run Disconnection Protocol");
                executeProtocol(protocol);
                return;
            }
        }

        Log.print("There is no protocol to execute");
    }
}
