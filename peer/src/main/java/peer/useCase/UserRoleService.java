package peer.useCase;

public interface UserRoleService {
    public boolean amILeader();
}
