package peer.useCase.newVersion;

import entities.Task;
import entities.Version;
import peer.Log;
import peer.data.LocalTaskManager;
import peer.data.VersionManager;
import peer.useCase.ProtocolScheduler.ProtocolScheduler;
import peer.useCase.ProtocolScheduler.UniqueInstanceProtocol;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

public class PeerNewVersionUseCase {
    private final VersionManager versionManager;
    private final LocalTaskManager localTaskManager;
    private final PeerNetworkNewVersionService peerNetworkNewVersionService;
    private final GuiVersionUpdater guiVersionUpdater;
    private final ProtocolScheduler protocolScheduler;

    public PeerNewVersionUseCase(VersionManager versionManager, LocalTaskManager localTaskManager, PeerNetworkNewVersionService peerNetworkNewVersionService, GuiVersionUpdater guiVersionUpdater, ProtocolScheduler protocolScheduler){
        this.versionManager = versionManager;
        this.localTaskManager = localTaskManager;
        this.peerNetworkNewVersionService = peerNetworkNewVersionService;
        this.guiVersionUpdater = guiVersionUpdater;
        this.protocolScheduler = protocolScheduler;
    }

    /**
     * This is called to start the new version protocol
     * */
    public void startNewVersionProtocol(int versionId){
        Log.print("======= Started new version protocol =======");

        UniqueInstanceProtocol protocol = new NewVersionProtocol(versionId);
        protocolScheduler.scheduleProtocol(protocol);
    }


    private class NewVersionProtocol implements UniqueInstanceProtocol {
        private final int versionId;

        private NewVersionProtocol(int newVersionId) {
            this.versionId = newVersionId;
        }

        @Override
        public CompletableFuture<Void> execute(Executor executor){
            CompletableFuture<Void> promise = CompletableFuture
                    .runAsync(() -> {
                    }, executor)
                    .thenCompose((none) -> guiVersionUpdater.setVersionId(versionId))
                    .thenApply((none) -> localTaskManager.getTasksForNewVersion(versionId))
                    .thenAcceptAsync((oldTasks) -> peerNetworkNewVersionService.sendNewVersionMessage(versionId, oldTasks), executor)
                    .thenCompose((none) -> peerNetworkNewVersionService.waitForLeaderAck())
                    .thenAccept((List<List<Task>> newVersionTasks) -> {
                        Version newVersion = versionManager.getCurrentVersion().newVersion(versionId, newVersionTasks);

                        System.out.println("=================== New Version ==================");
                        System.out.println(newVersion.toString());
                        System.out.println("=================================================");
                        versionManager.updateCurrentVersion(newVersion);
                        Log.print("New version computed and version manager updated");

                        localTaskManager.deleteTasks(versionId);

                        guiVersionUpdater.setVersionData(newVersion);
                    });

                    promise.exceptionally((e) -> {
                        System.out.println("New Version Protocol failed");
                        return null;
                    });

            return promise;

        }

        @Override
        public String getName() {
            return UniqueInstanceProtocol.NEW_VERSION;
        }
    }
}
