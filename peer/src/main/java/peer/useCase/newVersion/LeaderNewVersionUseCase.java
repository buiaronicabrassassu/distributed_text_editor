package peer.useCase.newVersion;

import entities.Version;
import peer.Log;
import peer.data.LocalTaskManager;
import peer.data.VersionManager;
import peer.useCase.ProtocolScheduler.ProtocolScheduler;
import peer.useCase.ProtocolScheduler.UniqueInstanceProtocol;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;

public class LeaderNewVersionUseCase{
    public final int NEW_VERSION_INTERVALL = 15;
    private final VersionManager versionManager;
    private final LocalTaskManager localTaskManager;
    private final GuiVersionUpdater guiVersionUpdater;
    private final LeaderNetworkNewVersionService leaderNetworkNewVersionService;
    private final ProtocolScheduler protocolScheduler;
    private final AtomicReference<CompletableFuture<Void>> versionProtocolScheduler = new AtomicReference<>(CompletableFuture.completedFuture(null));

    public LeaderNewVersionUseCase(
            VersionManager versionManager,
            LocalTaskManager localTaskManager,
            LeaderNetworkNewVersionService leaderNetworkNewVersionService,
            GuiVersionUpdater guiVersionUpdater,
            ProtocolScheduler protocolScheduler){
        this.versionManager = versionManager;
        this.localTaskManager = localTaskManager;
        this.leaderNetworkNewVersionService = leaderNetworkNewVersionService;
        this.guiVersionUpdater = guiVersionUpdater;
        this.protocolScheduler = protocolScheduler;
    }

    public void schedulePeriodicVersionProtocol(){
        synchronized(versionProtocolScheduler){
            abortScheduledNewVersionProtocol();
            versionProtocolScheduler.set(CompletableFuture.runAsync(()->{
                try {
                    Thread.sleep(NEW_VERSION_INTERVALL*1000);
                    createNewVersionProtocol();
                } catch (InterruptedException e){
                    Log.print("Scheduled new version protocol interrupted");
                }
            }));
        }
    }

    public void abortScheduledNewVersionProtocol(){
        synchronized(versionProtocolScheduler){
            if(!versionProtocolScheduler.get().isDone()){
                versionProtocolScheduler.get().cancel(true);
            }
        }
    }

    private void createNewVersionProtocol(){
        synchronized(versionManager){
            int newVersionId = versionManager.getCurrentVersionId() + 1;
            NewVersionProtocol protocol = new NewVersionProtocol(newVersionId);
            protocolScheduler.scheduleProtocol(protocol);
        }
    }

    private class NewVersionProtocol implements UniqueInstanceProtocol{
        private final int newVersionId;

        private NewVersionProtocol(int newVersionId) {
            this.newVersionId = newVersionId;
        }

        @Override
        public CompletableFuture<Void> execute(Executor executor) {

            CompletableFuture<Void> promise = CompletableFuture
                    .runAsync(() -> Log.print("======= Started new version protocol ======="), executor)
                    .thenCompose((none) -> guiVersionUpdater.setVersionId(newVersionId))
                    .thenRun(() ->leaderNetworkNewVersionService.sendNewVersionMessage(newVersionId, localTaskManager.getTasksForNewVersion(newVersionId)))
                    .thenCompose((none) -> leaderNetworkNewVersionService.waitForPeerTasks())
                    .thenAccept((peerTasks) -> {

                        Version newVersion = versionManager.getCurrentVersion().newVersion(newVersionId, peerTasks);

                        leaderNetworkNewVersionService.notifyNewVersionToServer(newVersion);
                        versionManager.updateCurrentVersion(newVersion);

                        localTaskManager.deleteTasks(newVersionId);

                        guiVersionUpdater.setVersionData(newVersion);

                    }).thenRunAsync(()->{
                        Log.print("New Version Protocol Completed.");
                        leaderNetworkNewVersionService.sendNewVersionAck();
                    },executor);

            promise
                    .exceptionally((e)->{
                        leaderNetworkNewVersionService.sendNewVersionFailed();
                        return null;
                    })
                    .thenRun(()->{
                        schedulePeriodicVersionProtocol();

                    });

            return promise;
        }

        @Override
        public String getName() {
            return UniqueInstanceProtocol.NEW_VERSION;
        }
    }
}

