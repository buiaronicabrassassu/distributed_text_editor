package peer.useCase.newVersion;

import entities.Task;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface PeerNetworkNewVersionService {

    CompletableFuture<List<List<Task>>> waitForLeaderAck();

    void sendNewVersionMessage(int versionId, List<Task> updates);

}
