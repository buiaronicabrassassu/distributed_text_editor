package peer.useCase.newVersion;

import entities.Task;
import entities.Version;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface LeaderNetworkNewVersionService {
    void sendNewVersionMessage(int versionId, List<Task> updates);
    CompletableFuture<List<List<Task>>> waitForPeerTasks();
    void sendNewVersionAck();
    void sendNewVersionFailed();
    void notifyNewVersionToServer(Version newVersion);
}
