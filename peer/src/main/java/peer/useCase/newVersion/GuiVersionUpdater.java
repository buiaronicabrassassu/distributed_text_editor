package peer.useCase.newVersion;

import entities.Version;

import java.util.concurrent.CompletableFuture;

public interface GuiVersionUpdater {
    CompletableFuture<Void> setVersionId(int versionId);
    void setVersionData(Version newVersion);
}
