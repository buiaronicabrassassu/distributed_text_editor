package peer.useCase;

public interface GuiController {
    void startServer();
    void stopServer();
}
