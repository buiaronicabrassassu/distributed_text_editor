package peer.useCase.nodeDown;

import entities.Node;

public interface NetworkNodeDownService {
    boolean isNodeDown(Node node);
}
