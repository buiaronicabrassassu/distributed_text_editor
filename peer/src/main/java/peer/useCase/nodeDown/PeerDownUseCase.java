package peer.useCase.nodeDown;

import entities.Node;
import peer.Log;
import peer.useCase.ProtocolScheduler.MultiInstanceProtocol;
import peer.useCase.ProtocolScheduler.ProtocolScheduler;
import peer.useCase.disconnection.NetworkDisconnectionService;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;

/**
 * Used only by the leader
 * */
public class PeerDownUseCase {
    private volatile NetworkNodeDownService networkNodeDownService = null;
    private volatile NetworkDisconnectionService disconnectionService = null;
    private final ProtocolScheduler protocolScheduler;
    private final ConcurrentHashMap<Integer, PeerDownProtocol> pendingPeerDownProtocols = new ConcurrentHashMap<>();

    public PeerDownUseCase(ProtocolScheduler protocolScheduler){
        this.protocolScheduler = protocolScheduler;
    }

    public synchronized void setNetworkServices(
            NetworkNodeDownService networkNodeDownService,
            NetworkDisconnectionService disconnectionService){
        this.networkNodeDownService = networkNodeDownService;
        this.disconnectionService = disconnectionService;
    }

    public void handlePeerDown(Node peer){
        if(!isInitialized()){
            throw new IllegalStateException("The PeerDownUseCase is not initialized");
        }
        PeerDownProtocol protocol;
        synchronized (pendingPeerDownProtocols){
            if(!pendingPeerDownProtocols.containsKey(peer.getId())){
                protocol = new PeerDownProtocol(peer);
                pendingPeerDownProtocols.put(peer.getId(),protocol);
            }
            else{
                return;
            }
        }

        protocolScheduler.scheduleProtocol(protocol);
    }

    private synchronized boolean isInitialized(){
        return (networkNodeDownService != null && disconnectionService != null);
    }

    private void removePendingProtocolFor(Node peer){
        Log.print("Remove pending peerDown of" + peer.getUserName());
        pendingPeerDownProtocols.remove(peer.getId());
    }


    private class PeerDownProtocol implements MultiInstanceProtocol {
        private final Node peer;

        PeerDownProtocol(final Node peer) {
            this.peer = peer;
        }

        @Override
        public CompletableFuture<Void> execute(Executor executor){
            return CompletableFuture
                    .supplyAsync(()->{
                        Log.print("Received node down request for peer " + peer.getUserName());
                        return networkNodeDownService.isNodeDown(peer);
                    },executor)
                    .thenAcceptAsync((isDown)->{
                        if(isDown){
                            Log.print("The peer " + peer.getUserName() + " is down. Notify to all peers.");
                            disconnectionService.notifyNodeDisconnection(peer, new ArrayList<>());
                        }
                        else{
                            Log.print("The peer " + peer.getUserName() + " is not down");
                        }
                    },executor)
                    .thenRun(()->removePendingProtocolFor(peer));
        }

        @Override
        public String getName() {
            return MultiInstanceProtocol.NODE_DOWN;
        }
    }

}
