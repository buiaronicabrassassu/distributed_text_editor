package peer.useCase.nodeDown;

import entities.Node;
import peer.Log;
import peer.useCase.ProtocolScheduler.ProtocolScheduler;
import peer.useCase.ProtocolScheduler.UniqueInstanceProtocol;
import peer.useCase.initialization.NodeInitializationUseCase;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

/**
 * Used only by the peer
 * */
public class LeaderDownUseCase {
    private final NodeInitializationUseCase nodeInitializer;
    private final ProtocolScheduler protocolScheduler;
    private final NetworkNodeDownService networkNodeDownService;
    private final GuiLeaderDownService guiLeaderDownService;

    public LeaderDownUseCase(NodeInitializationUseCase nodeInitializer, ProtocolScheduler protocolScheduler, NetworkNodeDownService networkNodeDownService, GuiLeaderDownService guiLeaderDownService){
        this.nodeInitializer = nodeInitializer;
        this.protocolScheduler = protocolScheduler;
        this.networkNodeDownService = networkNodeDownService;
        this.guiLeaderDownService = guiLeaderDownService;
    }

    /**
     * Leader down notification received by another peer
     * */
    public void handleNotificationOfLeaderDown(Node leader){
        boolean isLeaderDown = networkNodeDownService.isNodeDown(leader);
        if(isLeaderDown){
            protocolScheduler.scheduleProtocol(new LeaderDownUseCase.LeaderDownProtocol(leader));
        }
        else{
            Log.print("The leader " + leader.getUserName() + " is not down");
        }
    }

    /**
     * I realize that the leader is down
     * */
    public void handleLeaderDown(Node leader) {
        protocolScheduler.scheduleProtocol(new LeaderDownUseCase.LeaderDownProtocol(leader));
    }


    private class LeaderDownProtocol implements UniqueInstanceProtocol {
        private final Node leader;

        LeaderDownProtocol(final Node leader) {
            this.leader = leader;
        }

        //TODO: mettere invio dei messaggi di leader down ai vari peer in modo che si accorgono anche loro che è caduto e blocchino l'interfaccia

        @Override
        public CompletableFuture<Void> execute(Executor executor){
            return CompletableFuture
                    .runAsync(()->{
                        Log.print("Received leader down for leader: " + leader.getUserName());
                        guiLeaderDownService.leaderDownStart();
                        nodeInitializer.initializeAfterLeaderDown(leader);
                    },executor);
        }

        @Override
        public String getName() {
            return UniqueInstanceProtocol.LEADER_DOWN;
        }
    }
}
