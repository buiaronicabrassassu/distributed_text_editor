package peer.useCase.nodeDown;

import entities.Version;

public interface GuiLeaderDownService {
    void leaderDownStart();
    void leaderDownCompleted(Version version);
}
