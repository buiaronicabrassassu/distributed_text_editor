package peer.useCase.guiInitialization;

import entities.User;
import entities.Version;

public interface GuiInitializationService {
    void initializationCompleted(User user, Version version);
}
