package peer.useCase.guiInitialization;

import entities.User;
import entities.Version;
import peer.Log;
import peer.api.gui.commands.InitDataCommand;
import peer.data.VersionManager;
import peer.useCase.ProtocolScheduler.ProtocolScheduler;
import peer.useCase.ProtocolScheduler.UniqueInstanceProtocol;
import peer.useCase.initialization.PeerInitializerUseCase;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

public class GuiInitializationUseCase {
    private final VersionManager versionManager;
    private final ProtocolScheduler protocolScheduler;
    private final User user;
    private final GuiInitializationService guiInitializationService;

    public GuiInitializationUseCase(VersionManager versionManager, ProtocolScheduler protocolScheduler, User user, GuiInitializationService guiInitializationService){
        this.versionManager=versionManager;
        this.protocolScheduler = protocolScheduler;
        this.user = user;
        this.guiInitializationService = guiInitializationService;
    }

    public void handleGuiInitializationRequest(){
        protocolScheduler.scheduleProtocol(new GuiInitializationProtocol());
    }

    private class GuiInitializationProtocol implements UniqueInstanceProtocol {

        @Override
        public CompletableFuture<Void> execute(Executor executor) {
            return CompletableFuture
                    .runAsync(()->{
                        Version currentVersion = versionManager.getCurrentVersion();
                        if(currentVersion.id < 0){
                            handleGuiInitializationRequest();
                        }
                        else{
                            guiInitializationService.initializationCompleted(user, currentVersion);
                        }
                    }, executor);
        }

        @Override
        public String getName() {
            return UniqueInstanceProtocol.GUI_INITIALIZATION;
        }
    }
}
