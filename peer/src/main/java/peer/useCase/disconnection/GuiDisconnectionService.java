package peer.useCase.disconnection;

import java.util.concurrent.CompletableFuture;

public interface GuiDisconnectionService {
    public CompletableFuture<Void> confirmDisconnection();
}