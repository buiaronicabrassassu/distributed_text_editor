package peer.useCase.disconnection;

import entities.Task;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface NodeDisconnectionService{
    public CompletableFuture<Void> disconnectCurrentNode(List<Task> nodeTasks);
}