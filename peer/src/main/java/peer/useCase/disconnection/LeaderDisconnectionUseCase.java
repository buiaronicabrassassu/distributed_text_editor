package peer.useCase.disconnection;

import entities.Node;
import entities.Task;
import entities.Version;
import peer.Log;
import peer.data.LocalTaskManager;
import peer.data.VersionManager;
import peer.useCase.GuiController;
import peer.useCase.ProtocolScheduler.MultiInstanceProtocol;
import peer.useCase.ProtocolScheduler.ProtocolScheduler;
import peer.useCase.ProtocolScheduler.UniqueInstanceProtocol;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

public class LeaderDisconnectionUseCase extends DisconnectionUseCase{
    LocalTaskManager localTaskManager;
    NetworkDisconnectionService networkDisconnectionService;
    GuiDisconnectionService guiDisconnectionService;

    public LeaderDisconnectionUseCase(
            LocalTaskManager localTaskManager,
            VersionManager versionManager,
            GuiController guiController,
            NetworkDisconnectionService networkDisconnectionService,
            GuiDisconnectionService guiDisconnectionService,
            ProtocolScheduler protocolScheduler){
        super(versionManager, guiController, protocolScheduler);

        this.localTaskManager = localTaskManager;
        this.networkDisconnectionService = networkDisconnectionService;
        this.guiDisconnectionService = guiDisconnectionService;
    }

    @Override
    public UniqueInstanceProtocol getDisconnectionProtocol(){
        return new DisconnectionProtocol(localTaskManager.getNotifiedTasks());
    }

    public void handlePeerDisconnectionRequest(Node node, List<Task> nodeTasks){
        protocolScheduler.scheduleProtocol(new NodeDisconnectionProtocol(node, nodeTasks));
    }

    private class DisconnectionProtocol implements UniqueInstanceProtocol {
        private final List<Task> nodeTasks;

        private DisconnectionProtocol(List<Task> nodeTasks) {
            this.nodeTasks = nodeTasks;
        }

        @Override
        public CompletableFuture<Void> execute(Executor executor) {
            return CompletableFuture
                    .runAsync(()->Log.print("Leader disconnection protocol started"),executor)
                    .thenApply((none)-> updateVersion(nodeTasks))
                    .thenAcceptAsync(networkDisconnectionService::updateServerVersion,executor)
                    .thenRunAsync(()->networkDisconnectionService.notifyLeaderDisconnection(nodeTasks),executor)
                    .thenCompose((none)-> guiDisconnectionService.confirmDisconnection())
                    .thenRun(()->{
                        Log.print(" Leader disconnection protocol completed");
                        System.exit(0);
                    });
        }

        @Override
        public String getName() {
            return UniqueInstanceProtocol.DISCONNECTION;
        }
    }

    private class NodeDisconnectionProtocol implements MultiInstanceProtocol{
        private final Node node;
        private final List<Task> nodeTasks;

        NodeDisconnectionProtocol(final Node node, final List<Task> nodeTasks){
            this.node = node;
            this.nodeTasks = nodeTasks;
        }

        @Override
        public CompletableFuture<Void> execute(Executor executor) {
            return CompletableFuture
                    .runAsync(()->Log.print("Disconnecting peer "+node.getUserName()),executor)
                    .thenRunAsync(()->networkDisconnectionService.notifyNodeDisconnection(node, nodeTasks),executor)
                    .thenApply((none)->updateVersion(nodeTasks))
                    .thenAcceptAsync((version)->networkDisconnectionService.updateServerVersion(version),executor)
                    .thenRunAsync(()->{
                        Log.print("Sending disconnection confirm to "+ node.getUserName());
                        networkDisconnectionService.confirmDisconnection(node);
                        Log.print("Disconnection of "+node.getUserName()+" completed");
                    },executor);
        }

        @Override
        public String getName() {
            return MultiInstanceProtocol.NODE_DISCONNECTION;
        }
    }
}
