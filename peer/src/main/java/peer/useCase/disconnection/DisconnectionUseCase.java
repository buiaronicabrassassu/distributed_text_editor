package peer.useCase.disconnection;

import entities.Task;
import entities.Version;
import peer.Log;
import peer.data.VersionManager;
import peer.useCase.GuiController;
import peer.useCase.ProtocolScheduler.ProtocolScheduler;
import peer.useCase.ProtocolScheduler.UniqueInstanceProtocol;


import java.util.List;


public abstract class DisconnectionUseCase {
    protected final GuiController guiController;
    protected final VersionManager versionManager;
    protected final ProtocolScheduler protocolScheduler;

    DisconnectionUseCase(VersionManager versionManager, GuiController guiController, ProtocolScheduler protocolScheduler){
        this.guiController = guiController;
        this.versionManager = versionManager;
        this.protocolScheduler = protocolScheduler;
    }

    public void disconnect(){
        protocolScheduler.scheduleProtocol(getDisconnectionProtocol());
    }

    protected abstract UniqueInstanceProtocol getDisconnectionProtocol();


    protected Version updateVersion(List<Task> tasks){
        Version currentVersion = versionManager.getCurrentVersion();
        Version newPartialVersion = currentVersion.newPartialVersion(tasks);
        versionManager.updateCurrentVersion(newPartialVersion);
        return newPartialVersion;
    }

}
