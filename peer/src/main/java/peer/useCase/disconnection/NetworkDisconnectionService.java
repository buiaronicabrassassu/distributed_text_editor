package peer.useCase.disconnection;

import entities.Node;
import entities.Task;
import entities.Version;

import java.util.List;

public abstract class NetworkDisconnectionService {
    public abstract void notifyLeaderDisconnection(List<Task> nodeTasks);
    public abstract void notifyNodeDisconnection(Node node, List<Task> nodeTasks);
    public abstract void confirmDisconnection(Node node);
    public abstract void updateServerVersion(Version version);
}
