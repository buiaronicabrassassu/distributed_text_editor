package peer.useCase.disconnection;

import entities.Node;
import entities.Task;
import peer.Log;
import peer.data.LocalTaskManager;
import peer.data.VersionManager;
import peer.useCase.GuiController;
import peer.useCase.ProtocolScheduler.MultiInstanceProtocol;
import peer.useCase.ProtocolScheduler.ProtocolScheduler;
import peer.useCase.ProtocolScheduler.UniqueInstanceProtocol;
import peer.useCase.initialization.NodeInitializationUseCase;
import peer.useCase.initialization.DocumentInitializationService;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

public class PeerDisconnectionUseCase extends DisconnectionUseCase{
    LocalTaskManager localTaskManager;
    NodeDisconnectionService disconnectionService;
    GuiDisconnectionService guiDisconnectionService;
    NodeInitializationUseCase nodeInitializer;
    private final DocumentInitializationService documentInitializationService;

    public PeerDisconnectionUseCase(
            LocalTaskManager localTaskManager,
            VersionManager versionManager,
            GuiController guiController,
            NodeInitializationUseCase nodeInitializer,
            NodeDisconnectionService disconnectionService,
            DocumentInitializationService documentInitializationService,
            GuiDisconnectionService guiDisconnectionService,
            ProtocolScheduler protocolScheduler
    ){
        super(versionManager, guiController, protocolScheduler);
        this.localTaskManager = localTaskManager;
        this.guiDisconnectionService = guiDisconnectionService;
        this.disconnectionService = disconnectionService;
        this.nodeInitializer = nodeInitializer;
        this.documentInitializationService = documentInitializationService;
    }

    @Override
    public UniqueInstanceProtocol getDisconnectionProtocol(){
        return new DisconnectionProtocol(localTaskManager.getNotifiedTasks());
    }

    public void handlePeerDisconnection(Node node, List<Task> nodeTasks){
        handleNodeDisconnection(node, nodeTasks, false);
    }

    public void handleLeaderDisconnection(Node node, List<Task> nodeTasks){
        handleNodeDisconnection(node, nodeTasks, true);
    }

    private void handleNodeDisconnection(Node node,List<Task> nodeTasks, boolean isLeader){
        protocolScheduler.scheduleProtocol(new NodeDisconnectionProtocol(node, nodeTasks, isLeader));
    }

    private class DisconnectionProtocol implements UniqueInstanceProtocol {
        private final List<Task> nodeTasks;

        private DisconnectionProtocol(List<Task> nodeTasks) {
            this.nodeTasks = nodeTasks;
        }

        @Override
        public CompletableFuture<Void> execute(Executor executor) {
            CompletableFuture<Void> promise = CompletableFuture
                    .runAsync(() -> Log.print("Peer disconnection protocol started"), executor)
                    .thenCompose((none) -> disconnectionService.disconnectCurrentNode(nodeTasks))
                    .thenCompose((none) -> guiDisconnectionService.confirmDisconnection())
                    .thenRun(() -> {
                        Log.print(" Peer disconnection protocol completed");
                        System.exit(0);
                    });

                    promise.exceptionally((e) -> {
                        Log.print("Disconnection protocol fail");
                        return null;
                    })
                    .thenCompose((none) -> guiDisconnectionService.confirmDisconnection())
                    .thenRun(() -> {
                        Log.print(" Peer disconnection protocol completed");
                        System.exit(0);
                    });
            return promise;
        }

        @Override
        public String getName() {
            return UniqueInstanceProtocol.DISCONNECTION;
        }
    }

    private class NodeDisconnectionProtocol implements MultiInstanceProtocol{
        private final Node disconnectingLeader;
        private final List<Task> nodeTasks;
        private final boolean isLeader;

        NodeDisconnectionProtocol(final Node disconnectingLeader, final List<Task> nodeTasks, boolean isLeader){
            this.disconnectingLeader = disconnectingLeader;
            this.nodeTasks = nodeTasks;
            this.isLeader = isLeader;
        }

        @Override
        public CompletableFuture<Void> execute(Executor executor){
            CompletableFuture<Void> baseFuture = CompletableFuture
                    .runAsync(() -> Log.print("Handling Node disconnection"), executor)
                    .thenRun(() -> updateVersion(nodeTasks));
            if(isLeader){
                baseFuture
                        .thenRunAsync(()->{
                            nodeInitializer.initializeAfterLeaderDisconnection(disconnectingLeader);
                        },executor);
            }
            return baseFuture;
        }

        @Override
        public String getName() {
            return MultiInstanceProtocol.NODE_DISCONNECTION;
        }
    }
}
