package peer.useCase.initialization;

import entities.Version;
import peer.data.VersionManager;
import peer.factory.LeaderNodeFactory;
import peer.useCase.ProtocolScheduler.ProtocolScheduler;
import peer.useCase.ProtocolScheduler.UniqueInstanceProtocol;
import peer.useCase.newVersion.LeaderNewVersionUseCase;
import peer.useCase.nodeDown.GuiLeaderDownService;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

public class LeaderInitializerUseCase implements RoleSpecificInitializationUseCase {
    private final NodeInitializationUseCase nodeInitializationUseCase;
    private final ProtocolScheduler protocolScheduler;
    private final GuiLeaderDownService guiLeaderDownService;
    private final VersionManager versionManager;
    private final UniqueInstanceProtocol loginProtocol;
    private final LeaderNewVersionUseCase newVersionUserCase;

    LeaderInitializerUseCase(
            NodeInitializationUseCase nodeInitializationUseCase,
            LeaderNodeFactory factory,
            GuiLeaderDownService guiLeaderDownService,
            VersionManager versionManager){
        this.nodeInitializationUseCase = nodeInitializationUseCase;
        this.guiLeaderDownService = guiLeaderDownService;
        this.versionManager = versionManager;

        factory.buildNodeSpecificServices();
        factory.bindNodeSpecificNetworkServices();
        factory.startNode();

        this.protocolScheduler = factory.protocolScheduler;
        this.loginProtocol = factory.documentLoginUseCase.getLoginProtocol();
        this.newVersionUserCase = factory.leaderNewVersionUseCase;
    }

    @Override
    public void initialize() {
        protocolScheduler.scheduleProtocol(new LeaderInitializationProtocol());
    }

    @Override
    public void initializeAfterLeaderDown() {
        protocolScheduler.scheduleProtocol(new LeaderInitializationProtocolAfterLeaderDown());
    }

    @Override
    public void initializeAfterLeaderDisconnection() {
        protocolScheduler.scheduleProtocol(new LeaderRoleChangeInitializationProtocol());
    }

    @Override
    public void clearRoleSpecificServices() { }

    private class LeaderInitializationProtocol implements UniqueInstanceProtocol {

        @Override
        public CompletableFuture<Void> execute(Executor executor) {
            return loginProtocol.execute(executor)
                    .thenRun(newVersionUserCase::schedulePeriodicVersionProtocol);
        }

        @Override
        public String getName() {
            return UniqueInstanceProtocol.INITIALIZATION;
        }
    }

    private class LeaderInitializationProtocolAfterLeaderDown implements UniqueInstanceProtocol {

        @Override
        public CompletableFuture<Void> execute(Executor executor) {

            return loginProtocol.execute(executor)
                    .thenRunAsync(()->{
                        Version currentVersion = versionManager.getCurrentVersion();
                        guiLeaderDownService.leaderDownCompleted(currentVersion);
                    }, executor)
                    .thenRun(newVersionUserCase::schedulePeriodicVersionProtocol);
        }

        @Override
        public String getName() {
            return UniqueInstanceProtocol.INITIALIZATION;
        }
    }

    private class LeaderRoleChangeInitializationProtocol implements UniqueInstanceProtocol {

        @Override
        public CompletableFuture<Void> execute(Executor executor) {
            newVersionUserCase.schedulePeriodicVersionProtocol();
            return CompletableFuture.completedFuture(null);
        }

        @Override
        public String getName() {
            return UniqueInstanceProtocol.INITIALIZATION;
        }
    }
}
