package peer.useCase.initialization;

import entities.Version;
import peer.Log;
import peer.data.VersionManager;
import peer.factory.PeerNodeFactory;
import peer.useCase.ProtocolScheduler.ProtocolScheduler;
import peer.useCase.ProtocolScheduler.UniqueInstanceProtocol;
import peer.useCase.nodeDown.GuiLeaderDownService;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

public class PeerInitializerUseCase implements RoleSpecificInitializationUseCase {
    private final NodeInitializationUseCase nodeInitializationUseCase;
    private final ProtocolScheduler protocolScheduler;
    private final DocumentInitializationService documentInitializationService;
    private final GuiLeaderDownService guiLeaderDownService;
    private final VersionManager versionManager;
    private final UniqueInstanceProtocol loginProtocol;
    private final NetworkLeaderAliveService networkLeaderAliveService;

    PeerInitializerUseCase(
            NodeInitializationUseCase nodeInitializationUseCase,
            PeerNodeFactory factory,
            GuiLeaderDownService guiLeaderDownService,
            VersionManager versionManager
    ) {
        this.nodeInitializationUseCase = nodeInitializationUseCase;
        this.guiLeaderDownService = guiLeaderDownService;
        this.versionManager = versionManager;

        factory.buildNodeSpecificServices();
        factory.bindNodeSpecificNetworkServices();
        factory.startNode();

        this.protocolScheduler = factory.protocolScheduler;
        this.documentInitializationService = factory.documentInitializationService;
        this.networkLeaderAliveService = factory.rmiPeerNetworkHeartbeatService;
        this.loginProtocol = factory.documentLoginUseCase.getLoginProtocol();
    }

    @Override
    public void initialize() {
        protocolScheduler.scheduleProtocol(new PeerInitializationProtocol());
    }

    @Override
    public void initializeAfterLeaderDown() {
        protocolScheduler.scheduleProtocol(new PeerInitializationProtocolAfterLeaderDown());
    }

    @Override
    public void initializeAfterLeaderDisconnection(){
        protocolScheduler.scheduleProtocol(new InitializeAfterLeaderDisconnection());
    }

    @Override
    public void clearRoleSpecificServices() {
        networkLeaderAliveService.stopHeartBeatScheduling();
    }

    private class InitializeAfterLeaderDisconnection implements UniqueInstanceProtocol {

        @Override
        public CompletableFuture<Void> execute(Executor executor) {
            return CompletableFuture
                    .runAsync(documentInitializationService::waitForLeader,executor)
                    .thenRun(networkLeaderAliveService::startHeartBeatScheduling)
                    .exceptionally((e)->{
                        return null;
                    });
        }

        @Override
        public String getName() {
            return UniqueInstanceProtocol.INITIALIZATION;
        }
    }

    private class PeerInitializationProtocol implements UniqueInstanceProtocol {

        @Override
        public CompletableFuture<Void> execute(Executor executor) {
            return CompletableFuture
                    .runAsync(documentInitializationService::waitForLeader, executor)
                    .thenComposeAsync((none)->loginProtocol.execute(executor),executor)
                    .thenRun(networkLeaderAliveService::startHeartBeatScheduling)
                    .exceptionally((e)->{
                        nodeInitializationUseCase.retryInitialization();
                        return null;
                    });
        }

        @Override
        public String getName() {
            return UniqueInstanceProtocol.INITIALIZATION;
        }
    }

    private class PeerInitializationProtocolAfterLeaderDown implements UniqueInstanceProtocol {

        @Override
        public CompletableFuture<Void> execute(Executor executor) {
            return CompletableFuture
                    .runAsync(()->{
                        Log.print("Initialization after leader down started");
                        documentInitializationService.waitForLeader();
                        Log.print("the leader is ready");
                    },executor)
                    .thenRun(networkLeaderAliveService::startHeartBeatScheduling)
                    .thenComposeAsync((none)->loginProtocol.execute(executor),executor)
                    .thenRun(()->{
                        Log.print("send leader down completed to gui");
                        Version currentVersion = versionManager.getCurrentVersion();
                        guiLeaderDownService.leaderDownCompleted(currentVersion);
                    })
                    .exceptionally((e)->{
                        e.printStackTrace();
                        Log.print("Initialize after leader down failed");
                        return null;
                    });
        }

        @Override
        public String getName() {
            return UniqueInstanceProtocol.INITIALIZATION;
        }
    }
}
