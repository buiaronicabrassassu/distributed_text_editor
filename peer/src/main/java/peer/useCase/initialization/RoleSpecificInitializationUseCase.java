package peer.useCase.initialization;

public interface RoleSpecificInitializationUseCase {
    void initialize();
    void initializeAfterLeaderDown();
    void initializeAfterLeaderDisconnection();
    void clearRoleSpecificServices();

}
