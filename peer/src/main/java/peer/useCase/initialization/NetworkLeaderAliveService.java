package peer.useCase.initialization;

public interface NetworkLeaderAliveService {
    void startHeartBeatScheduling();
    void stopHeartBeatScheduling();
}
