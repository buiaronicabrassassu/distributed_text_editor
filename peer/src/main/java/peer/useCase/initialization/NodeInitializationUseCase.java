package peer.useCase.initialization;

import entities.Node;
import peer.Log;
import peer.factory.CommonNodeFactory;
import peer.factory.LeaderNodeFactory;
import peer.factory.PeerNodeFactory;
import peer.network.rmi.service.initialization.RmiInitializationService;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class NodeInitializationUseCase{
    private final Node currentNode;
    private final RmiInitializationService rmiNetworkInitialization;
    private final CommonNodeFactory commonNodeFactory;

    private final AtomicReference<RoleSpecificInitializationUseCase>roleSpecificInitialization = new AtomicReference<>();
    private final AtomicInteger initializations = new AtomicInteger(0);

    public NodeInitializationUseCase(Node currentNode, CommonNodeFactory commonNodeFactory, RmiInitializationService networkService){
        this.currentNode = currentNode;
        this.rmiNetworkInitialization = networkService;
        this.commonNodeFactory = commonNodeFactory;
    }

    public void initializeCurrentNode(){
        initializations.set(0);

        Node leader = rmiNetworkInitialization.getLeaderNode();
        if(amILeader(leader)){
            roleSpecificInitialization.set(getLeaderInitializerUseCase());
        }
        else{
            roleSpecificInitialization.set(getPeerInitializerUseCase());
        }
        roleSpecificInitialization.get().initialize();
    }

    void retryInitialization(){
        int initNumbers = initializations.incrementAndGet();
        if(initNumbers == 2){
            Log.print("\nInitialization failed");
            initializeAfterLeaderDown(rmiNetworkInitialization.getLeaderNode());
            return;
        }

        Node leader = rmiNetworkInitialization.getLeaderNode();
        if(amILeader(leader)){
            initializations.set(0);
            roleSpecificInitialization.set(getLeaderInitializerUseCase());
        }

        roleSpecificInitialization.get().initialize();
    }

    public void initializeAfterLeaderDisconnection(Node disconnectingLeader){

        roleSpecificInitialization.get().clearRoleSpecificServices();
        Node newLeader = rmiNetworkInitialization.newLeaderElection(disconnectingLeader);
        Log.print("The new leader is "+newLeader.getUserName());

        if(amILeader(newLeader)){
            roleSpecificInitialization.set(getLeaderInitializerUseCase());
        }
        roleSpecificInitialization.get().initializeAfterLeaderDisconnection();
    }

    public void initializeAfterLeaderDown(Node oldLeader){
        commonNodeFactory.clearDataStructure();
        roleSpecificInitialization.get().clearRoleSpecificServices();

        Node newLeader = rmiNetworkInitialization.notifyLeaderDownToServer(oldLeader);
        Log.print("The new leader is "+newLeader.getUserName());

        if(amILeader(newLeader)){
            roleSpecificInitialization.set(getLeaderInitializerUseCase());
        }
        roleSpecificInitialization.get().initializeAfterLeaderDown();
    }

    boolean amILeader(Node leader){
        return currentNode.equals(leader);
    }

    private PeerInitializerUseCase getPeerInitializerUseCase(){
        return new PeerInitializerUseCase(
                this,
                new PeerNodeFactory(commonNodeFactory, this),
                commonNodeFactory.guiLeaderDownService,
                commonNodeFactory.versionManager
        );
    }

    private LeaderInitializerUseCase getLeaderInitializerUseCase(){
        return new LeaderInitializerUseCase(
                this,
                new LeaderNodeFactory(commonNodeFactory, this),
                commonNodeFactory.guiLeaderDownService,
                commonNodeFactory.versionManager
        );
    }

}
