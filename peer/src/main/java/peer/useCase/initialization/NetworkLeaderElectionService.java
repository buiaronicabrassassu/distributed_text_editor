package peer.useCase.initialization;

import entities.Node;

public interface NetworkLeaderElectionService {
    Node getLeaderNode();
    Node newLeaderElection(Node oldLeaderNode);

    Node notifyLeaderDownToServer(Node oldLeaderNode);
}
