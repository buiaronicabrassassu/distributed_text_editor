package peer.useCase.initialization;

public interface DocumentInitializationService {
    /*
    * This method  must block the execution until the leader is ready*/
    void waitForLeader();
}