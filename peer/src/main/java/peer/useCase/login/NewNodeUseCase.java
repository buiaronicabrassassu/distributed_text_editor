package peer.useCase.login;

import entities.Node;
import entities.Task;
import peer.data.LocalTaskManager;
import peer.useCase.taskExchange.TaskNotificationService;

import java.util.List;

public class NewNodeUseCase {
    LocalTaskManager localTaskManager;
    TaskNotificationService notificationService;

    public NewNodeUseCase(LocalTaskManager localTaskManager,
                          TaskNotificationService notificationService){
        this.localTaskManager = localTaskManager;
        this.notificationService = notificationService;
    }

    public void handleNewNode(Node node){
        List<Task> oldTasks = localTaskManager.getNotifiedTasks();
        notificationService.notifyTasksToNode(oldTasks, node);
    }
}
