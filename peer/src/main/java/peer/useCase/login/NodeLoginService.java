package peer.useCase.login;

import entities.Node;
import entities.Version;

public interface NodeLoginService {
    public void sendLoginAck(Node node, Version version);
    public void notifyNewNode(Node newNode);
}
