package peer.useCase.login;

import entities.Version;

import java.util.concurrent.CompletableFuture;

public interface NetworkLoginService {
    CompletableFuture<Version> login();
}
