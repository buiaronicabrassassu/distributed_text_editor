package peer.useCase.login;

import entities.Node;
import peer.Log;
import peer.data.LocalTaskManager;
import peer.data.VersionManager;
import peer.useCase.ProtocolScheduler.MultiInstanceProtocol;
import peer.useCase.ProtocolScheduler.ProtocolScheduler;
import peer.useCase.taskExchange.TaskNotificationService;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Leader_PeerLoginUseCase {

    private final VersionManager versionManager;
    private final NodeLoginService nodeLoginService;

    private final LocalTaskManager localTaskManager;
    private final ProtocolScheduler protocolScheduler;
    private final TaskNotificationService notificationService;

    public Leader_PeerLoginUseCase(VersionManager versionManager,
                                   LocalTaskManager localTaskManager,
                                   TaskNotificationService notificationService,
                                   ProtocolScheduler protocolScheduler,
                                   NodeLoginService nodeLoginService){

        this.versionManager = versionManager;
        this.localTaskManager = localTaskManager;
        this.protocolScheduler = protocolScheduler;
        this.notificationService = notificationService;
        this.nodeLoginService = nodeLoginService;
    }

    public void handleLoginRequest(Node node){
        Log.print("Node login request received");
        NodeLoginProtocol protocol = new NodeLoginProtocol(node);
        protocolScheduler.scheduleProtocol(protocol);

    }

    private class NodeLoginProtocol implements MultiInstanceProtocol{
        private final Node node;

        private NodeLoginProtocol(Node node) {
            this.node = node;
        }

        @Override
        public CompletableFuture<Void> execute(Executor executor){
            return CompletableFuture
                    .supplyAsync(versionManager::getCurrentVersion, Executors.newSingleThreadExecutor())
                    .thenAccept((version)->nodeLoginService.sendLoginAck(node, version))
                    .thenApply((none) -> localTaskManager.getNotifiedTasks())
                    .thenAccept((tasks)->notificationService.notifyTasksToNode(tasks, node))
                    .thenRun(()->{nodeLoginService.notifyNewNode(node);});
        }

        @Override
        public String getName() {
            return MultiInstanceProtocol.NODE_LOGIN;
        }
    }
}
