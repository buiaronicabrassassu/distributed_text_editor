package peer.useCase.login;

import peer.data.VersionManager;
import peer.Log;
import peer.useCase.ProtocolScheduler.ProtocolScheduler;
import peer.useCase.ProtocolScheduler.UniqueInstanceProtocol;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

public class DocumentLoginUseCase {
    private final VersionManager versionManager;
    private final NetworkLoginService networkLoginService;

    public DocumentLoginUseCase(VersionManager versionManager, NetworkLoginService networkLoginService){
        this.versionManager = versionManager;
        this.networkLoginService = networkLoginService;
    }

    public LoginProtocol getLoginProtocol(){
        return new LoginProtocol();
    }

    private class LoginProtocol implements UniqueInstanceProtocol{

        @Override
        public CompletableFuture<Void> execute(Executor executor) {
            return CompletableFuture
                    .runAsync(() ->Log.print("Start login Protocol "+this), executor)
                    .thenComposeAsync((none) -> networkLoginService.login(), executor)
                    .thenAccept(versionManager::updateCurrentVersion);
        }

        @Override
        public String getName() {
            return UniqueInstanceProtocol.LOGIN;
        }
    }
}
