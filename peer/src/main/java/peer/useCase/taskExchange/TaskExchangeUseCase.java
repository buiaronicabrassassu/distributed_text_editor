package peer.useCase.taskExchange;

import entities.Task;
import peer.data.LocalTaskManager;
import peer.data.VersionManager;

public class TaskExchangeUseCase {
    private final VersionManager versionManager;
    private final LocalTaskManager localTaskManager;
    private final TaskNotificationService notificationService;
    private final GuiTaskNotifier guiTaskNotifier;

    public TaskExchangeUseCase(
            VersionManager versionManager,
            LocalTaskManager localTaskManager,
            GuiTaskNotifier guiTaskNotifier,
            TaskNotificationService notificationService){
        this.versionManager = versionManager;
        this.localTaskManager = localTaskManager;
        this.notificationService = notificationService;
        this.guiTaskNotifier = guiTaskNotifier;
    }

    public void handleRemoteTask(Task remoteTask){
        if(isValidTask(remoteTask)){
            guiTaskNotifier.notifyRemoteTask(remoteTask);
        }
    }

    public void handleLocalTask(Task localTask){

        if(isValidTask(localTask)){
            notificationService.notifyTask(localTask);
            localTaskManager.insertTask(localTask);
        }
    }

    private boolean isValidTask(Task task){
        return task.version >= versionManager.getCurrentVersionId();
    }
}

