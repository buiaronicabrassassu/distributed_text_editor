package peer.useCase.taskExchange;

import entities.Node;
import entities.Task;

import java.util.List;

public interface TaskNotificationService{
    public void notifyTask(Task task);
    public void notifyTasksToNode(List<Task> tasks, Node node);
}
