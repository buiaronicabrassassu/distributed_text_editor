package peer.useCase.taskExchange;

import entities.Task;

import java.util.List;

public interface GuiTaskNotifier {
    public void notifyRemoteTask(Task task);
    public void notifyRemoteTasks(List<Task> tasks);
}
