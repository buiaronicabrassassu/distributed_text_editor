package peer.factory;

import peer.useCase.ProtocolScheduler.ProtocolScheduler;

public interface NodeFactory{
    void buildNodeSpecificServices();
    void bindNodeSpecificNetworkServices();
    void startNode();

    ProtocolScheduler getProtocolScheduler();
}