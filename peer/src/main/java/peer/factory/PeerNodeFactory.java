package peer.factory;

import peer.api.gui.*;
import peer.network.rmi.service.errorHandler.RmiPeerErrorHandler;
import peer.network.rmi.service.heartBeat.IHeartBeatService;
import peer.network.rmi.service.heartBeat.RmiPeerNetworkHeartbeatService;
import peer.network.rmi.service.initialization.RmiDocumentInitializationService;
import peer.network.rmi.service.login.IPeerLoginService;
import peer.network.rmi.service.newVersion.IPeerNewVersionService;
import peer.network.rmi.service.disconnection.IPeerNodeDisconnectionService;
import peer.network.rmi.service.disconnection.RmiPeerDisconnectionService;
import peer.network.rmi.service.login.RmiPeerLoginService;

import peer.network.rmi.service.newVersion.RmiPeerNetworkNewVersionService;
import peer.network.rmi.service.nodeDown.RmiNetworkNodeDownService;
import peer.network.rmi.service.taskExchange.ITaskNotificationService;
import peer.network.rmi.service.taskExchange.RmiTaskNotificationService;
import peer.useCase.ProtocolScheduler.PeerProtocolScheduler;
import peer.useCase.ProtocolScheduler.ProtocolScheduler;
import peer.useCase.disconnection.PeerDisconnectionUseCase;
import peer.useCase.guiInitialization.GuiInitializationUseCase;
import peer.useCase.initialization.NodeInitializationUseCase;
import peer.useCase.initialization.DocumentInitializationService;
import peer.useCase.nodeDown.LeaderDownUseCase;
import peer.useCase.login.DocumentLoginUseCase;
import peer.useCase.disconnection.GuiDisconnectionService;
import peer.useCase.login.NewNodeUseCase;
import peer.useCase.newVersion.PeerNewVersionUseCase;
import peer.useCase.taskExchange.GuiTaskNotifier;
import peer.useCase.taskExchange.TaskExchangeUseCase;

import java.rmi.RemoteException;


public class PeerNodeFactory implements NodeFactory {
    public volatile CommonNodeFactory commonNodeFactory;
    RmiPeerLoginService rmiLoginService;
    RmiPeerDisconnectionService networkDisconnectionService;
    RmiPeerNetworkNewVersionService rmiNetworkNewVersionService;
    public final RmiPeerNetworkHeartbeatService rmiPeerNetworkHeartbeatService;

    private final NodeInitializationUseCase nodeInitializer;

    public final ProtocolScheduler protocolScheduler;
    public volatile NewNodeUseCase newNodeUseCase;
    public volatile DocumentLoginUseCase documentLoginUseCase;
    private final GuiInitializationUseCase guiInitializationUseCase;
    public volatile PeerDisconnectionUseCase disconnectionUseCase;
    public volatile PeerNewVersionUseCase peerNewVersionUseCase;
    private volatile TaskExchangeUseCase taskExchangeUseCase;
    public final DocumentInitializationService documentInitializationService;
    private volatile LeaderDownUseCase leaderDownUseCase;
    private final RmiPeerErrorHandler rmiErrorHandler;
    private final RmiTaskNotificationService taskNotificationService;

    public PeerNodeFactory(
            CommonNodeFactory commonNodeFactory,
            NodeInitializationUseCase nodeInitializer){
        this.commonNodeFactory = commonNodeFactory;
        this.protocolScheduler = new PeerProtocolScheduler(commonNodeFactory.protocolManager);
        this.nodeInitializer = nodeInitializer;
        this.leaderDownUseCase = buildLeaderDownService();
        guiInitializationUseCase = buildGuiInitializationService();
        rmiErrorHandler = new RmiPeerErrorHandler(commonNodeFactory.rmiNetwork, leaderDownUseCase, commonNodeFactory.protocolManager);
        taskNotificationService = new RmiTaskNotificationService(commonNodeFactory.rmiNetwork, rmiErrorHandler);
        documentInitializationService = new RmiDocumentInitializationService(commonNodeFactory.rmiNetwork);
        rmiPeerNetworkHeartbeatService = new RmiPeerNetworkHeartbeatService(commonNodeFactory.rmiNetwork, rmiErrorHandler);
    }

    @Override
    public void buildNodeSpecificServices(){
        disconnectionUseCase = buildDisconnectionService();
        peerNewVersionUseCase = buildNewVersionService();
        documentLoginUseCase = buildDocumentLoginService();
        newNodeUseCase = buildNewNodeService();
        taskExchangeUseCase = buildTaskExchangeService();
    }

    @Override
    public void bindNodeSpecificNetworkServices(){
        try {
            commonNodeFactory.registryController.bindPeerServices(
                    new ITaskNotificationService(commonNodeFactory.rmiNetwork, taskExchangeUseCase),
                    new IPeerLoginService(commonNodeFactory.rmiNetwork, rmiLoginService, newNodeUseCase),
                    new IPeerNodeDisconnectionService(commonNodeFactory.rmiNetwork, networkDisconnectionService, disconnectionUseCase),
                    new IPeerNewVersionService(peerNewVersionUseCase,rmiNetworkNewVersionService),
                    new IHeartBeatService(commonNodeFactory.rmiNetwork)
            );
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ProtocolScheduler getProtocolScheduler() {
        return protocolScheduler;
    }

    private PeerDisconnectionUseCase buildDisconnectionService(){
        networkDisconnectionService = new RmiPeerDisconnectionService(commonNodeFactory.rmiNetwork, commonNodeFactory.registryController);
        GuiDisconnectionService guiDisconnectionService = new IGuiDisconnectionService(commonNodeFactory.commandManager, commonNodeFactory.httpServer);

        PeerDisconnectionUseCase disconnectionUseCase = new PeerDisconnectionUseCase(
                commonNodeFactory.localTaskManager,
                commonNodeFactory.versionManager,
                commonNodeFactory.httpServer,
                nodeInitializer,
                networkDisconnectionService,
                documentInitializationService,
                guiDisconnectionService,
                protocolScheduler
        );

        commonNodeFactory.httpServer.addContext(new DisconnectionContext(disconnectionUseCase));
        return disconnectionUseCase;

    }

    private GuiInitializationUseCase buildGuiInitializationService(){
        GuiInitializationUseCase guiInitializationUseCase = new GuiInitializationUseCase(commonNodeFactory.versionManager, protocolScheduler, commonNodeFactory.rmiNetwork.currentNode.user, commonNodeFactory.guiInitializationService);
        commonNodeFactory.httpServer.addContext(new GuiInitializationContext(commonNodeFactory.rmiNetwork.currentNode.user, guiInitializationUseCase, commonNodeFactory.commandManager));
        return guiInitializationUseCase;
    }

    private DocumentLoginUseCase buildDocumentLoginService(){
        rmiLoginService = new RmiPeerLoginService(commonNodeFactory.rmiNetwork);
        DocumentLoginUseCase documentLoginUseCase = new DocumentLoginUseCase(commonNodeFactory.versionManager, rmiLoginService);
        return documentLoginUseCase;
    }

    private NewNodeUseCase buildNewNodeService(){
        NewNodeUseCase newNodeUseCase = new NewNodeUseCase(commonNodeFactory.localTaskManager, taskNotificationService);
        return newNodeUseCase;
    }

    private LeaderDownUseCase buildLeaderDownService(){
        RmiNetworkNodeDownService rmiNetworkNodeDownService = new RmiNetworkNodeDownService(commonNodeFactory.rmiNetwork);
        LeaderDownUseCase leaderDownUseCase = new LeaderDownUseCase(nodeInitializer, protocolScheduler, rmiNetworkNodeDownService,commonNodeFactory.guiLeaderDownService);
        return leaderDownUseCase;
    }

    private TaskExchangeUseCase buildTaskExchangeService(){
        GuiTaskNotifier guiTaskNotifier = new IGuiTaskNotifier(commonNodeFactory.commandManager);
        TaskExchangeUseCase taskExchangeUseCase = new TaskExchangeUseCase(
                commonNodeFactory.versionManager,
                commonNodeFactory.localTaskManager,
                guiTaskNotifier,
                taskNotificationService
        );

        commonNodeFactory.httpServer.addContext(new TaskExchangeContext(taskExchangeUseCase));

        return taskExchangeUseCase;
    }

    private PeerNewVersionUseCase buildNewVersionService(){
        rmiNetworkNewVersionService = new RmiPeerNetworkNewVersionService(commonNodeFactory.rmiNetwork, rmiErrorHandler);
        IGuiVersionUpdater guiVersionUpdater = new IGuiVersionUpdater(commonNodeFactory.commandManager);
        PeerNewVersionUseCase peerNewVersionUseCase = new PeerNewVersionUseCase(
                commonNodeFactory.versionManager,
                commonNodeFactory.localTaskManager,
                rmiNetworkNewVersionService,
                guiVersionUpdater,
                protocolScheduler);
        commonNodeFactory.httpServer.addContext(new NewVersionContext(guiVersionUpdater));
        return peerNewVersionUseCase;
    }


    @Override
    public void startNode() {
        commonNodeFactory.startHttpServer();
        //documentLoginUseCase.documentLogin();
    }
}
