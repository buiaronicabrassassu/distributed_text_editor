package peer.factory;

import peer.api.gui.*;
import peer.network.rmi.service.errorHandler.RmiLeaderErrorHandler;
import peer.network.rmi.service.heartBeat.IHeartBeatService;
import peer.network.rmi.service.login.ILeaderLoginService;
import peer.network.rmi.service.disconnection.ILeaderNodeDisconnectionService;
import peer.network.rmi.service.login.RmiLeaderLoginService;
import peer.network.rmi.service.login.RmiDocumentLoginService;
import peer.network.rmi.service.disconnection.RmiLeaderDisconnectionService;
import peer.network.rmi.service.newVersion.ILeaderNewVersionService;
import peer.network.rmi.service.newVersion.RmiLeaderNetworkNewVersionService;
import peer.network.rmi.service.taskExchange.ITaskNotificationService;
import peer.network.rmi.service.taskExchange.RmiTaskNotificationService;
import peer.useCase.ProtocolScheduler.LeaderProtocolScheduler;
import peer.useCase.ProtocolScheduler.ProtocolScheduler;
import peer.network.rmi.service.nodeDown.ILeaderPeerDownService;
import peer.network.rmi.service.nodeDown.RmiNetworkNodeDownService;
import peer.useCase.disconnection.GuiDisconnectionService;
import peer.useCase.disconnection.LeaderDisconnectionUseCase;
import peer.useCase.guiInitialization.GuiInitializationUseCase;
import peer.useCase.initialization.NodeInitializationUseCase;
import peer.useCase.login.DocumentLoginUseCase;
import peer.useCase.login.Leader_PeerLoginUseCase;
import peer.useCase.newVersion.LeaderNewVersionUseCase;
import peer.useCase.taskExchange.GuiTaskNotifier;
import peer.useCase.taskExchange.TaskExchangeUseCase;
import peer.useCase.nodeDown.PeerDownUseCase;


import java.rmi.RemoteException;

public class LeaderNodeFactory implements NodeFactory {
    public volatile CommonNodeFactory commonNodeFactory;
    RmiLeaderDisconnectionService rmiDisconnectionService;
    RmiLeaderNetworkNewVersionService rmiLeaderNetworkNewVersionService;

    public volatile Leader_PeerLoginUseCase leaderPeerLoginUseCase;
    public volatile LeaderDisconnectionUseCase disconnectionUseCase;
    private final GuiInitializationUseCase guiInitializationUseCase;
    public volatile DocumentLoginUseCase documentLoginUseCase;
    public volatile LeaderNewVersionUseCase leaderNewVersionUseCase;
    public volatile TaskExchangeUseCase taskExchangeUseCase;
    public volatile PeerDownUseCase peerDownUseCase;
    public final ProtocolScheduler protocolScheduler;

    private final RmiLeaderErrorHandler rmiErrorHandler;
    private final RmiTaskNotificationService taskNotificationService;
    private final RmiNetworkNodeDownService networkPeerDownService;
    private final NodeInitializationUseCase nodeInitializationUseCase;


    public LeaderNodeFactory(CommonNodeFactory commonNodeFactory, NodeInitializationUseCase nodeInitializationUseCase){
        this.commonNodeFactory = commonNodeFactory;
        this.protocolScheduler = new LeaderProtocolScheduler(commonNodeFactory.protocolManager);
        this.nodeInitializationUseCase = nodeInitializationUseCase;

        peerDownUseCase = buildPeerDownService(protocolScheduler);
        networkPeerDownService = new RmiNetworkNodeDownService(commonNodeFactory.rmiNetwork);
        rmiErrorHandler = new RmiLeaderErrorHandler(commonNodeFactory.rmiNetwork, peerDownUseCase);
        rmiDisconnectionService = new RmiLeaderDisconnectionService(commonNodeFactory.rmiNetwork, rmiErrorHandler);
        taskNotificationService = new RmiTaskNotificationService(commonNodeFactory.rmiNetwork, rmiErrorHandler);

        guiInitializationUseCase = buildGuiInitializationService();

        peerDownUseCase.setNetworkServices(networkPeerDownService, rmiDisconnectionService);

    }

    @Override
    public void buildNodeSpecificServices(){

        buildNodeLoginService();
        buildDisconnectionService();
        buildDocumentLoginService();
        buildNewVersionService();
        taskExchangeUseCase = buildTaskExchangeService();
    }

    @Override
    public void bindNodeSpecificNetworkServices() {
        try {
            commonNodeFactory.registryController.bindLeaderServices(
                    new ITaskNotificationService(commonNodeFactory.rmiNetwork, taskExchangeUseCase),
                    new IHeartBeatService(commonNodeFactory.rmiNetwork),
                    new ILeaderLoginService(commonNodeFactory.rmiNetwork, leaderPeerLoginUseCase),
                    new ILeaderNodeDisconnectionService(disconnectionUseCase),
                    new ILeaderNewVersionService(commonNodeFactory.rmiNetwork, rmiLeaderNetworkNewVersionService),
                    new ILeaderPeerDownService(peerDownUseCase)
            );
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ProtocolScheduler getProtocolScheduler() {
        return protocolScheduler;
    }

    private TaskExchangeUseCase buildTaskExchangeService(){
        GuiTaskNotifier guiTaskNotifier = new IGuiTaskNotifier(commonNodeFactory.commandManager);
        TaskExchangeUseCase taskExchangeUseCase = new TaskExchangeUseCase(
                commonNodeFactory.versionManager,
                commonNodeFactory.localTaskManager,
                guiTaskNotifier,
                taskNotificationService
        );

        commonNodeFactory.httpServer.addContext(new TaskExchangeContext(taskExchangeUseCase));

        return taskExchangeUseCase;
    }

    private GuiInitializationUseCase buildGuiInitializationService(){
        GuiInitializationUseCase guiInitializationUseCase = new GuiInitializationUseCase(commonNodeFactory.versionManager, protocolScheduler, commonNodeFactory.rmiNetwork.currentNode.user, commonNodeFactory.guiInitializationService);
        commonNodeFactory.httpServer.addContext(new GuiInitializationContext(commonNodeFactory.rmiNetwork.currentNode.user, guiInitializationUseCase, commonNodeFactory.commandManager));
        return guiInitializationUseCase;
    }

    private void buildDisconnectionService(){

        GuiDisconnectionService guiDisconnectionService = new IGuiDisconnectionService(commonNodeFactory.commandManager, commonNodeFactory.httpServer);

        disconnectionUseCase = new LeaderDisconnectionUseCase(
                commonNodeFactory.localTaskManager,
                commonNodeFactory.versionManager,
                commonNodeFactory.httpServer,
                rmiDisconnectionService,
                guiDisconnectionService,
                protocolScheduler);
        commonNodeFactory.httpServer.addContext(new DisconnectionContext(disconnectionUseCase));
    }


    private PeerDownUseCase buildPeerDownService(ProtocolScheduler protocolScheduler){
        PeerDownUseCase peerDownUseCase = new PeerDownUseCase( protocolScheduler);
        return peerDownUseCase;
    }

    private void buildDocumentLoginService(){
        RmiLeaderLoginService rmiLoginService = new RmiLeaderLoginService(commonNodeFactory.rmiNetwork);
        documentLoginUseCase = new DocumentLoginUseCase(commonNodeFactory.versionManager, rmiLoginService);
    }

    private void buildNodeLoginService(){
        RmiDocumentLoginService rmiDocumentLoginService = new RmiDocumentLoginService(commonNodeFactory.rmiNetwork);
        leaderPeerLoginUseCase = new Leader_PeerLoginUseCase(
                commonNodeFactory.versionManager,
                commonNodeFactory.localTaskManager,
                taskNotificationService,
                protocolScheduler,
                rmiDocumentLoginService);
    }

    private void buildNewVersionService(){
        rmiLeaderNetworkNewVersionService = new RmiLeaderNetworkNewVersionService(commonNodeFactory.rmiNetwork, rmiErrorHandler);
        IGuiVersionUpdater guiVersionUpdater = new IGuiVersionUpdater(commonNodeFactory.commandManager);
        leaderNewVersionUseCase = new LeaderNewVersionUseCase(
                commonNodeFactory.versionManager,
                commonNodeFactory.localTaskManager,
                rmiLeaderNetworkNewVersionService,
                guiVersionUpdater,
                protocolScheduler
        );
        commonNodeFactory.httpServer.addContext(new NewVersionContext(guiVersionUpdater));
    }

    @Override
    public void startNode(){
        commonNodeFactory.startHttpServer();
    }
}
