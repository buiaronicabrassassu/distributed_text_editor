package peer.factory;

import peer.api.gui.*;
import peer.data.LocalTaskManager;
import peer.data.VersionManager;
import peer.network.rmi.RmiNetwork;
import peer.network.rmi.RmiRegistryController;

import peer.useCase.ProtocolScheduler.ProtocolManager;
import peer.network.rmi.service.errorHandler.RmiPeerErrorHandler;

import peer.useCase.guiInitialization.GuiInitializationUseCase;
import peer.useCase.nodeDown.GuiLeaderDownService;

public class CommonNodeFactory {
    final RmiRegistryController registryController;
    final RmiNetwork rmiNetwork;

    public final VersionManager versionManager;
    final LocalTaskManager localTaskManager;
    final ProtocolManager protocolManager;
    final CommandManager commandManager;

    final HttpServerWrapper httpServer;

    public final GuiLeaderDownService guiLeaderDownService;
    public final IGuiInitializationService guiInitializationService;

    public CommonNodeFactory(RmiNetwork rmiNetwork){
        this.rmiNetwork = rmiNetwork;
        this.httpServer = new HttpServerWrapper(true);
        this.registryController = new RmiRegistryController(rmiNetwork);
        this.versionManager = new VersionManager();
        this.localTaskManager = new LocalTaskManager();
        this.protocolManager = new ProtocolManager();
        this.commandManager = new CommandManager();

        httpServer.addContext(new CommandContext(commandManager));

        guiLeaderDownService = new IGuiLeaderDownService(commandManager);
        guiInitializationService = new IGuiInitializationService(commandManager);
    }

    public void clearDataStructure(){
        localTaskManager.clear();
        versionManager.clear();
        rmiNetwork.clear();
        protocolManager.clear();
        commandManager.clear();
    }

    void startHttpServer(){
        httpServer.startServer();
    }
}
