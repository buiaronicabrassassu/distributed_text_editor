package peer;

import entities.*;
import peer.factory.CommonNodeFactory;
import peer.network.rmi.RmiNetwork;
import peer.network.rmi.service.initialization.RmiInitializationService;
import peer.useCase.initialization.NodeInitializationUseCase;

import java.io.IOException;

public class Peer {

    public static void main(String[] args) throws IOException {
        String userName = args[0];
        int nodeId = Integer.parseInt(args[1]);
        String peerAddress= args[2];
        int peerPort = Integer.parseInt(args[3]);
        String serverAddress= args[4];
        int serverPort = Integer.parseInt(args[5]);

        System.setProperty("java.rmi.server.hostname", peerAddress);
        //System.setProperty("sun.rmi.transport.tcp.responseTimeout", "5000");
        System.setProperty("sun.rmi.transport.connectionTimeout", "5000");
        System.setProperty("sun.rmi.transport.proxy.connectTimeout", "5000");

        Log.initialize(new Log.ConsoleLog());

        Log.print("=============NODE INITIALIZATION==============");
        Log.print("User: "+ userName +"("+nodeId+")");

        Node currentNode = new Node(new User(userName, nodeId), peerAddress, peerPort);
        RmiNetwork rmiNetwork = new RmiNetwork(currentNode, serverAddress, serverPort);

        CommonNodeFactory commonNodeFactory = new CommonNodeFactory(rmiNetwork);
        RmiInitializationService initializationService = new RmiInitializationService(rmiNetwork);

        NodeInitializationUseCase initializationUseCase = new NodeInitializationUseCase(currentNode, commonNodeFactory, initializationService);

        Log.print("=============INIT PROTOCOL==============");
        initializationUseCase.initializeCurrentNode();
    }
}
