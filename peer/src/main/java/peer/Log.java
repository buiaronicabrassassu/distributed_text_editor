package peer;

public class Log {
    private static Log instance = null;
    private LogImplementation implementation;

    static void initialize(LogImplementation implementation){
        if(instance != null){
            throw new RuntimeException("Log service already initialized");
        }
        instance = new Log(implementation);
    }

    private Log(LogImplementation implementation){
        this.implementation = implementation;
    }

    public static void print(String message){
        if(instance == null){
            instance.initialize(new Log.NullLog());
        }
        instance.implementation.message(message);
    }

    public static void printError(String message){
        if(instance == null){
            throw new RuntimeException("Log service is not initialized");
        }
        instance.implementation.error(message);
    }

    private interface LogImplementation {
        void message(String message);
        void error(String message);
    }

    static class NullLog implements LogImplementation{
        @Override
        public void message(String message) {}
        @Override
        public void error(String message) {}
    }

    static class ConsoleLog implements LogImplementation{
        @Override
        public void message(String message) {System.out.println(message);}
        @Override
        public void error(String message) {System.out.println(message);}
    }
}
