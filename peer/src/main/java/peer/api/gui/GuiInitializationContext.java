package peer.api.gui;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import entities.User;
import peer.Log;
import peer.useCase.guiInitialization.GuiInitializationUseCase;

public class GuiInitializationContext extends HttpContextWrapper{
    private final String CONTEXT_NAME = "/getInitData";

    private final boolean logEnabled = true;
    private Gson parser = new Gson();
    private final GuiInitializationUseCase guiInitializationUseCase;
    private final User currentUser;
    private final CommandManager commandManager;

    public GuiInitializationContext(User currentUser, GuiInitializationUseCase guiInitializationUseCase, CommandManager commandManager){
        this.currentUser = currentUser;
        this.guiInitializationUseCase = guiInitializationUseCase;
        this.commandManager = commandManager;
    }

    @Override
    protected String getContextName(){
        return CONTEXT_NAME;
    }

    @Override
    protected HttpHandler getContextHandler(HttpServerWrapper server){
        HttpHandler handleInitDataRequest = (HttpExchange exchange)->{
            if(logEnabled){
                /*Log.print("\n--------------------------------------\n");
                Log.print("New document request");*/
                //printRequst(exchange);
            }

            commandManager.startInitialization();

            guiInitializationUseCase.handleGuiInitializationRequest();

            String response = "ok";

            server.sendResponse(exchange, response);
        };

        return handleInitDataRequest;
    }

}
