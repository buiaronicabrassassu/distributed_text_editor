package peer.api.gui;

import entities.Version;
import peer.Log;
import peer.api.gui.commands.LeaderDownCompletedCommand;
import peer.api.gui.commands.LeaderDownStartCommand;
import peer.useCase.nodeDown.GuiLeaderDownService;

public class IGuiLeaderDownService implements GuiLeaderDownService {
    private final CommandManager commandManager;

    public IGuiLeaderDownService(CommandManager commandManager){
        this.commandManager = commandManager;
    }

    @Override
    public void leaderDownStart() {
        LeaderDownStartCommand leaderDownStartCommand = new LeaderDownStartCommand();
        Log.print("Set leader down start command");
        commandManager.addCommand(leaderDownStartCommand);
    }

    @Override
    public void leaderDownCompleted(Version version) {
        LeaderDownCompletedCommand leaderDownCompletedCommand = new LeaderDownCompletedCommand(version);
        Log.print("Set leader down completed command");
        commandManager.addCommand(leaderDownCompletedCommand);
    }
}
