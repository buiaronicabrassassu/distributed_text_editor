package peer.api.gui;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import entities.Task;
import peer.Log;
import peer.useCase.taskExchange.TaskExchangeUseCase;

import static peer.api.gui.HttpUtils.parseRequestBody;

public class TaskExchangeContext extends HttpContextWrapper{
    private final String CONTEXT_NAME = "/sendLocalTask";
    private final boolean logEnabled = true;
    private final TaskExchangeUseCase taskExchangeUseCase;

    public TaskExchangeContext(TaskExchangeUseCase taskExchangeUseCase){
        this.taskExchangeUseCase = taskExchangeUseCase;
    }

    @Override
    protected String getContextName(){
        return CONTEXT_NAME;
    }

    @Override
    protected HttpHandler getContextHandler(HttpServerWrapper server){
        return (HttpExchange exchange)->{
            String taskBody = parseRequestBody(exchange);


            Gson gson = new Gson();
            Task receivedTask = gson.fromJson(taskBody, Task.class);
            if(logEnabled){
                /*Log.print("\n--------------------------------------\n");
                Log.print("Local "+receivedTask.type+" task");
                Log.print(receivedTask.type+" char "+receivedTask.character.value);*/
            }
            taskExchangeUseCase.handleLocalTask(receivedTask);

            String response = "ok";
            server.sendResponse(exchange, response);
        };
    }
}
