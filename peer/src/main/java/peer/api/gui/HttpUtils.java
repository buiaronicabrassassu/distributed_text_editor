package peer.api.gui;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpPrincipal;
import peer.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;

public class HttpUtils{
    static String parseRequestBody(HttpExchange exchange){
        StringBuilder body = new StringBuilder();
        try(InputStreamReader is = new InputStreamReader(exchange.getRequestBody());
            BufferedReader br = new BufferedReader(is)){

            br.lines().forEach(body::append);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return body.toString();
    }

    static void printRequst(HttpExchange exchange){
        Log.print("-- headers --");
        Headers requestHeaders = exchange.getRequestHeaders();
        requestHeaders.entrySet().forEach(System.out::println);

        Log.print("-- principle --");
        HttpPrincipal principal = exchange.getPrincipal();
        Log.print(principal.toString());

        Log.print("-- HTTP method --");
        String requestMethod = exchange.getRequestMethod();
        Log.print(requestMethod);

        Log.print("-- query --");
        URI requestURI = exchange.getRequestURI();
        String query = requestURI.getQuery();
        Log.print(query);
    }
}
