package peer.api.gui;


import com.sun.net.httpserver.HttpHandler;

public abstract class HttpContextWrapper {

    protected abstract String getContextName();
    protected abstract HttpHandler getContextHandler(HttpServerWrapper server);
}
