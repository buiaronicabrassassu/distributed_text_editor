package peer.api.gui.commands;

import entities.Task;

import java.util.List;

public class TasksCommand extends Command{
    public static transient final String TYPE = "crdtTask";
    public final List<Task> data;

    public TasksCommand(List<Task> tasks){
        super(TYPE);
        this.data = tasks;
    }

    public String toJsonString(){
        return gson.toJson(this, TasksCommand.class);
    }
}
