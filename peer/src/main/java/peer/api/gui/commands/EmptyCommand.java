package peer.api.gui.commands;

public class EmptyCommand extends Command{
    public static transient final String TYPE = "empty";
    public EmptyCommand() {
        super(TYPE);
    }

    public String toJsonString(){
        return  gson.toJson(this);
    }

}
