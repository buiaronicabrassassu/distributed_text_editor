package peer.api.gui.commands;


public class LeaderDownStartCommand extends Command {
    public static transient final String TYPE = "leaderDownStart";

    public LeaderDownStartCommand(){
        super(TYPE);
    }

    @Override
    public String toJsonString() {
        return gson.toJson(this, LeaderDownStartCommand.class);
    }
}