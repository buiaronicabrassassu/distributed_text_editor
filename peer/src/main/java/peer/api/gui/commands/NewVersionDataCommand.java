package peer.api.gui.commands;

import entities.Version;

public class NewVersionDataCommand extends Command{
    public static transient final String TYPE = "setNewVersionData";
    public final Version version;

    public NewVersionDataCommand(Version version){
        super(TYPE);
        this.version = version;
    }

    public String toJsonString(){
        return gson.toJson(this, NewVersionDataCommand.class);
    }
}
