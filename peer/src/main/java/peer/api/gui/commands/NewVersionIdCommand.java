package peer.api.gui.commands;

public class NewVersionIdCommand extends Command{
    public static transient final String TYPE = "setNewVersionId";
    public final int versionId;

    public NewVersionIdCommand(int versionId){
        super(TYPE);
        this.versionId = versionId;
    }

    public String toJsonString(){
        return gson.toJson(this, NewVersionIdCommand.class);
    }
}