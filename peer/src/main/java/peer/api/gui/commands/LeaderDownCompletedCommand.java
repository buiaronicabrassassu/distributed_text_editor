package peer.api.gui.commands;


import entities.Version;

public class LeaderDownCompletedCommand extends Command {
    public static transient final String TYPE = "leaderDownCompleted";
    public final Version version;

    public LeaderDownCompletedCommand(Version version){
        super(TYPE);
        this.version = version;
    }

    @Override
    public String toJsonString() {
        return gson.toJson(this, LeaderDownCompletedCommand.class);
    }
}