package peer.api.gui.commands;

import entities.User;
import entities.Version;

public class InitDataCommand extends Command{
    public static transient final String TYPE = "initializationCompleted";
    public final User user;
    public final Version version;

    public InitDataCommand(User user, Version version){
        super(TYPE);
        this.user = user;
        this.version = version;
    }

    @Override
    public String toJsonString() {
        return gson.toJson(this, InitDataCommand.class);
    }
}
