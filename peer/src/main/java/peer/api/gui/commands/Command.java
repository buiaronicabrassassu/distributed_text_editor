package peer.api.gui.commands;

import com.google.gson.Gson;

public abstract class Command{
    protected transient final Gson gson = new Gson();

    public final String type;

    public Command(String type){
        this.type = type;
    }

    public void onCommandSent(){
        return;
    }

    public abstract String toJsonString();

}