package peer.api.gui.commands;

import peer.api.gui.IGuiDisconnectionService;

public class DisconnectionCommand extends Command {
    public static transient final String TYPE = "disconnect";
    private transient IGuiDisconnectionService guiDisconnectionService;

    public DisconnectionCommand(IGuiDisconnectionService guiDisconnectionService){
        super(TYPE);
        this.guiDisconnectionService = guiDisconnectionService;
    }


    public void onCommandSent(){
        guiDisconnectionService.handleDisconnectionCompleted();
    }

    @Override
    public String toJsonString() {
        return gson.toJson(this, DisconnectionCommand.class);
    }
}