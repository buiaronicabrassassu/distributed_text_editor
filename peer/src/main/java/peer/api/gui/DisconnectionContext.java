package peer.api.gui;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import peer.Log;
import peer.useCase.disconnection.DisconnectionUseCase;

public class DisconnectionContext extends HttpContextWrapper{
    private final String CONTEXT_NAME = "/disconnect";
    private final boolean logEnabled = false;
    DisconnectionUseCase disconnectionUseCase;

    public DisconnectionContext(DisconnectionUseCase disconnectionUseCase){
        this.disconnectionUseCase = disconnectionUseCase;
    }

    @Override
    protected String getContextName(){
        return CONTEXT_NAME;
    }

    @Override
    protected HttpHandler getContextHandler(HttpServerWrapper server){
        HttpHandler handleLocalTask = (HttpExchange exchange)->{
            if(logEnabled){
                Log.print("\n--------------------------------------\n");
                Log.print("GUI: Disconnection request");
            }

            disconnectionUseCase.disconnect();

            server.sendResponse(exchange,"");

        };
        return handleLocalTask;
    }
}
