package peer.api.gui;

import entities.Version;
import peer.Log;
import peer.api.gui.commands.Command;
import peer.api.gui.commands.NewVersionDataCommand;
import peer.api.gui.commands.NewVersionIdCommand;
import peer.useCase.newVersion.GuiVersionUpdater;

import java.util.concurrent.CompletableFuture;

public class IGuiVersionUpdater implements GuiVersionUpdater {

    CompletableFuture<Void> pendingGuiSettingVersionId;
    CommandManager commandManager;

    public IGuiVersionUpdater(CommandManager commandManager){
        this.commandManager = commandManager;
    }

    @Override
    public CompletableFuture<Void> setVersionId(int versionId) {
        NewVersionIdCommand setNewVersionIdCommand = new NewVersionIdCommand(versionId);
        //Log.print("Set new version id command: " + setNewVersionIdCommand.toJsonString());
        Log.print("Set new version id command");
        commandManager.addCommand(setNewVersionIdCommand);

        pendingGuiSettingVersionId = new CompletableFuture<>();
        return pendingGuiSettingVersionId;
    }

    @Override
    public void setVersionData(Version newVersion){
        NewVersionDataCommand setNewVersionDataCommand = new NewVersionDataCommand(newVersion);
        //Log.print("Set new version data command: " + setNewVersionDataCommand.toJsonString());
        Log.print("Set new version data command");
        commandManager.addCommand(setNewVersionDataCommand);
    }

    public void versionIdGuiUpdated(){
        pendingGuiSettingVersionId.complete(null);
    }
}
