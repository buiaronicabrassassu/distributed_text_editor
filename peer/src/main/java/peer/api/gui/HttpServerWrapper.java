package peer.api.gui;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import peer.Log;
import peer.useCase.GuiController;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;

public class HttpServerWrapper implements GuiController {
    // socket server port on which it will listen
    private static final int DEFAULT_PORT = 8080;

    private HttpServer server;
    HashMap<String, HttpContext> serverContexts = new HashMap<>();
    private boolean logEnabled;
    private final AtomicBoolean serverStarted = new AtomicBoolean(false);

    public HttpServerWrapper(boolean logEnabled){
        this.logEnabled = logEnabled;

        try {
            server = HttpServer.create(new InetSocketAddress(DEFAULT_PORT), 0);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Error during Http server creation", e.getCause());
        }
    }

    public void addContext(HttpContextWrapper context){
        String contextName = context.getContextName();
        HttpHandler httpHandler = context.getContextHandler(this);

        if(serverContexts.containsKey(contextName)){
            Log.print("replaced handler for "+contextName);
            server.removeContext(serverContexts.get(contextName));
            serverContexts.replace(contextName, server.createContext(contextName, httpHandler));
        }
        else{
            serverContexts.put(contextName, server.createContext(contextName, httpHandler));
        }
    }

    @Override
    public void startServer(){
        boolean serverInitialized = !serverStarted.compareAndSet(false, true);
        if(!serverInitialized){
            server.start();
        }

        if(logEnabled){
            Log.print("Http Server started on port "+DEFAULT_PORT);
            /*Log.print("The available context are:");
            serverContexts.keySet().forEach((contextName)-> {
                Log.print("\t- " + contextName);
            });*/
        }
    }

    @Override
    public void stopServer(){
        Log.print("Http Server stopped");
        server.stop(0);
    }

    void sendResponse(HttpExchange exchange, String response) throws IOException {
        exchange.sendResponseHeaders(200, response.getBytes().length);
        try(OutputStream os =  exchange.getResponseBody()){
            os.write(response.getBytes());
        }
    }
}