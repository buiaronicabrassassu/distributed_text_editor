package peer.api.gui;

import peer.api.gui.commands.DisconnectionCommand;
import peer.useCase.disconnection.GuiDisconnectionService;

import java.util.concurrent.CompletableFuture;

public class IGuiDisconnectionService implements GuiDisconnectionService {
    CommandManager commandManager;
    HttpServerWrapper serverController;

    CompletableFuture<Void> guiDisconnectedPromise;

    public IGuiDisconnectionService(CommandManager commandManager, HttpServerWrapper serverController){
        this.commandManager = commandManager;
        this.serverController = serverController;
    }

    @Override
    public CompletableFuture<Void> confirmDisconnection(){
        guiDisconnectedPromise = new CompletableFuture<>();
        commandManager.addCommand(new DisconnectionCommand(this));
        return guiDisconnectedPromise;
    }

    public void handleDisconnectionCompleted(){
        serverController.stopServer();
        guiDisconnectedPromise.complete(null);
    }
}
