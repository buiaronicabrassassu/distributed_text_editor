package peer.api.gui;

import entities.Task;

import peer.Log;
import peer.api.gui.commands.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class CommandManager{
    private final List<Task> taskList = new ArrayList<>(1024);
    private final AtomicReference<DisconnectionCommand> disconnectionCommand = new AtomicReference<>(null);
    private final AtomicReference<NewVersionDataCommand> newVersionDataCommand= new AtomicReference<>(null);
    private final AtomicReference<NewVersionIdCommand> newVersionIdCommand= new AtomicReference<>(null);
    private final AtomicReference<LeaderDownStartCommand> leaderDownStartCommand = new AtomicReference<>(null);
    private final AtomicReference<LeaderDownCompletedCommand> leaderDownCompletedCommand = new AtomicReference<>(null);
    private final AtomicReference<InitDataCommand> initDataCommand = new AtomicReference<>(null);
    private final AtomicBoolean pendingInitialization = new AtomicBoolean(false);

    void startInitialization(){
        pendingInitialization.set(true);
    }

    public void addTasks(List<Task> tasks){
        synchronized (taskList){
            taskList.addAll(tasks);
        }
    }

    public void addCommand(DisconnectionCommand command){
        disconnectionCommand.set(command);
    }

    public void addCommand(NewVersionDataCommand command){
        newVersionDataCommand.set(command);
    }

    public void addCommand(NewVersionIdCommand command){
        newVersionIdCommand.set(command);
    }

    public void addCommand(LeaderDownStartCommand command) {
        leaderDownStartCommand.set(command);
    }

    public void addCommand(LeaderDownCompletedCommand command) {
        leaderDownCompletedCommand.set(command);
    }

    public void addCommand(InitDataCommand command) {
        initDataCommand.set(command);
    }

    public synchronized Command getCommand(){

        if(pendingInitialization.get()){
            InitDataCommand initDataCommand = this.initDataCommand.getAndSet(null);
            if(initDataCommand!=null){

                pendingInitialization.set(false);
                //TODO DELETE: Log.print("Return init data command "+initDataCommand.toJsonString());
                return initDataCommand;
            }
            else{
                return new EmptyCommand();
            }
        }

        DisconnectionCommand disconnectionCommand = this.disconnectionCommand.getAndSet(null);
        if(disconnectionCommand != null){
            return disconnectionCommand;
        }

        LeaderDownStartCommand leaderDownStartCommand = this.leaderDownStartCommand.getAndSet(null);
        if(leaderDownStartCommand != null){
            return leaderDownStartCommand;
        }

        LeaderDownCompletedCommand leaderDownCompletedCommand = this.leaderDownCompletedCommand.getAndSet(null);
        if(leaderDownCompletedCommand != null){
            return leaderDownCompletedCommand;
        }

        NewVersionIdCommand newVersionIdCommand = this.newVersionIdCommand.getAndSet(null);
        if(newVersionIdCommand != null){
            return newVersionIdCommand;
        }

        NewVersionDataCommand newVersionDataCommand = this.newVersionDataCommand.getAndSet(null);
        if(newVersionDataCommand != null){
            return newVersionDataCommand;
        }

        if(taskList.isEmpty()){
            return new EmptyCommand();
        }

        Command taskCommand = new TasksCommand(new ArrayList<>(taskList));
        taskList.clear();
        return taskCommand;
    }

    public void clear() {
        taskList.clear();
        disconnectionCommand.set(null);
        newVersionIdCommand.set(null);
        newVersionDataCommand.set(null);
        /** Don't delete this commands
        leaderDownStartCommand.set(null);
        leaderDownCompletedCommand.set(null);*/
    }

}
