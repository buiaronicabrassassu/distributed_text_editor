package peer.api.gui;

import entities.Task;
import peer.useCase.taskExchange.GuiTaskNotifier;
import java.util.ArrayList;
import java.util.List;

public class IGuiTaskNotifier implements GuiTaskNotifier {
    CommandManager commandManager;

    public IGuiTaskNotifier(CommandManager commandManager){
        this.commandManager = commandManager;
    }

    @Override
    public void notifyRemoteTask(Task task){
        notifyRemoteTasks(new ArrayList<Task>(){{add(task);}});
    }

    @Override
    public void notifyRemoteTasks(List<Task> tasks){
         commandManager.addTasks(tasks);
    }
}
