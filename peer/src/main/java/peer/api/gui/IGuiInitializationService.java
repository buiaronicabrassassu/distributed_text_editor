package peer.api.gui;

import entities.User;
import entities.Version;
import peer.api.gui.commands.InitDataCommand;
import peer.useCase.guiInitialization.GuiInitializationService;

public class IGuiInitializationService implements GuiInitializationService {
    private final CommandManager commandManager;

    public IGuiInitializationService(CommandManager commandManager){
        this.commandManager = commandManager;
    }

    @Override
    public void initializationCompleted(User user, Version version) {
        InitDataCommand initDataCommand = new InitDataCommand(user, version);
        commandManager.addCommand(initDataCommand);
    }

}
