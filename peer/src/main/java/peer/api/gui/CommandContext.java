package peer.api.gui;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import peer.api.gui.commands.Command;

public class CommandContext extends HttpContextWrapper{
    private final String CONTEXT_NAME = "/getCommand";
    private final boolean logEnabled = false;

    private final CommandManager commandManager;

    public CommandContext(CommandManager commandManager){
        this.commandManager = commandManager;
    }

    @Override
    protected String getContextName(){
        return CONTEXT_NAME;
    }

    @Override
    protected HttpHandler getContextHandler(HttpServerWrapper server){
        HttpHandler handleCommandRequest = (HttpExchange exchange)->{
            Command command = commandManager.getCommand();

            String response = command.toJsonString();

            if(logEnabled){
               /* Log.print("\n--------------------------------------\n");
                Log.print("Handle command request");
                Log.print("Response: "+response);*/
                //printRequst(exchange);
            }

            server.sendResponse(exchange, response);

            command.onCommandSent();
        };

        return handleCommandRequest;
    }

}
