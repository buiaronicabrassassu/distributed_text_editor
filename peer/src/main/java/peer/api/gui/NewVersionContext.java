package peer.api.gui;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import peer.Log;

public class NewVersionContext extends HttpContextWrapper{
    private final String CONTEXT_NAME = "/sendNewVersionIdAck";
    private final boolean logEnabled = true;
    IGuiVersionUpdater iGuiVersionUpdater;

    public NewVersionContext(IGuiVersionUpdater iGuiVersionUpdater){
        this.iGuiVersionUpdater = iGuiVersionUpdater;
    }

    @Override
    protected String getContextName(){
        return CONTEXT_NAME;
    }

    @Override
    protected HttpHandler getContextHandler(HttpServerWrapper server){
        HttpHandler handleLocalTask = (HttpExchange exchange)->{
            if(logEnabled){
                Log.print("\n--------------------------------------\n");
                Log.print("GUI: new version id updated");
            }

            iGuiVersionUpdater.versionIdGuiUpdated();

            String response = "ok";
            server.sendResponse(exchange, response);
        };
        return handleLocalTask;
    }
}