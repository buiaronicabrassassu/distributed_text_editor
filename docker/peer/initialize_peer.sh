#! /bin/bash

wait_peer_and_start_nginx(){
    peer_ready=false
    
    while ! nc -z -w5 localhost 8080; do
        #echo "not ready -> netcat $?"
        sleep 1
    done
    
    echo "ready -> start nginx"
    nginx
}

#cp -r peer gradle
#cp -r entities/src/main/java/entities/* gradle/peer/src/main/java/entities
#cp -r RmiInterfaces/src/main/java/rmiinterfaces gradle/peer/src/main/java/

# cp -r peer gradle
# cp -r entities gradle
# cp -r RmiInterfaces gradle


PEER_ADDRESS=$(awk 'END{print $1}' /etc/hosts)
export PEER_ADDRESS=$PEER_ADDRESS

wait_peer_and_start_nginx &

#cd /home/gradle
#gradle run --args="${USER_NAME} ${USER_ID} ${PEER_ADDRESS} ${PEER_REG_PORT} ${SERVER_ADDRESS} ${SERVER_REG_PORT}"
cd /home/app
java -jar peer.jar ${USER_NAME} ${USER_ID} ${PEER_ADDRESS} ${PEER_REG_PORT} ${SERVER_ADDRESS} ${SERVER_REG_PORT}

wait